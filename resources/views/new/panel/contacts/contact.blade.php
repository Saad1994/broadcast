@extends('newclient')

@section('content')
    @include('new.panel.contacts.top')
    <div class="col-md-12">
        <ul class="nav nav-tabs light-blue lighten-2 mx-0 mb-0 mt-1">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/user/view-contact-all')}}">Individual</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/user/phone-book')}}">Contact Group</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/user/sms/blacklist-contacts')}}">Blacklist</a>
            </li>
        </ul>
    </div>
    <div class="row" style="padding-top: 10px">
        @yield('table')
    </div>
@endsection

@section('scripts')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/vue.js") !!}
    {!! Html::script("assets/js/import-contact.js") !!}
    {!! Html::script("assets/js/form-elements-page.js")!!}

    <script>
        $('#submitContact').click(function(){
            $(this).hide();
            $('#uploadContact').show();
        });
    </script>
@endsection
