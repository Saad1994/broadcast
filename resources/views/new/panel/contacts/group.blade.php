@extends('new.panel.contacts.contact')

@section('table')
    <div class="col-md-12" style="padding-top: 10px">
        <h5>Group Contacts</h5>
    </div>
    <div class="col-md-12" style="padding-top: 10px">

        <table id="contact" class="table data-table table-striped table-bordered table-responsive-md" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th style="width: 15%">{{language_data('SL',Auth::guard('client')->user()->lan_id)}}</th>
                <th style="width: 40%">{{language_data('List name',Auth::guard('client')->user()->lan_id)}}</th>
                <th style="width: 45%">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
            </tr>
            </thead>
            <tbody>

            @foreach($clientGroups as $cg)
                <tr>
                    <td>
                        <p>{{ $loop->iteration }}</p>
                    </td>
                    <td>
                        <p>{{$cg->group_name}} </p>
                    </td>
                    <td>

                        <a href="{{url('user/view-contact/'.$cg->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> {{language_data('View Contacts',Auth::guard('client')->user()->lan_id)}}</a>
                        <a href="{{url('user/add-contact/'.$cg->id)}}" class="btn btn-complete btn-xs"><i class="fa fa-plus"></i> {{language_data('Add Contact',Auth::guard('client')->user()->lan_id)}}</a>
                        <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target=".modal_edit_list_{{$cg->id}}"><i class="fa fa-edit"></i> {{language_data('Edit',Auth::guard('client')->user()->lan_id)}}</a>
                        @include('client.modal-edit-contact-list')

                        <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$cg->id}}"><i class="fa fa-trash"></i> {{language_data('Delete',Auth::guard('client')->user()->lan_id)}}</a>
                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>

    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{language_data('Add New List',Auth::guard('client')->user()->lan_id)}}</h3>
            </div>
            <div class="panel-body">
                <form class="" role="form" method="post" action="{{url('user/post-phone-book')}}">

                    <div class="form-group">
                        <label>{{language_data('List name',Auth::guard('client')->user()->lan_id)}}</label>
                        <input type="text" class="form-control" name="list_name">
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add',Auth::guard('client')->user()->lan_id)}} </button>
                </form>
            </div>
        </div>
    </div>

@endsection
