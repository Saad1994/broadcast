
    <div class="row" style="padding-top: 20px">
        <div class="col-md-12">
            @include('notification.notify')
            <h2>My Contacts</h2>
        </div>
        <div class="col-md-3">
            <!-- Card -->
            <div class="card mb-4">
                <!--Card content-->
                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title">Add People</h4>
                    <!--Text-->
                    <p class="card-text">New Individual Contact</p>
                    <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                    <button type="button" class="btn btn-light-blue btn-md" data-toggle="modal" data-target="#addIndi">Add</button>
                </div>
            </div>
            <!-- Card -->
        </div>
        <div class="col-md-3">
            <!-- Card -->
            <div class="card mb-4">
                <!--Card content-->
                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title">Upload Contacts</h4>
                    <!--Text-->
                    <p class="card-text">Import an Excel or .CSV File</p>
                    <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                    <!--<button type="button" class="btn btn-light-blue btn-md" data-toggle="modal" data-target="#uploadDetails">Upload</button>-->
                    <a  class="btn btn-light-blue btn-md" href="{{url('user/sms/import-contacts')}}">Upload</a>

                </div>
            </div>
            <!-- Card -->
        </div>
        <div class="col-md-3">
            <!-- Card -->
            <div class="card mb-4">
                <!--Card content-->
                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title">Quick Add Number</h4>
                    <!--Text-->
                    <p class="card-text">Copy and Paste Phone Number</p>
                    <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                    <button type="button" class="btn btn-light-blue btn-md" data-toggle="modal" data-target="#quickAdd">Quick Add</button>
                </div>
            </div>
            <!-- Card -->
        </div>
        <div class="col-md-3">
            <!-- Card -->
            <div class="card mb-4">
                <!--Card content-->
                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title creategroup">Create Groups</h4>
                    <!--Text-->
                    <p class="card-text">Organize Your Contacts</p>
                    <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                    <button type="button" class="btn btn-light-blue btn-md"  data-toggle="modal" data-target="#createGroup" >Create</button>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    @include('new.modals.individual')
    @include('new.modals.createGroup')
    @include('new.modals.quickAdd')
    @include('new.modals.uploadContact')
