@extends('newclient')

@section('content')

    <div class="row" style="padding-top: 20px">
        <div class="col-md-9" >
            <!-- Card -->
            <div class="card">
                <!-- Card content -->
                <div class="card-body text-center">
                    <!-- Title -->
                    <h4 class="card-title">Broadcasting Center</h4>
                    <!-- Text -->
                    <p class="card-text">Need to send another broadcast or purchase a monthly broadcast plan</p>
                    <!-- Button -->
                    <a href="{{url('tab/call')}}" class="btn btn-info">Create Call Broadcast</a>
                    <a href="{{url('tab/text')}}" class="btn btn-amber">Create Text Broadcast</a>
                    <a href="{{url('user/sms/buy-unit')}}" class="btn btn-success">Buy Credits/Plans here</a>
                </div>
            </div>
            <!-- Card -->
            <!--Top Table UI-->
            <div class="card card-cascade narrower">
                <h3 class="text-center" style="padding-top: 10px">{{language_data('Recurring SMS',Auth::guard('client')->user()->lan_id)}}</h3>
                <table class="table data-table table-hover">
                    <thead>
                    <tr>
                        <th style="width: 10%">

                            <div class="coder-checkbox">
                                <input type="checkbox"  id="bulkDelete"  />
                                <span class="co-check-ui"></span>
                            </div>

                        </th>
                        <th style="width: 10%;">{{language_data('Sender',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 10%;">{{language_data('Recipients',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 15%;">{{language_data('Period',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 20%;">{{language_data('Date',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 10%;">{{language_data('Status',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 25%;">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
                    </tr>
                    </thead>
                </table>
                <!--Table-->
            </div>

            <div class="row" style="padding-top:10px">
                <div class="col-md-6">
                    <div class="card">
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Title -->
                            <h4 class="card-title">Inbox</h4>
                            <!-- Text -->
                            <p class="card-text"><strong>17</strong> new messages</p>

                            <strong>Total Incoming</strong> <br>
                            <p>17 Text | 0 Voice Mail</p>
                            <!-- Button -->
                            <a href="#" class="btn btn-primary btn-sm">View 2-way</a>
                            <a href="#" class="btn btn-primary btn-sm">View Voice Mail</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Recent Updates</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #00c853;">Your Active Service (s)</div>
            </div>
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-body">
                    <a class="activator waves-effect waves-light mr-4"><i class="fa fa-check-circle"></i></a>
                    <h5 class="card-title">Broadcasting Plan</h5>
                    <p class="card-text"><strong>300</strong> Users </p>
                    <a href="#" class="black-text d-flex justify-content-end"><h5>manage <i
                                    class="fa fa-angle-double-right"></i></h5></a>
                </div>
            </div>
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-body">
                    <a class="activator waves-effect waves-light mr-4"><i class="fa fa-check-circle"></i></a>
                    <h5 class="card-title">Premium Keywords</h5>
                    <p class="card-text"><strong>2</strong> Registered Keywords </p>
                    <a href="#" class="black-text d-flex justify-content-end"><h5>manage <i
                                    class="fa fa-angle-double-right"></i></h5></a>
                </div>
            </div>
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-body">
                    <a class="activator waves-effect waves-light mr-4"><i class="fa fa-check-circle"></i></a>
                    <h5 class="card-title">2 way sms</h5>
                    <p class="card-text"><strong>450</strong> Remaining </p>
                    <a href="#" class="black-text d-flex justify-content-end"><h5>manage <i
                                    class="fa fa-angle-double-right"></i></h5></a>
                </div>
            </div>

            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #00bcd4">Other Services we Offer</div>
            </div>
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-body">
                    <a class="activator waves-effect waves-light mr-4"><i class="fa fa-ban"></i></a>
                    <h5 class="card-title">Push to Talk</h5>
                    <p class="card-text">Increase your sale</p>
                    <a href="#" class="black-text d-flex justify-content-end"><h5>buy now <i
                                    class="fa fa-angle-double-right"></i></h5></a>
                </div>
            </div>
            <div class="card mb-3" style="max-width: 18rem;">
                <div class="card-body">
                    <a class="activator waves-effect waves-light mr-4"><i class="fa fa-ban"></i></a>
                    <h5 class="card-title">Vanity Number</h5>
                    <p class="card-text">Get a cool number</p>
                    <a href="#" class="black-text d-flex justify-content-end"><h5>buy now <i
                                    class="fa fa-angle-double-right"></i></h5></a>
                </div>
            </div>
        </div>
    </div>




@endsection

@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function(){


            let oTable = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{!! url('user/sms/get-recurring-sms-data/') !!}',
                },
                columns: [
                    {data: 'id', name: 'id',orderable: false, searchable: false},
                    {data: 'sender', name: 'sender'},
                    {data: 'total_recipients', name: 'total_recipients'},
                    {data: 'recurring', name: 'recurring'},
                    {data: 'recurring_date', name: 'recurring_date'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                language: {
                    url: '{!! url("assets/libs/data-table/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language.".lang") !!}'
                },
                responsive: true,
                dom: 'lBrtip',
                lengthMenu: [[10,25, 100, -1], [10,25, 100, "All"]],
                pageLength: 10,
                order: [[ 1, "desc" ]],
                buttons: [
                    {
                        extend: 'excel',
                        text: '<span class="fa fa-file-excel-o"></span> {!! language_data('Excel',Auth::guard('client')->user()->lan_id) !!}',
                        exportOptions: {
                            columns: [1,2,3,4,5],
                            modifier: {
                                search: 'applied',
                                order: 'applied',
                            }
                        }
                    },
                    {
                        extend: 'pdf',
                        text: '<span class="fa fa-file-pdf-o"></span> {!! language_data('PDF',Auth::guard('client')->user()->lan_id) !!}',
                        exportOptions: {
                            columns: [1,2,3,4,5],
                            modifier: {
                                search: 'applied',
                                order: 'applied',
                            }
                        }
                    },
                    {
                        extend: 'csv',
                        text: '<span class="fa fa-file-excel-o"></span> {!! language_data('CSV',Auth::guard('client')->user()->lan_id) !!}',
                        exportOptions: {
                            columns: [1,2,3,4,5],
                            modifier: {
                                search: 'applied',
                                order: 'applied',
                            }
                        }
                    },
                    {
                        extend: 'print',
                        text: '<span class="fa fa-print"></span> {!! language_data('Print',Auth::guard('client')->user()->lan_id) !!}',
                        exportOptions: {
                            columns: [1,2,3,4,5],
                            modifier: {
                                search: 'applied',
                                order: 'applied',
                            }
                        }
                    }
                ],
            });


            $("#bulkDelete").on('click',function() { // bulk checked
                let status = this.checked;
                $(".deleteRow").each( function() {
                    $(this).prop("checked",status);
                });
            });

            let deleteTriger =  $('#deleteTriger');
            deleteTriger.hide();

            $( ".panel" ).delegate( ".deleteRow, #bulkDelete", "change",function (e) {
                $('#deleteTriger').toggle($('.deleteRow:checked').length > 0);
            });


            deleteTriger.on("click", function(event){ // triggering delete one by one
                if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
                    let ids = [];
                    $('.deleteRow').each(function(){
                        if($(this).is(':checked')) {
                            ids.push($(this).val());
                        }
                    });
                    let ids_string = ids.toString();  // array to string conversion

                    $.ajax({
                        type: "POST",
                        url: '{!! url('user/sms/bulk-recurring-sms-delete/') !!}',
                        data: {data_ids:ids_string},
                        success: function(result) {
                            oTable.draw(); // redrawing datatable
                        },
                        async:false
                    });
                }
            });

            let Body = $( "body" );

            Body.delegate( ".stop-recurring", "click",function (e) {
                e.preventDefault();
                let id = this.id;
                bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        let _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/stop-recurring-sms/" + id;
                    }
                });
            });


            Body.delegate( ".start-recurring", "click",function (e) {
                e.preventDefault();
                let id = this.id;
                bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        let _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/start-recurring-sms/" + id;
                    }
                });
            });

            /*For Delete SMS*/
            Body.delegate( ".cdelete", "click",function (e) {
                e.preventDefault();
                let id = this.id;
                bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        let _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/delete-recurring-sms/" + id;
                    }
                });
            });

        });
    </script>
@endsection