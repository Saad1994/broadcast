
<div class="row" style="padding-top: 20px">
    <div class="col-md-12">
        <h2>Manage Message</h2>
    </div>
    <div class="col-md-3">
        <!-- Card -->
        <div class="card mb-4">
            <!--Card content-->
            <div class="card-body">
                <!--Title-->
                <h4 class="card-title">Use A Phone</h4>
                <!--Text-->
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <button type="button" class="btn btn-light-blue btn-md"><i class="fa fa-phone"> Record Message</i></button>
            </div>
        </div>
        <!-- Card -->
    </div>
    <div class="col-md-3">
        <!-- Card -->
        <div class="card mb-4">
            <!--Card content-->
            <div class="card-body">
                <!--Title-->
                <h4 class="card-title">Upload .MP3/.WAV </h4>
                <!--Text-->
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <button type="button" class="btn btn-light-blue btn-md"><i class="fa fa-file"> Upload File</i></button>
            </div>
        </div>
        <!-- Card -->
    </div>
    <div class="col-md-3">
        <!-- Card -->
        <div class="card mb-4">
            <!--Card content-->
            <div class="card-body">
                <!--Title-->
                <h4 class="card-title">Text to Speech Tool</h4>
                <!--Text-->
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <button type="button" class="btn btn-light-blue btn-md"><i class="fa fa-microphone"> Use this Tool</i></button>
            </div>
        </div>
        <!-- Card -->
    </div>
    <div class="col-md-3">
        <!-- Card -->
        <div class="card mb-4">
            <!--Card content-->
            <div class="card-body">
                <!--Title-->
                <h4 class="card-title">Phone-In System</h4>
                <!--Text-->
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <button type="button" class="btn btn-light-blue btn-md"><i class="fa fa-phone"> More Info</i></button>
            </div>
        </div>
        <!-- Card -->
    </div>

</div>