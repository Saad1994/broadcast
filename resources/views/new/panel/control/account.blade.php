@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Account Overview</h3>
        <div class="row" style="padding-top: 10px">

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> {{language_data('Invoices History',Auth::guard('client')->user()->lan_id)}}</h4>
                        <div class="card-text">
                            {!! $invoices_json->render() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{language_data('SMS Success History',Auth::guard('client')->user()->lan_id)}}</h4>
                        <div class="card-text">
                            {!! $sms_status_json->render() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Monthly Plan</h4>
                        <div class="card-text">
                            {{language_data('SMS Balance',Auth::guard('client')->user()->lan_id)}}   <strong>{{Auth::guard('client')->user()->sms_limit}}</strong>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="padding-top: 10px">
            <div class="col-md-6">
                <h3>{{language_data('Recent 5 Invoices',Auth::guard('client')->user()->lan_id)}}</h3>
                <table class="table table-hover table-ultra-responsive">
                    <thead>
                    <tr>
                        <th style="width: 45px;">{{language_data('SL',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 20px;">{{language_data('Amount',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 20px;">{{language_data('Due Date',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 15px;">{{language_data('Status',Auth::guard('client')->user()->lan_id)}}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($recent_five_invoices as $inv)
                        <tr>
                            <td data-label="{{language_data('SL',Auth::guard('client')->user()->lan_id)}}">
                                <p> {{$loop->iteration}} </p>
                            </td>
                            <td data-label="{{language_data('Amount',Auth::guard('client')->user()->lan_id)}}"><p><a href="{{url('user/invoices/view/'.$inv->id)}}">{{us_money_format($inv->total)}}</a> </p>
                            </td>
                            <td data-label="{{language_data('Due Date',Auth::guard('client')->user()->lan_id)}}"><p>{{get_date_format($inv->duedate)}}</p></td>
                            @if($inv->status=='Paid')
                                <td data-label="{{language_data('Status',Auth::guard('client')->user()->lan_id)}}"><p class="label label-success label-xs">{{language_data('Paid',Auth::guard('client')->user()->lan_id)}}</p></td>
                            @elseif($inv->status=='Unpaid')
                                <td data-label="{{language_data('Status',Auth::guard('client')->user()->lan_id)}}"><p class="label label-warning label-xs">{{language_data('Unpaid',Auth::guard('client')->user()->lan_id)}}</p></td>
                            @elseif($inv->status=='Partially Paid')
                                <td data-label="{{language_data('Status',Auth::guard('client')->user()->lan_id)}}"><p class="label label-info label-xs">{{language_data('Partially Paid',Auth::guard('client')->user()->lan_id)}}</p></td>
                            @else
                                <td data-label="{{language_data('Status',Auth::guard('client')->user()->lan_id)}}"><p class="label label-danger label-xs">{{language_data('Cancelled',Auth::guard('client')->user()->lan_id)}}</p></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h3>{{language_data('Recent 5 Support Tickets',Auth::guard('client')->user()->lan_id)}}</h3>
                <table class="table table-hover table-ultra-responsive">
                    <thead>
                    <tr>
                        <th style="width: 30%;">{{language_data('SL',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 50%;">{{language_data('Subject',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 20%;">{{language_data('Date',Auth::guard('client')->user()->lan_id)}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recent_five_tickets as $rtic)
                        <tr>
                            <td data-label="{{language_data('SL',Auth::guard('client')->user()->lan_id)}}">
                                <p>{{$loop->iteration}}</p>
                            </td>
                            <td data-label="{{language_data('Subject',Auth::guard('client')->user()->lan_id)}}">
                                <p><a href="{{url('user/tickets/view-ticket/'.$rtic->id)}}">{{$rtic->subject}}</a></p>
                            </td>
                            <td data-label="{{language_data('Date',Auth::guard('client')->user()->lan_id)}}">
                                <p>{{get_date_format($rtic->date)}}</p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
