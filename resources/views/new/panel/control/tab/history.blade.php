@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Credits</h3>
        <div class="row" style="padding-top: 10px">
            @include('notification.notify')
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link "  href="{{'credit'}}" >Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active "  href="{{url('history')}}" >{{language_data('All Invoices',Auth::guard('client')->user()->lan_id)}}</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">

                        <table class="table data-table table-striped table-bordered table-responsive-md " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="width: 10%;">#</th>
                                <th style="width: 10%;">{{language_data('Amount',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 15%;">{{language_data('Invoice Date',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 15%;">{{language_data('Due Date',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 10%;">{{language_data('Status',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 15%;">{{language_data('Type',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 30%;">{{language_data('Manage',Auth::guard('client')->user()->lan_id)}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $in)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{us_money_format($in->total)}}</td>
                                    <td>{{get_date_format($in->created)}}</td>
                                    <td>{{get_date_format($in->duedate)}}</td>
                                    <td>
                                        @if($in->status=='Unpaid')
                                            <span class="label label-warning">{{language_data('Unpaid',Auth::guard('client')->user()->lan_id)}}</span>
                                        @elseif($in->status=='Paid')
                                            <span class="label label-success">{{language_data('Paid',Auth::guard('client')->user()->lan_id)}}</span>
                                        @elseif($in->status=='Cancelled')
                                            <span class="label label-danger">{{language_data('Cancelled',Auth::guard('client')->user()->lan_id)}}</span>
                                        @else
                                            <span class="label label-info">{{language_data('Partially Paid',Auth::guard('client')->user()->lan_id)}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($in->recurring=='0')
                                            <span class="label label-success"> {{language_data('Onetime',Auth::guard('client')->user()->lan_id)}}</span>
                                        @else
                                            <span class="label label-info"> {{language_data('Recurring',Auth::guard('client')->user()->lan_id)}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('user/invoices/view/'.$in->id)}}" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> {{language_data('View',Auth::guard('client')->user()->lan_id)}}</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection