@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Credits</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active"  href="{{'user/sms/purchase-sms-plan'}}" >Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link "  href="{{url('user/invoices/all')}}" >History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                        <div class="card-deck mb-3 text-center">
                            <div class="card mb-6 box-shadow">
                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal">Monthly Plans</h4>
                                </div>
                                <div class="card-body">
                                    <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>10 users included</li>
                                        <li>2 GB of storage</li>
                                        <li>Email support</li>
                                        <li>Help center access</li>
                                    </ul>
                                    <button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button>
                                </div>
                            </div>
                            <div class="card mb-6 ">
                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal">Enterprise</h4>
                                </div>
                                <div class="card-body">
                                    <h1 class="card-title pricing-card-title">$29 <small class="text-muted">/ mo</small></h1>
                                    <ul class="list-unstyled mt-3 mb-4">
                                        <li>30 users included</li>
                                        <li>15 GB of storage</li>
                                        <li>Phone and email support</li>
                                        <li>Help center access</li>
                                    </ul>
                                    <button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection