@extends('newclient')

@section('content')
    <div class="container">
        @yield('modal')
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <!-- Classic tabs -->
                <div class="classic-tabs">
                    <!-- Nav tabs -->
                    <div class="tabs-wrapper">
                        <ul class="nav tabs-blue-grey">
                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('account')}}">
                                    <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                                    <br> Overview</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('user/sms/purchase-sms-plan')}}">
                                    <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
                                    <br> Credit/Plans</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('user/edit-profile')}}">
                                    <i class="fa fa-wrench fa-2x" aria-hidden="true"></i>
                                    <br> Profile Settings</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('user/sms/buy-unit')}}">
                                    <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                    <br> Billing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('/user/sms/sender-id-management')}}">
                                    <i class="fa fa-bell fa-2x" aria-hidden="true"></i>
                                    <br>Sender ID Management</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('user/tickets/all')}}">
                                    <i class="fa fa-hand-peace-o fa-2x" aria-hidden="true"></i>
                                    <br> Support</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link waves-light" href="{{url('integration')}}">
                                    <i class="fa fa-gears fa-2x" aria-hidden="true"></i>
                                    <br> Integration</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Tab panels -->
                    <div class="tab-content card">
                        <div class="tab-pane fade in show active" role="tabpanel">
                            @yield('data')
                        </div>
                    </div>
                </div>
                <!-- Classic tabs -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('#credit').DataTable();
            $('#block').DataTable();
            $('#billinghistory').DataTable();
            $('.dataTables_wrapper').find('label').each(function () {
                $(this).parent().append($(this).children());
            });
            $('.dataTables_filter').find('input').each(function () {
                $('input').attr("placeholder", "Search");
                $('input').removeClass('form-control-sm');
            });
            $('.dataTables_length').addClass('d-flex flex-row');
            $('.dataTables_filter').addClass('md-form');
            $('select').addClass('mdb-select');
            $('.mdb-select').material_select();
            $('.mdb-select').removeClass('form-control form-control-sm');
            $('.dataTables_filter').find('label').remove();


        });
    </script>
@endsection