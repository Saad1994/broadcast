@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Account Overview</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-4">
                    <div class="card-body">
                        <h4 class="card-title">Integration Methods</h4>
                        <nav class="nav flex-column ">
                            <a class="nav-link " href="#">Mail Chimp</a>
                            <a class="nav-link" href="#">Slack</a>
                            <a class="nav-link" href="#">Zapier</a>
                            <a class="nav-link" href="#">AWeber</a>
                        </nav>
                    </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Rest API</h4>
                        <div class="card-text">
                            <p>Our robust API gives you the ability to offer our services right from your own website or applications. To get started visit our <a href="#">API Documentation</a></p> <br>

                            <h4>API Info</h4>
                             API Key: <strong> 3123124325324645dafe5</strong>
                            <form class="form" action="#">
                                <div class="md-form">
                                    <input  type="text" id="url" class="form-control">
                                    <label for="url">API Postback URL:</label>
                                </div>
                                <input type="submit" class="btn btn-warning" value="Update URL">
                            </form>

                            <h5 style="padding-top: 10px">Need Help Or Support with API</h5>
                            <p>some@email.com</p>
                            <p>+1-778-885-7441</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
