@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Billing Method and History</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">Billing Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                        @include('new.panel.control.tab.billingMethod')
                    </div>


                    <div class="tab-pane fade" id="panel2" role="tabpanel">
                        @include('new.panel.control.tab.billinghistory')
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
