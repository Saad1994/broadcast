@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        <h3>Credits</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link "  href="{{'credit'}}" >Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link "  href="{{url('user/invoices/all')}}" >History</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                        @yield('data')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
