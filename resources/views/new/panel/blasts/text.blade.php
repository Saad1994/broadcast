@extends('newclient')


@section('style')

    @endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            <ul class="nav  md-pills pills-primary flex-column" role="tablist">

                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel22" role="tab">Add/Select Message
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">Choose Contact
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel24" role="tab">Additional Settings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel25" role="tab">Confirm Broadcast Settings
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-9">

            <form method="post" id="text-sms">
            <!-- Tab panels -->
            <div class="tab-content vertical">
                <!--Panel 2-->
                <div class="tab-pane fade in show active" id="panel22" role="tabpanel">
                    @include('new.panel.blasts.textTab.tab2')
                </div>
                <!--/.Panel 2-->
                <!--Panel 3-->
                <div class="tab-pane fade" id="panel23" role="tabpanel">
                    <div class="container">
                        @include('new.panel.blasts.textTab.tab3')
                    </div>
                </div>
                <!--/.Panel 3-->
                <div class="tab-pane fade" id="panel24" role="tabpanel">
                    @include('new.panel.blasts.textTab.tab4')
                </div>

                <div class="tab-pane fade" id="panel25" role="tabpanel">
                    @include('new.panel.blasts.textTab.tab5')
                </div>

            </div>

            </form>
        </div>
    </div>



@endsection

@section('script')

@endsection
