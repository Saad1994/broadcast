
    <div class="container">
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav md-pills nav-justified pills-info">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel11" role="tab">Individuals (500)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel12" role="tab">Contact Groups</a>
                            </li>
                        </ul>
                        <div class="tab-content">

                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel11" role="tabpanel">
                                <!--Table-->
                                <table class="table">

                                    <!--Table head-->
                                    <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Miscellaneous</th>
                                        <th>Phone Number</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <!--Table head-->

                                    <!--Table body-->
                                    <tbody>
                                    <tr>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>-</td>
                                        <td>0335241451</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>-</td>
                                        <td>0335241451</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>-</td>
                                        <td>0335241451</td>
                                        <td>-</td>
                                    </tr>
                                    </tbody>
                                    <!--Table body-->

                                </table>
                                <!--Table-->
                                <a href="#" class="btn btn-warning">Add New Contact</a>

                            </div>
                            <!--/.Panel 1-->

                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel12" role="tabpanel">
                                <table class="table">

                                    <!--Table head-->
                                    <thead>
                                    <tr>
                                        <th>Group Name</th>
                                        <th>Number of Contacts</th>
                                        <th>Created at</th>
                                    </tr>
                                    </thead>
                                    <!--Table head-->

                                    <!--Table body-->
                                    <tbody>
                                    <tr>
                                        <td>US Number Group</td>
                                        <td>520</td>
                                        <td>23/03/2018</td>
                                    </tr>
                                    <tr>
                                        <td>US Number Group</td>
                                        <td>520</td>
                                        <td>23/03/2018</td>
                                    </tr>
                                    <tr>
                                        <td>US Number Group</td>
                                        <td>520</td>
                                        <td>23/03/2018</td>
                                    </tr>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <a href="#" class="btn btn-warning">Create a New Group</a>
                            </div>
                            <!--/.Panel 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
