<div class="container">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form">
            <div class="md-form">
                <input type="text" id="form1" class="form-control">
                <label for="form1" >Broadcast Name</label>
            </div>

            <!--Blue select-->
            <select class="mdb-select">
                <option value="" disabled selected>Choose a Number</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            <!--/Blue select-->

            <select class="mdb-select">
                <option value="" disabled selected>Pick a keyword to send your message from</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>

            <div class="form-group">
                <label for="exampleFormControlTextarea2">Type your message</label>
                <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3"></textarea>
            </div>

            <a href="#" class="btn btn-primary">Next Step</a>

        </form>
                </div>
            </div>
        </div>
    </div>
</div>