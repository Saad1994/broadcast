<div class="container">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form">
                        <h5>Additional Sending Options</h5>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="checkbox100" >
                            <label class="form-check-label" for="checkbox100">Email this to my contacts as well.</label>
                        </div>
                        <h5>When to place this Broadcast</h5>
                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio100">
                            <label class="form-check-label" for="radio100">Send Broadcast Now</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio101" checked>
                            <label class="form-check-label" for="radio101">Send Broadcast to a later date</label>
                        </div>
                        <div class="md-form">
                            <input placeholder="Selected date" type="text" id="date-picker-example" class="form-control datepicker">
                            <label for="date-picker-example">Date Picker</label>
                        </div>
                        <div class="md-form">
                            <input placeholder="Selected time" type="text" id="input_starttime" class="form-control timepicker">
                            <label for="input_starttime">Time Picker</label>
                        </div>

                        <a href="#" class="btn btn-primary">Next Step</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>