@extends('new.main.app')

@section('content')
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>Broadcast Confirmation Report</h3>
                    <table class="table ">
                        <tbody>
                        <tr>
                            <th>Confirmation</th>
                            <th>Text Broadcast Name</th>
                            <th>SMS Keyword</th>
                            <th>Message to Send</th>
                            <th>Contact to text</th>
                            <th>Credit Cost</th>
                            <th>Time & Date</th>
                        </tr>
                        <tr>
                            <td>DT10401452</td>
                            <td>test</td>
                            <td>FireArmClass</td>
                            <td>test</td>
                            <td>1 Contact</td>
                            <td>1</td>
                            <td>May 23, 2018 5:54PM Estern Time</td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-success">Successfully Created</a>

                </div>
            </div>
        </div>
    </div>
@endsection
