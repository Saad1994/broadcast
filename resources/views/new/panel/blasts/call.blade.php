@extends('newclient')

@section('content')

    <div class="row">
        <div class="col-md-3">
            <ul class="nav  md-pills pills-primary flex-column" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel31" role="tab">Select Advance Features
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel22" role="tab">Add/Select Message
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel23" role="tab">Choose Contact
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel24" role="tab">Additional Settings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel25" role="tab">Confirm Broadcast Settings
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            <!-- Tab panels -->
            <div class="tab-content vertical">

                <div class="tab-pane fade in show active" id="panel31" role="tabpanel">
                    @include('new.panel.blasts.callTab.tab6')
                </div>
                <!--Panel 2-->
                <div class="tab-pane fade" id="panel22" role="tabpanel">
                    @include('new.panel.blasts.callTab.selectRecording')
                </div>
                <!--/.Panel 2-->
                <!--Panel 3-->
                <div class="tab-pane fade" id="panel23" role="tabpanel">
                    <div class="container">
                        @include('new.panel.blasts.callTab.tab3')
                    </div>
                </div>
                <!--/.Panel 3-->
                <div class="tab-pane fade" id="panel24" role="tabpanel">
                    @include('new.panel.blasts.callTab.tab4')
                </div>

                <div class="tab-pane fade" id="panel25" role="tabpanel">
                    @include('new.panel.blasts.callTab.tab5')
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.mdb-select').material_select();
            $('.datepicker').pickadate();
            $('#input_starttime').pickatime({});

        });
    </script>

@endsection
