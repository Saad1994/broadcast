<div class="container">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create Call Broadcast</h4>
                    <div class="card-text">
                        <ul>
                            <li> Easy to setup and create messages</li>
                            <li> Send all the messages you want</li>
                            <li> Schedule broadcast for anytime you want</li>
                        </ul>
                        <div class="radio">
                            <input type="radio" name="optradio">Use Call Broadcast Service
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create Text Broadcast</h4>
                    <div class="card-text">
                        <ul>
                            <li> Easy to setup and create</li>
                            <li> Ability to use keywords and 2-way</li>
                            <li> Use same credits as calls</li>
                        </ul>
                        <div class="radio">
                            <input type="radio" name="optradio">Use Text Message Broadcast
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 10px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                    <!-- Title -->
                    <h4 class="card-title">Create a Free Call Broadcast | Try it </h4>
                    <!-- Text -->
                    <div class="card-text">
                        <div class="radio">
                            <input type="radio" name="optradio">Create a free Broadcast | Try it
                        </div>
                        <br> Terms and Conditions are applied.</div>
                    <!-- Button -->
                    <a href="#" class="btn btn-primary">New Step</a>
                </div>
            </div>
        </div>
    </div>
</div>

