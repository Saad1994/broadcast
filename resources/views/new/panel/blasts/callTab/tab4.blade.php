<div class="container">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form">
                        <h5>Additional Sending Options</h5>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="checkbox100" >
                            <label class="form-check-label" for="checkbox100">Email this to my contacts as well.</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="checkbox101" >
                            <label class="form-check-label" for="checkbox101">Send message to my own phone number.</label>
                        </div>
                        <h5>Caller ID Display</h5>
                        <p>This number will be displayed as caller ID.</p>
                        <select class="mdb-select colorful-select dropdown-primary">
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                            <option value="4">Option 4</option>
                            <option value="5">Option 5</option>
                        </select>

                        <h5>Answering Machine Detection Setting</h5>
                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio200">
                            <label class="form-check-label" for="radio200">Use Answering Machine Detection</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio201" checked>
                            <label class="form-check-label" for="radio201">Do not use answering machine detection</label>
                        </div>



                        <h5>When to Place this Broadcast</h5>
                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio100">
                            <label class="form-check-label" for="radio100">Send Broadcast Now</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" name="group100" type="radio" id="radio101" checked>
                            <label class="form-check-label" for="radio101">Send Broadcast to a later date</label>
                        </div>
                        <div class="md-form">
                            <input placeholder="Selected date" type="text" id="date-picker-example" class="form-control datepicker">
                            <label for="date-picker-example">Date Picker</label>
                        </div>
                        <div class="md-form">
                            <input placeholder="Selected time" type="text" id="input_starttime" class="form-control timepicker">
                            <label for="input_starttime">Time Picker</label>
                        </div>

                        <a href="#" class="btn btn-primary">Next Step</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>