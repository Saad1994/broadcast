<div class="container">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">

                        <!--Table head-->
                        <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Number of Contacts</th>
                            <th>Created at</th>
                        </tr>
                        </thead>
                        <!--Table head-->

                        <!--Table body-->
                        <tbody>
                        <tr>
                            <td>US Number Group</td>
                            <td>520</td>
                            <td>23/03/2018</td>
                        </tr>
                        <tr>
                            <td>US Number Group</td>
                            <td>520</td>
                            <td>23/03/2018</td>
                        </tr>
                        <tr>
                            <td>US Number Group</td>
                            <td>520</td>
                            <td>23/03/2018</td>
                        </tr>
                        </tbody>
                        <!--Table body-->
                    </table>
                    <a href="#" class="btn btn-warning">Create New Message</a>
                </div>
            </div>

        </div>
    </div>
</div>