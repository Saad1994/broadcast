<div class="modal fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Create Content Group</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" role="form" method="post" action="{{url('user/post-phone-book')}}">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <input type="text" id="group_name" class="form-control validate"  name="list_name">
                        <label data-error="wrong" data-success="right" for="group_name">{{language_data('List name',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>

                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-default"> {{language_data('Add',Auth::guard('client')->user()->lan_id)}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
