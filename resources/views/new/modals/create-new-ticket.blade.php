<div class="modal fade" id="createTicket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">{{language_data('Create New Ticket',Auth::guard('client')->user()->lan_id)}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" role="form" method="post"  action="{{ url('user/tickets/post-ticket') }}">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <input type="text" id="subject" class="form-control validate" name="subject">
                        <label data-error="wrong" data-success="right" for="subject">{{language_data('Subject',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form mb-5">
                        <label data-error="wrong" data-success="right" for="message">{{language_data('Message',Auth::guard('client')->user()->lan_id)}}</label>
                        <textarea class="form-control rounded-0"  rows="10" name="message"></textarea>
                    </div>
                    <div class="md-form mb-5">
                        <select class="mdb-select" name="did" data-live-search="true">
                            <option value="" disabled selected>{{language_data('Department',Auth::guard('client')->user()->lan_id)}}</option>
                            @foreach($sd as $d)
                                <option value="{{$d->id}}">{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-default"> {{language_data('Add',Auth::guard('client')->user()->lan_id)}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
