<div class="modal fade" id="quickAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Quick Add</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" role="form" method="post" action="{{url('user/post-multiple-contact')}}">

                <div class="modal-body mx-3">

                    <div class="md-form mb-5">
                        <select class="mdb-select" data-live-search="true" name="group_name">
                            <option value="" disabled selected>Choose your group</option>
                            @foreach($phone_book as $pb)
                                <option value="{{$pb->id}}">{{$pb->group_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="md-form mb-5">
                        <select class="mdb-select" name="country_code" data-live-search="true">
                            <option value="" disabled selected>Country Code</option>
                            @foreach($country_code as $code)
                                <option value="{{$code->country_code}}"
                                        @if(app_config('Country') == $code->country_name) selected @endif >{{$code->country_name}}
                                    ({{$code->country_code}})
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="md-form mb-5">
                        <label>{{language_data('Paste Numbers',Auth::guard('client')->user()->lan_id)}}</label>
                        <textarea class="form-control" rows="5" name="import_numbers"></textarea>
                        <span class="help">{{language_data('Insert number with comma',Auth::guard('client')->user()->lan_id)}} (,) Ex. 8801670000000,8801721000000</span>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-success btn-sm pull-right"><i
                                class="fa fa-plus"></i> {{language_data('Add',Auth::guard('client')->user()->lan_id)}} </button>

                </div>

            </form>
        </div>
    </div>
</div>
