<div class="modal fade" id="uploadDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">{{language_data('Import Contact By File',Auth::guard('client')->user()->lan_id)}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body mx-3">
                <a href="{{url('user/sms/download-contact-sample-file')}}" class="btn btn-complete"><i class="fa fa-download"></i> {{language_data('Download Sample File',Auth::guard('client')->user()->lan_id)}}</a>


                <form class="" id="send-sms-file-form" role="form" method="post" action="{{url('user/post-import-file-contact')}}" enctype="multipart/form-data">

                    <div class="md-form mb-5">
                            <div class="file-field">
                                <div class="btn btn-primary btn-sm float-left">
                                    <span>{{language_data('Import Numbers',Auth::guard('client')->user()->lan_id)}}</span>
                                    <input type="file"  name="import_numbers" @change="handleImportNumbers">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload your file">
                                </div>
                            </div>
                    </div>


                    <div class="md-form mb-5">
                        <div class="form-check">
                            <input type="checkbox" name="header_exist"  :checked="form.header_exist" v-model="form.header_exist" class="filled-in form-check-input" id="checkbox101" checked="checked">
                            <label class="form-check-label" for="checkbox101">{{language_data('First Row As Header',Auth::guard('client')->user()->lan_id)}}</label>
                        </div>
                    </div>

                    <div class="md-form mb-5">
                        <select class="mdb-select" data-live-search="true" name="group_name">
                            <option value="" disabled selected>{{language_data('Import List into',Auth::guard('client')->user()->lan_id)}}</option>
                            @foreach($phone_book as $pb)
                                <option value="{{$pb->id}}">{{$pb->group_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="md-form mb-5">
                    <select class="mdb-select" name="country_code" data-live-search="true">
                        <option value="" disabled selected>{{language_data('Exist on phone number',Auth::guard('client')->user()->lan_id)}}</option>
                        @foreach($country_code as $code)
                            <option value="{{$code->country_code}}" @if(app_config('Country') == $code->country_name) selected @endif >{{$code->country_name}} ({{$code->country_code}})</option>
                        @endforeach
                    </select>
                </div>


                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('Phone Number',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="number_column" name="number_column"  data-live-search="true" v-model="number_column">
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>

                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('Email',Auth::guard('client')->user()->lan_id)}} {{language_data('Address',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="email_address_column" name="email_address_column"  data-live-search="true">
                        <option :value="0"></option>
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>

                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('User name',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="user_name_column" name="user_name_column"  data-live-search="true">
                        <option :value="0"></option>
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>


                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('Company',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="company_column" name="company_column"  data-live-search="true">
                        <option :value="0"></option>
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>


                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('First name',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="first_name_column" name="first_name_column"  data-live-search="true">
                        <option :value="0"></option>
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>

                <div class=" md-form mb-5" v-show="number_columns.length > 0">
                    <label>{{language_data('Last name',Auth::guard('client')->user()->lan_id)}} {{language_data('Column',Auth::guard('client')->user()->lan_id)}}</label>
                    <select class="mdb-select" ref="last_name_column" name="last_name_column"  data-live-search="true">
                        <option :value="0"></option>
                        <option v-for="column in number_columns" :value="column.key" v-text="column.value"></option>
                    </select>
                </div>




                <div id='uploadContact' style='display:none' class="md-form mb-5">
                    <label>{{language_data('Contact importing.. Please wait',Auth::guard('client')->user()->lan_id)}}</label>
                    <div class="progress">
                        <div class="progress-bar-indeterminate"></div>
                    </div>
                </div>


                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" id="submitContact" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add',Auth::guard('client')->user()->lan_id)}} </button>
            </form>

            </div>

        </div>
    </div>
</div>


