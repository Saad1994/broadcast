<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> youtextpro.com</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('new_assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('new_assets/css/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('new_assets/css/style.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>

    @yield('style')
</head>
<body>
<div class="container" style="min-height:100vh" >
    <nav class="navbar navbar-expand-lg navbar-dark blue scrolling-navbar">
        <a class="navbar-brand" href="#">Welcome <strong>Jay King</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link">300 User Monthly Plan <i class="fa fa-user-circle"></i></a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-success">Upgrade</a>
                </li>
            </ul>
        </div>
    </nav>
    @yield('content')
</div>
<!-- Footer -->
<div class="container">
    <footer class="page-footer font-small blue pt-4 mt-4">
        <!-- Copyright -->

        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="#"> youtextpro.com</a>
        </div>
        <!-- Copyright -->
    </footer>
</div><!-- Footer -->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{asset('new_assets/js/jquery-3.3.1.min.js')}}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('new_assets/js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('new_assets/js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('new_assets/js/mdb.min.js')}}"></script>


@yield('scripts')
</body>

</html>
