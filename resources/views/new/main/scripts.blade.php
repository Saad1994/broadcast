<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{asset('new_assets/js/jquery-3.3.1.min.js')}}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('new_assets/js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('new_assets/js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('new_assets/js/mdb.min.js')}}"></script>


@yield('scripts')