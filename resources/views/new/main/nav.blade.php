<nav class="navbar navbar-expand-lg navbar-dark blue scrolling-navbar">
    <a class="navbar-brand" href="#">Welcome <strong>Jay King</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

        </ul>
        <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
                <a class="nav-link">300 User Monthly Plan <i class="fa fa-user-circle"></i></a>
            </li>
            <li class="nav-item">
                <a class="btn btn-success">Upgrade</a>
            </li>
        </ul>
    </div>
</nav>
