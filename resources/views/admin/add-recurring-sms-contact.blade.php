@extends('admin')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Add Contact')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Add Contact')}}</h3>
                        </div>
                        <div class="panel-body">

                            <form class="" role="form" method="post" action="{{url('sms/post-recurring-sms-contact')}}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>{{language_data('Country Code')}}</label>
                                    <select class="selectpicker form-control" name="country_code" data-live-search="true">
                                        <option value="0">{{language_data('Exist on phone number')}}</option>
                                        @foreach($country_code as $code)
                                            <option value="{{$code->country_code}}" @if(app_config('Country') == $code->country_name) selected @endif >{{$code->country_name}} ({{$code->country_code}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Recipients')}}</label>
                                    <textarea class="form-control" rows="4" name="recipients" id="recipients"></textarea>
                                    <span class="help text-uppercase">{{language_data('Insert number with comma')}} (,) Ex. 8801721900000,8801721900001</span>
                                    <span class="help text-uppercase pull-right">{{language_data('Total Number Of Recipients')}}
                                        : <span class="number_of_recipients bold text-success m-r-5">0</span></span>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Remove Duplicate')}}</label>
                                    <select class="selectpicker form-control" name="remove_duplicate">
                                        <option value="yes">{{language_data('Yes')}}</option>
                                        <option value="no">{{language_data('No')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Message Type')}}</label>
                                    <select class="selectpicker form-control message_type" name="message_type" disabled>
                                        <option value="plain" @if($recurring->type == 'plain') selected @endif>{{language_data('Plain')}}</option>
                                        <option value="unicode" @if($recurring->type == 'unicode') selected @endif>{{language_data('Unicode')}}</option>
                                        <option value="voice" @if($recurring->type == 'voice') selected @endif>{{language_data('Voice')}}</option>
                                        <option value="mms" @if($recurring->type == 'mms') selected @endif>{{language_data('MMS')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Message')}}</label>
                                    <textarea class="form-control" name="message" rows="5" id="message"></textarea>
                                    <span class="help text-uppercase" id="remaining">160 {{language_data('characters remaining')}}</span>
                                    <span class="help text-success" id="messages">1 {{language_data('message')}} (s)</span>
                                </div>

                                <input type="hidden" value="{{$recurring->id}}" name="recurring_id">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add')}} </button>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}

    <script>
        $(document).ready(function () {

            var $get_msg = $("#message"),
                $remaining = $('#remaining'),
                $messages = $remaining.next(),
                message_type = 'plain',
                maxCharInitial = 160,
                maxChar = 157,
                messages = 1;


          function get_character() {
            var totalChar = $get_msg[0].value.length;
            var remainingChar = maxCharInitial;

            if ( totalChar <= maxCharInitial ) {
              remainingChar = maxCharInitial - totalChar;
              messages = 1;
            } else {
              totalChar = totalChar - maxCharInitial;
              messages = Math.ceil( totalChar / maxChar );
              remainingChar = messages * maxChar - totalChar;
              messages = messages + 1;
            }

            $remaining.text(remainingChar + " {!! language_data('characters remaining') !!}");
            $messages.text(messages + " {!! language_data('message') !!}"+ '(s)');
          }

          $('.send-mms').hide();
          $('.message_type').on('change', function () {
            message_type = $(this).val();

            if (message_type == 'unicode') {
              maxCharInitial = 70;
              maxChar = 67;
              messages = 1;
              $('.send-mms').hide();
              get_character()
            }

            if (message_type == 'plain' || message_type == 'voice') {
              maxCharInitial = 160;
              maxChar = 160;
              messages = 1;
              $('.send-mms').hide();
              get_character()
            }
          });

            $get_msg.keyup(get_character);
        });
    </script>
@endsection