<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{app_config('AppTitle')}}</title>
    <link rel="icon" type="image/x-icon"  href="<?php echo asset(app_config('AppFav')); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--Global StyleSheet Start--}}
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    {!! Html::style("assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css") !!}
    {!! Html::style("assets/libs/font-awesome/css/font-awesome.min.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify-bootstrap-3.css") !!}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('new_assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('new_assets/css/mdb.min.css')}}" rel="stylesheet">
    <link href="{{asset('new_assets/css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>

    {{--Custom StyleSheet Start--}}

    @yield('style')

    <style>
        .btn-xs, .btn-group-xs>.btn {
            padding: 2px 10px;
            font-size: 10.5px;
            line-height: 17px;
        }

    </style>


</head>



<body class="container @if(Auth::guard('client')->user()->menu_open==1) left-bar-open @endif">


    <nav class="navbar navbar-expand-lg navbar-dark blue scrolling-navbar">

        <a class="navbar-brand" href="#">Welcome <strong>{{Auth::guard('client')->user()->fname}} {{Auth::guard('client')->user()->lname}}</strong></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('test')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('account')}}">Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('user/phone-book')}}">Contact</a>
                </li>
                <li @if(Request::path()=='user/sms/history' OR Request::path()=='user/sms/view-inbox/'.view_id()) @endif class="nav-item">
                    <a class="nav-link" href="{{url('user/sms/history')}}">{{language_data('SMS History',Auth::guard('client')->user()->lan_id)}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('manage')}}">Messages</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('logout')}}">{{language_data('Logout',Auth::guard('client')->user()->lan_id)}} <i class="fa fa-power-off"></i>
                    </a>
                </li>

            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link">{{language_data('SMS Balance',Auth::guard('client')->user()->lan_id)}} : {{Auth::guard('client')->user()->sms_limit}} <i class="fa fa-user-circle"></i></a>
                </li>
                <li class="nav-item">
                    <a href="{{url('credit')}}" class="btn btn-success btn-sm">Upgrade</a>
                </li>
            </ul>
        </div>
    </nav>

    {{--Content File Start Here--}}

    @yield('content')

    {{--Content File End Here--}}

    <input type="hidden" id="_url" value="{{url('/')}}">

    <div class="container">
        <footer class="page-footer font-small blue pt-4 mt-4">
            <!-- Copyright -->

            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="#"> youtextpro.com</a>
            </div>
            <!-- Copyright -->
        </footer>
    </div><!-- Footer -->

{{--Global JavaScript Start--}}
{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language_code.".js") !!}
{!! Html::script("assets/js/scripts.js") !!}
    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('new_assets/js/jquery-3.3.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('new_assets/js/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('new_assets/js/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('new_assets/js/mdb.min.js')}}"></script>

{{--Global JavaScript End--}}

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        }
    });

    var _url=$('#_url').val();

    $('#bar-setting').click(function(e){
        e.preventDefault();
        $.post(_url+'/user/menu-open-status');
    });

    var width = $(window).width();
    if (width <= 768 ) {
        $("body").removeClass('left-bar-open')
    }
    $(document).ready(function() {
        $('.mdb-select').material_select();
    });

</script>

{{--Custom JavaScript Start--}}

@yield('script')

{{--Custom JavaScript End Here--}}
</body>

</html>