@extends('client')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Export and Import Clients',Auth::guard('client')->user()->lan_id)}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-4">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Export Clients',Auth::guard('client')->user()->lan_id)}}</h3>
                        </div>
                        <div class="panel-body">
                                <ul class="info-list">
                                    <li>
                                        <span class="info-list-title">{{language_data('Export Clients',Auth::guard('client')->user()->lan_id)}}</span><span class="info-list-des"><a href="{{url('user/export-user')}}" class="btn btn-success btn-xs">{{language_data('Export Clients as CSV',Auth::guard('client')->user()->lan_id)}}</a></span>
                                    </li>
                                    <li>
                                        <span class="info-list-title">{{language_data('Sample File',Auth::guard('client')->user()->lan_id)}}</span><span class="info-list-des"><a href="{{url('user/download-sample-csv')}}" class="btn btn-complete btn-xs">{{language_data('Download Sample File',Auth::guard('client')->user()->lan_id)}}</a> </span>
                                    </li>

                                </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Import Clients',Auth::guard('client')->user()->lan_id)}}</h3>
                        </div>
                        <div class="panel-body">

                            <form class="" role="form" method="post" action="{{url('user/post-new-user-csv')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>{{language_data('Client Group',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="selectpicker form-control" name="client_group"  data-live-search="true">
                                        <option value="0">{{language_data('None',Auth::guard('client')->user()->lan_id)}}</option>
                                        @foreach($client_groups as $cg)
                                            <option value="{{$cg->id}}">{{$cg->group_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('SMS Gateway',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="selectpicker form-control" name="sms_gateway[]"  data-live-search="true" multiple>
                                        @foreach($sms_gateways as $sg)
                                            <option value="{{$sg->id}}">{{$sg->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Import Clients',Auth::guard('client')->user()->lan_id)}}</label>
                                    <div class="form-group input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse',Auth::guard('client')->user()->lan_id)}} <input type="file" class="form-control" name="import_client">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="">
                                    </div>
                                    <p class="text-uppercase text-complete help">{{language_data('It will take few minutes. Please do not reload the page',Auth::guard('client')->user()->lan_id)}}</p>
                                </div>

                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-download"></i> {{language_data('Import',Auth::guard('client')->user()->lan_id)}} </button>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection