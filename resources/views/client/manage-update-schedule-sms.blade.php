@extends('client')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Update',Auth::guard('client')->user()->lan_id)}} {{language_data('Schedule SMS',Auth::guard('client')->user()->lan_id)}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Update',Auth::guard('client')->user()->lan_id)}} {{language_data('Schedule SMS',Auth::guard('client')->user()->lan_id)}}</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="{{url('user/sms/post-update-schedule-sms')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>{{language_data('Phone Number',Auth::guard('client')->user()->lan_id)}}</label>
                                    <span class="help">({{language_data('Remain country code at the beginning of the number',Auth::guard('client')->user()->lan_id)}})</span>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" value="{{$sh->receiver}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Schedule Time',Auth::guard('client')->user()->lan_id)}}</label>
                                    <input type="text" class="form-control dateTimePicker" name="schedule_time" value="{{date('m/d/y H:i y',strtotime($sh->submit_time))}}">
                                </div>


                                <div class="form-group">
                                    <label>{{language_data('SMS Gateway',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="selectpicker form-control" name="sms_gateway" data-live-search="true">
                                        @foreach($sms_gateways as $sg)
                                            <option @if($sg->id == $sh->use_gateway) selected @endif value="{{$sg->id}}">{{$sg->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @if(app_config('sender_id_verification') == 1)
                                    @if($sender_ids)
                                        <div class="form-group">
                                            <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                            <select class="selectpicker form-control sender_id" name="sender_id" data-live-search="true">
                                                @foreach($sender_ids as $si)
                                                    <option value="{{$si}}">{{$si}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                            <p><a href="{{url('user/sms/sender-id-management')}}" class="text-uppercase">{{language_data('Request New Sender ID',Auth::guard('client')->user()->lan_id)}}</a> </p>
                                        </div>
                                    @endif
                                @else
                                    <div class="form-group">
                                        <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                        <input type="text" class="form-control sender_id" name="sender_id">
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label>{{language_data('Message Type',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="selectpicker form-control message_type" name="message_type">
                                        <option value="plain" @if($sh->type == 'plain') selected @endif>{{language_data('Plain',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="unicode" @if($sh->type == 'unicode') selected @endif>{{language_data('Unicode',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="voice" @if($sh->type == 'voice') selected @endif>{{language_data('Voice',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="mms" @if($sh->type == 'mms') selected @endif>{{language_data('MMS',Auth::guard('client')->user()->lan_id)}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Message',Auth::guard('client')->user()->lan_id)}}</label>
                                    <textarea class="form-control" name="message" rows="5" id="message"> {{$sh->message}}</textarea>
                                    <span class="help text-uppercase" id="remaining">160 {{language_data('characters remaining',Auth::guard('client')->user()->lan_id)}}</span>
                                    <span class="help text-success" id="messages">1 {{language_data('message',Auth::guard('client')->user()->lan_id)}}(s)</span>
                                </div>


                                @if($sh->type == 'mms')
                                    <div class="form-group type_mms">
                                        <label>{{language_data('Current Media',Auth::guard('client')->user()->lan_id)}}</label>
                                        <p><a href="{{$sh->media_url}}" target="_blank">{{basename($sh->media_url)}}</a></p>
                                    </div>
                                @endif

                                <div class="form-group send-mms">
                                    <label>{{language_data('Select File',Auth::guard('client')->user()->lan_id)}}</label>
                                    <div class="form-group input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse',Auth::guard('client')->user()->lan_id)}} <input type="file" class="form-control" name="image" accept="audio/*,video/*,image/*">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>



                                <input type="hidden" value="{{$sh->id}}" name="cmd">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('update',Auth::guard('client')->user()->lan_id)}} </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}

    <script>
      $(document).ready(function () {

        var $get_msg = $('#message'),
          $remaining = $('#remaining'),
          $messages = $remaining.next(),
          message_type = 'plain',
          maxCharInitial = 160,
          maxChar = 157,
          messages = 1

        function get_character () {
          var totalChar = $get_msg[0].value.length
          var remainingChar = maxCharInitial

          if (totalChar <= maxCharInitial) {
            remainingChar = maxCharInitial - totalChar
            messages = 1
          } else {
            totalChar = totalChar - maxCharInitial
            messages = Math.ceil(totalChar / maxChar)
            remainingChar = messages * maxChar - totalChar
            messages = messages + 1
          }

            $remaining.text(remainingChar + " {!! language_data('characters remaining',Auth::guard('client')->user()->lan_id) !!}");
            $messages.text(messages + " {!! language_data('message',Auth::guard('client')->user()->lan_id) !!}"+ '(s)');
        }

        var messageType = $('.message_type');

        if (messageType.val() == 'mms'){
          $('.send-mms').show();
        } else {
          $('.send-mms').hide();
          $('.type_mms').hide();
        }

        messageType.on('change', function () {
          message_type = $(this).val()

          if (message_type == 'unicode') {
            maxCharInitial = 70
            maxChar = 67
            messages = 1
            $('.send-mms').hide();
            $('.type_mms').hide();
            get_character()
          }

          if (message_type == 'plain' || message_type == 'voice') {
            maxCharInitial = 160
            maxChar = 160
            messages = 1
            $('.send-mms').hide();
            $('.type_mms').hide();
            get_character()
          }

          if (message_type == 'mms') {
            $('.send-mms').show();
            $('.type_mms').show();
          }

        })

        $get_msg.keyup(get_character)

      })
    </script>
@endsection