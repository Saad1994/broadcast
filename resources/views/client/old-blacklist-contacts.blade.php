@extends('new.panel.contacts.contact')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('table')
    <div class="container">
        @include('notification.notify')
        <h3 style="padding-top: 10px">{{language_data('Blacklist Contacts',Auth::guard('client')->user()->lan_id)}}</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <p> This list allows you to input numbers that will not be called on any of your future blasts. They will be
                    automatically blocked even if they are listed in your blast. </p> <br>
                <p> *Before removing a number from your like, make sure you have consent from the owner.</p>
            </div>

            <div class="col-md-12">
                <table class="table data-table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 50%">{{language_data('Numbers',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 30%">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>{{language_data('Add New Contact',Auth::guard('client')->user()->lan_id)}}</h5>
                <form class="form" role="form" method="post" action="{{url('user/sms/post-blacklist-contact')}}">

                    <div class="md-form">
                        <input type="text" id="phone_number" class="form-control" name="phone_number" required>
                        <label for="phone_number" >{{language_data('Phone Number',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add',Auth::guard('client')->user()->lan_id)}} </button>
                </form>
            </div>
        </div>
    </div>
@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
            {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function(){


            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('user/sms/get-blacklist-contact/') !!}',
                columns: [
                    {data: 'numbers', name: 'numbers'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
              language: {
                url: '{!! url("assets/libs/data-table/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language.".lang") !!}'
              },
              responsive: true
            });

            /*For Delete Group*/
            $( "body" ).delegate( ".cdelete", "click",function (e) {
                e.preventDefault();
                var id = this.id;
              bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/delete-blacklist-contact/" + id;
                    }
                });
            });

        });
    </script>
@endsection