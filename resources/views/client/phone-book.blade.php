@extends('new.panel.contacts.contact')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('table')

    <div class="col-md-12" style="padding-top: 10px">
        <h5>Group Contacts</h5>
    </div>
    <div class="col-md-12" style="padding-top: 10px">

        <table id="contact" class="table data-table table-striped table-bordered table-responsive-md" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th style="width: 15%">{{language_data('SL',Auth::guard('client')->user()->lan_id)}}</th>
                <th style="width: 40%">{{language_data('List name',Auth::guard('client')->user()->lan_id)}}</th>
                <th style="width: 45%">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
            </tr>
            </thead>
            <tbody>

            @foreach($clientGroups as $cg)
                <tr>
                    <td>
                        <p>{{ $loop->iteration }}</p>
                    </td>
                    <td>
                        <p>{{$cg->group_name}} </p>
                    </td>
                    <td>

                        <a href="{{url('user/view-contact/'.$cg->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> {{language_data('View Contacts',Auth::guard('client')->user()->lan_id)}}</a>
                        <button class="btn btn-indigo btn-xs" data-toggle="modal" data-target="#addContact_{{$cg->id}}" ><i class="fa fa-plus"></i> {{language_data('Add Contact',Auth::guard('client')->user()->lan_id)}}</button>
                        @include('new.modals.addContact')


                        <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target=".modal_edit_list_{{$cg->id}}"><i class="fa fa-edit"></i> {{language_data('Edit',Auth::guard('client')->user()->lan_id)}}</a>
                        @include('client.modal-edit-contact-list')

                        <a href="#" class="btn btn-danger btn-xs cdelete" id="{{$cg->id}}"><i class="fa fa-trash"></i> {{language_data('Delete',Auth::guard('client')->user()->lan_id)}}</a>
                    </td>
                </tr>


            @endforeach

            </tbody>
        </table>
    </div>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){

            $('.data-table').DataTable({
                language: {
                    url: '{!! url("assets/libs/data-table/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language.".lang") !!}'
                },
                responsive: true
            })

            /*For Delete Group*/
            $( "body" ).delegate( ".cdelete", "click",function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/delete-import-phone-number/" + id;
                    }
                });
            });

            $( "body" ).delegate( ".creategroup", "click",function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/delete-import-phone-number/" + id;
                    }
                });
            });

        });
    </script>
@endsection