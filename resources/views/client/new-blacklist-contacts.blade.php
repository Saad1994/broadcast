@extends('new.main.app')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection


@section('content')

    <div class="container">
        @include('notification.notify')
        <h3 style="padding-top: 10px">{{language_data('Blacklist Contacts',Auth::guard('client')->user()->lan_id)}}</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-12">
                <p> This list allows you to input numbers that will not be called on any of your future blasts. They will be
                    automatically blocked even if they are listed in your blast. </p> <br>
                <p> *Before removing a number from your like, make sure you have consent from the owner.</p>
            </div>

            <div class="col-md-12">
                <table class="table data-table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 50%">{{language_data('Numbers',Auth::guard('client')->user()->lan_id)}}</th>
                        <th style="width: 30%">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>Accountant</td>

                    </tr>
                    <tr>
                        <td>Ashton Cox</td>
                        <td>Junior Technical Author</td>

                    </tr>
                    <tr>
                        <td>Cedric Kelly</td>
                        <td>Senior Javascript Developer</td>

                    </tr>
                    <tr>
                        <td>Airi Satou</td>
                        <td>Accountant</td>

                    </tr>
                    <tr>
                        <td>Brielle Williamson</td>
                        <td>Integration Specialist</td>

                    </tr>
                    <tr>
                        <td>Herrod Chandler</td>
                        <td>Sales Assistant</td>

                    </tr>
                    <tr>
                        <td>Rhona Davidson</td>
                        <td>Integration Specialist</td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>{{language_data('Add New Contact',Auth::guard('client')->user()->lan_id)}}</h5>
                <form class="form" role="form" method="post" action="{{url('user/sms/post-blacklist-contact')}}">

                    <div class="md-form">
                        <input type="text" id="phone_number" class="form-control" name="phone_number" required>
                        <label for="phone_number" >{{language_data('Phone Number',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> {{language_data('Add',Auth::guard('client')->user()->lan_id)}} </button>
                    </form>
            </div>
        </div>




@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>


    <script>
        $(document).ready(function(){

            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('user/sms/get-blacklist-contact/') !!}',
                columns: [
                    {data: 'numbers', name: 'numbers'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
              language: {
                url: '{!! url("assets/libs/data-table/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language.".lang") !!}'
              },
              responsive: true
            });

            /*For Delete Group*/
            $( "body" ).delegate( ".cdelete", "click",function (e) {
                e.preventDefault();
                var id = this.id;
              bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/user/sms/delete-blacklist-contact/" + id;
                    }
                });
            });

            $('.dataTables_wrapper').find('label').each(function () {
                $(this).parent().append($(this).children());
            });
            $('.dataTables_filter').find('input').each(function () {
                $('input').attr("placeholder", "Search");
                $('input').removeClass('form-control-sm');
            });
            $('.dataTables_length').addClass('d-flex flex-row');
            $('.dataTables_filter').addClass('md-form');
            $('select').addClass('mdb-select');
            $('.mdb-select').material_select();
            $('.mdb-select').removeClass('form-control form-control-sm');
            $('.dataTables_filter').find('label').remove();

        });
    </script>
@endsection