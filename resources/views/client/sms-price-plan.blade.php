@extends('new.panel.control.overview')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection

@section('data')
    <div class="container">
        <h3>Credits</h3>
        <div class="row" style="padding-top: 10px">
            @include('notification.notify')
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active"  href="{{url('user/sms/purchase-sms-plan')}}" >{{language_data('SMS Price Plan',Auth::guard('client')->user()->lan_id)}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  "  href="{{url('user/invoices/all')}}" >{{language_data('All Invoices',Auth::guard('client')->user()->lan_id)}}</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                        <table class="table data-table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10%;">{{language_data('SL',Auth::guard('client')->user()->lan_id)}}#</th>
                                <th style="width: 35%;">{{language_data('Plan Name',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 15%;">{{language_data('Price',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 10%;">{{language_data('Popular',Auth::guard('client')->user()->lan_id)}}</th>
                                <th style="width: 30%;">{{language_data('Action',Auth::guard('client')->user()->lan_id)}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($price_plan as $pp)
                                <tr>
                                    <td data-label="SL">{{ $loop->iteration }}</td>
                                    <td data-label="Plan Name"><p>{{$pp->plan_name}}</p></td>
                                    <td data-label="Price"><p>{{us_money_format($pp->price)}}</p></td>
                                    @if($pp->popular=='Yes')
                                        <td data-label="Popular"><p class="label label-success label-xs">{{language_data('Yes',Auth::guard('client')->user()->lan_id)}}</p></td>
                                    @else
                                        <td data-label="Popular"><p class="label label-primary label-xs">{{language_data('No',Auth::guard('client')->user()->lan_id)}}</p></td>
                                    @endif
                                    <td data-label="Actions">
                                        <a class="btn btn-success btn-xs" href="{{url('user/sms/sms-plan-feature/'.$pp->id)}}" ><i class="fa fa-eye"></i> {{language_data('View Features',Auth::guard('client')->user()->lan_id)}}</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){

          $('.data-table').DataTable({
            language: {
              url: '{!! url("assets/libs/data-table/i18n/".get_language_code(Auth::guard('client')->user()->lan_id)->language.".lang") !!}'
            },
            responsive: true
          })

            /*For Delete Price Plan*/
            $( "body" ).delegate( ".cdelete", "click",function (e) {
                e.preventDefault();
                var id = this.id;
              bootbox.confirm("{!! language_data('Are you sure',Auth::guard('client')->user()->lan_id) !!} ?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/sms/delete-price-plan/" + id;
                    }
                });
            });

        });
    </script>
@endsection