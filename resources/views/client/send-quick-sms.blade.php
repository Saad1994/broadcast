@extends('newclient')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="padding-top: 10px;">{{language_data('Send Quick SMS',Auth::guard('client')->user()->lan_id)}}</h3>
                        </div>
                        <div class="panel-body">

                            <form class="" role="form" method="post" action="{{url('user/sms/post-quick-sms')}}" enctype="multipart/form-data">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>{{language_data('SMS Gateway',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="mdb-select" name="sms_gateway" data-live-search="true">
                                        @foreach($sms_gateways as $gateway)
                                            <option value="{{$gateway->id}}">{{$gateway->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @if(app_config('sender_id_verification') == 1)
                                    @if($sender_ids)
                                        <div class="form-group">
                                            <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                            <select class="mdb-select sender_id" name="sender_id" data-live-search="true">
                                                @foreach($sender_ids as $si)
                                                    <option value="{{$si}}">{{$si}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                            <p><a href="{{url('user/sms/sender-id-management')}}" class="text-uppercase">{{language_data('Request New Sender ID',Auth::guard('client')->user()->lan_id)}}</a> </p>
                                        </div>
                                    @endif
                                @else
                                    <div class="form-group">
                                        <label>{{language_data('Sender ID',Auth::guard('client')->user()->lan_id)}}</label>
                                        <input type="text" class="form-control sender_id" name="sender_id" value="{{old('sender_id')}}">
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label>{{language_data('Country Code',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="mdb-select" name="country_code" data-live-search="true">
                                        <option value="0">{{language_data('Exist on phone number',Auth::guard('client')->user()->lan_id)}}</option>
                                        @foreach($country_code as $code)
                                            <option value="{{$code->country_code}}" @if(app_config('Country') == $code->country_name) selected @endif >{{$code->country_name}} ({{$code->country_code}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Recipients',Auth::guard('client')->user()->lan_id)}}</label>
                                    <textarea class="form-control" rows="4" name="recipients"  id="recipients">{{old('recipients')}}</textarea>
                                    <span class="help text-uppercase">{{language_data('Insert number with comma',Auth::guard('client')->user()->lan_id)}} (,) Ex. 8801721900000,8801721900001</span>
                                    <span class="help text-uppercase pull-right">{{language_data('Total Number Of Recipients',Auth::guard('client')->user()->lan_id)}}
                                        : <span class="number_of_recipients bold text-success m-r-5">0</span></span>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Remove Duplicate',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="mdb-select " name="remove_duplicate">
                                        <option value="yes">{{language_data('Yes',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="no">{{language_data('No',Auth::guard('client')->user()->lan_id)}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Message Type',Auth::guard('client')->user()->lan_id)}}</label>
                                    <select class="mdb-select message_type" name="message_type">
                                        <option value="plain">{{language_data('Plain',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="unicode">{{language_data('Unicode',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="voice">{{language_data('Voice',Auth::guard('client')->user()->lan_id)}}</option>
                                        <option value="mms">{{language_data('MMS',Auth::guard('client')->user()->lan_id)}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Message',Auth::guard('client')->user()->lan_id)}}</label>
                                    <textarea class="form-control" name="message" rows="5" id="message"> {{old('message')}} </textarea>
                                    <span class="help text-uppercase"
                                          id="remaining">160 {{language_data('characters remaining',Auth::guard('client')->user()->lan_id)}}</span>
                                    <span class="help text-success" id="messages">1 {{language_data('message',Auth::guard('client')->user()->lan_id)}}
                                        (s)</span>
                                </div>


                                <div class="form-group send-mms">
                                    <label>{{language_data('Select File',Auth::guard('client')->user()->lan_id)}}</label>
                                    <div class="form-group input-group input-group-file">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                {{language_data('Browse',Auth::guard('client')->user()->lan_id)}} <input type="file" class="form-control" name="image" accept="audio/*,video/*,image/*">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> {{language_data('Send',Auth::guard('client')->user()->lan_id)}} </button>
                            </form>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}

    <script>
      $(document).ready(function () {

        var number_of_recipients_ajax = 0,
          number_of_recipients_manual = 0,
          $get_msg = $("#message"),
          $remaining = $('#remaining'),
          $messages = $remaining.next(),
          message_type = 'plain',
          maxCharInitial = 160,
          maxChar = 157,
          messages = 1;


        function get_character() {
          var totalChar = $get_msg[0].value.length;
          var remainingChar = maxCharInitial;

          if ( totalChar <= maxCharInitial ) {
            remainingChar = maxCharInitial - totalChar;
            messages = 1;
          } else {
            totalChar = totalChar - maxCharInitial;
            messages = Math.ceil( totalChar / maxChar );
            remainingChar = messages * maxChar - totalChar;
            messages = messages + 1;
          }

            $remaining.text(remainingChar + " {!! language_data('characters remaining',Auth::guard('client')->user()->lan_id) !!}");
            $messages.text(messages + " {!! language_data('message',Auth::guard('client')->user()->lan_id) !!}"+ '(s)');
        }

        $('.send-mms').hide();
        $('.message_type').on('change', function () {
          message_type = $(this).val()

          if (message_type == 'unicode') {
            maxCharInitial = 70
            maxChar = 67
            messages = 1
            $('.send-mms').hide();
            get_character()
          }

          if (message_type == 'plain' || message_type == 'voice') {
            maxCharInitial = 160
            maxChar = 160
            messages = 1
            $('.send-mms').hide();
            get_character()
          }

          if (message_type == 'mms'){
            $('.send-mms').show();
          }

        })

        $get_msg.keyup(get_character);


        $('#recipients').on('keyup', function () {

          if ($(this).val().trim()) {
            number_of_recipients_manual = splitMulti($(this).val(),[',','\n',';']).length;
          } else {
            number_of_recipients_manual = 0;
          }
          var total = number_of_recipients_manual + Number(number_of_recipients_ajax);

          $('.number_of_recipients').text(total);

        });
      });
    </script>
@endsection