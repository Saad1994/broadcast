@extends('new.panel.control.overview')

@section('data')
    <div class="container">
        @include('notification.notify')
        <h3>Account Settings</h3>
        <div class="row" style="padding-top: 10px">
            <div class="col-md-8">
                <h5>{{language_data('View Profile',Auth::guard('client')->user()->lan_id)}}</h5>
                <form class="form" action="{{url('user/post-personal-info')}}" method="post">
                    {{ csrf_field() }}
                    <div class="md-form">
                        <input  type="text" id="fname" class="form-control" required="" name="first_name" value="{{$client->fname}}">
                        <label for="fname">{{language_data('First Name',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="lname" class="form-control" name="last_name"  value="{{$client->lname}}">
                        <label for="lname">{{language_data('Last Name',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="company" class="form-control" name="company" value="{{$client->company}}">
                        <label for="company">{{language_data('Company',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="url" id="website" class="form-control" name="website" value="{{$client->website}}">
                        <label for="website">{{language_data('Website',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>

                    <div class="md-form">
                        <select name="country" class="mdb-select" data-live-search="true">
                            <option value="" disabled selected>Choose your country</option>
                            {!!countries($client->country)!!}
                        </select>
                    </div>


                    <h3>Your Contact Info</h3>
                    <div class="md-form">
                        <input  type="text" id="phone" class="form-control" required name="phone" value="{{$client->phone}}">
                        <label for="phone">{{language_data('Phone',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="email" id="email" class="form-control" required name="email" value="{{$client->email}}">
                        <label for="email">{{language_data('Email',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="address" class="form-control"  name="address" value="{{$client->address1}}">
                        <label for="address">{{language_data('Address',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="address" class="form-control"  name="address" value="{{$client->address2}}">
                        <label for="address">{{language_data('More Address',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="state" class="form-control" name="state"  value="{{$client->state}}">
                        <label for="state">{{language_data('State',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="text" id="zip" class="form-control" name="postcode"  value="{{$client->postcode}}">
                        <label for="zip">{{language_data('Postcode',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input type="hidden" value="{{$client->id}}" name="cmd">
                        <input type="submit" value="{{language_data('Update',Auth::guard('client')->user()->lan_id)}}" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <h5>{{language_data('Change Password',Auth::guard('client')->user()->lan_id)}}</h5>
                <form class="form" role="form" action="{{url('user/update-password')}}" method="post">
                    <div class="md-form">
                        <input  type="password" id="current_pass" class="form-control" required name="current_password">
                        <label for="current_pass">{{language_data('Current Password',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input  type="password" id="new_pass" class="form-control" required name="new_password">
                        <label for="new_pass">{{language_data('New Password',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="repeat_new_pass" class="form-control" required name="confirm_password">
                        <label for="repeat_new_pass">{{language_data('Confirm Password',Auth::guard('client')->user()->lan_id)}}</label>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> {{language_data('Update',Auth::guard('client')->user()->lan_id)}} </button>

                </form>
            </div>
        </div>
    </div>
@endsection

