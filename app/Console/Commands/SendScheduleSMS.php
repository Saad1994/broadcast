<?php

namespace App\Console\Commands;

use App\CustomSMSGateways;
use App\Jobs\SendBulkMMS;
use App\Jobs\SendBulkSMS;
use App\Jobs\SendBulkVoice;
use App\ScheduleSMS;
use App\SMSGatewayCredential;
use App\SMSGateways;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendScheduleSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send schedule sms to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_time = Carbon::now()->format('Y-m-d H:i').':00';
        $end_time = Carbon::now()->format('Y-m-d H:i').':59';


        $results = ScheduleSMS::whereBetween('submit_time',[$start_time,$end_time])->get();

        foreach ($results as $s){
            $gateway = SMSGateways::find($s->use_gateway);

            $gateway_credential = null;
            $cg_info = null;

            if ($gateway->custom == 'Yes') {
                if ($gateway->type == 'smpp') {
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                } else {
                    $cg_info = CustomSMSGateways::where('gateway_id', $s->use_gateway)->first();
                }
            } else {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
            }

            $msg_type = $s->type;

            if ($msg_type == 'plain' || $msg_type == 'unicode') {
                dispatch(new SendBulkSMS($s->userid,$s->receiver, $gateway,$gateway_credential ,$s->sender, $s->message, $s->amount,$cg_info,'',$msg_type));
            }

            if ($msg_type == 'voice') {
                dispatch(new SendBulkVoice($s->userid, $s->receiver, $gateway, $gateway_credential, $s->sender, $s->message, $s->amount, '', $msg_type));
            }

            if ($msg_type == 'mms') {
                dispatch(new SendBulkMMS($s->userid, $s->receiver, $gateway, $gateway_credential, $s->sender, $s->message, $s->media_url, '', $msg_type));
            }

            $s->delete();
        }

    }
}
