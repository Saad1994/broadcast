<?php

namespace App\Console\Commands;

use App\CustomSMSGateways;
use App\Jobs\SendBulkMMS;
use App\Jobs\SendBulkSMS;
use App\Jobs\SendBulkVoice;
use App\SMSGatewayCredential;
use App\SMSGateways;
use App\StoreBulkSMS;
use Illuminate\Console\Command;

class BulkSMSFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:sendbulk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Bulk SMS From File';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bulk_sms = StoreBulkSMS::where('status', '0')->get();
        foreach ($bulk_sms as $sms) {
            $results = $sms->msg_data;
            $msg_type = $sms->type;
            $results = json_decode($results);
            $gateway = SMSGateways::find($sms->use_gateway);


            $gateway_credential = null;
            $cg_info = null;
            if ($gateway->custom == 'Yes') {
                if ($gateway->type == 'smpp') {
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                } else {
                    $cg_info = CustomSMSGateways::where('gateway_id', $sms->use_gateway)->first();
                }
            } else {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
            }


            $sms->status = '1';
            $sms->save();
            foreach ($results as $r) {
                if ($msg_type == 'plain' || $msg_type == 'unicode') {
                    dispatch(new SendBulkSMS($sms->userid, $r->phone_number, $gateway, $gateway_credential, $sms->sender, $r->message, $r->segments, $cg_info, '', $msg_type));
                }

                if ($msg_type == 'voice') {
                    dispatch(new SendBulkVoice($sms->userid, $r->phone_number, $gateway, $gateway_credential, $sms->sender, $r->message, $r->segments, '', $msg_type));
                }

                if ($msg_type == 'mms') {
                    dispatch(new SendBulkMMS($sms->userid, $r->phone_number, $gateway, $gateway_credential, $sms->sender, $r->message, $r->media_url, '', $msg_type));
                }
            }
            $sms->delete();
        }
    }
}
