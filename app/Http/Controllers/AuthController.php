<?php

namespace App\Http\Controllers;

use App\Admin;
use App\AdminRolePermission;
use App\AppConfig;
use App\Client;
use App\EmailTemplates;
use App\Language;
use App\LanguageData;
use App\PaymentGateways;
use App\SMSGatewayCredential;
use App\SMSGateways;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use ReCaptcha\ReCaptcha;

class AuthController extends Controller
{
    //======================================================================
    // clientLogin Function Start Here
    //======================================================================
    public function clientLogin()
    {
        if (!env('DB_DATABASE')) {
            return redirect('install');
        }

        if (Auth::guard('client')->check()) {
            return redirect('dashboard');
        }

        return view('client.login');
    }

    //======================================================================
    // clientGetLogin Function Start Here
    //======================================================================
    public function clientGetLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required', 'password' => 'required'
        ]);

        $check_input = $request->only('username', 'password');
        $remember = (Input::has('remember')) ? true : false;


        if (app_config('captcha_in_client') == '1') {
            if (isset($_POST['g-recaptcha-response'])) {
                $getCaptchaResponse = $_POST['g-recaptcha-response'];
                $recaptcha = new ReCaptcha(app_config('captcha_secret_key'));
                $resp = $recaptcha->verify($getCaptchaResponse);

                if (!$resp->isSuccess()) {
                    if (array_key_exists('0', $resp->getErrorCodes())) {
                        $error_msg = $resp->getErrorCodes()[0];
                    } else {
                        $error_msg = language_data('Invalid Captcha');
                    }

                    return redirect('/')->with([
                        'message' => $error_msg,
                        'message_important' => true
                    ]);
                }
            } else {
                return redirect('/')->with([
                    'message' => language_data('Invalid Captcha'),
                    'message_important' => true
                ]);
            }
        }

        if (Auth::guard('client')->attempt($check_input, $remember)) {

            if (Auth::guard('client')->user()->status == 'Active') {
                return redirect()->intended('dashboard');
            } else {
                Auth::guard('client')->logout();
                return redirect('/')->withInput($request->only('username'))->withErrors([
                    'username' => language_data('Your are inactive or blocked by system. Please contact with administrator')
                ]);
            }

        } else {
            return redirect('/')->withInput($request->only('username'))->withErrors([
                'username' => language_data('Invalid User name or Password')
            ]);
        }
    }



    //======================================================================
    // clientRegistrationVerification Function Start Here
    //======================================================================
    public function clientRegistrationVerification()
    {
        return view('client.user-verification');
    }


    //======================================================================
    // postVerificationToken Function Start Here
    //======================================================================
    public function postVerificationToken(Request $request)
    {
        $cmd = Input::get('cmd');

        if ($cmd == '') {
            return redirect('/')->with([
                'message' => language_data('Invalid Request'),
                'message_important' => true
            ]);
        }


        $ef = Client::find($cmd);

        if ($ef) {

            $fprand = substr(str_shuffle(str_repeat('0123456789', '16')), 0, '16');

            $name = $ef->fname . ' ' . $ef->lname;
            $email = $ef->email;
            /*For Email Confirmation*/

            $conf = EmailTemplates::where('tplname', '=', 'Client Registration Verification')->first();

            $estatus = $conf->status;
            if ($estatus == '1') {


                $ef->pwresetkey = $fprand;
                $ef->save();

                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $fpw_link = url('/verify-user/' . $fprand);

                $template = $conf->message;
                $subject = $conf->subject;

                $data = array('name' => $name,
                    'business_name' => $sysCompany,
                    'template' => $template,
                    'sys_url' => $fpw_link
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;
                        if (!$mail->send()) {
                            return redirect('user/registration-verification')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('user/registration-verification')->with([
                                'message' => language_data('Verification code send successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('user/registration-verification')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }


                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('user/registration-verification')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('user/registration-verification')->with([
                                'message' => language_data('Verification code send successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('user/registration-verification')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }

                }

            } else {
                return redirect('user/registration-verification')->with([
                    'message' => language_data('Something wrong, Please contact with your provider')
                ]);
            }

        } else {
            return redirect('/')->with([
                'message' => language_data('Invalid Request'),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // verifyUserAccount Function Start Here
    //======================================================================
    public function verifyUserAccount($token)
    {


        $tfnd = Client::where('pwresetkey', '=', $token)->count();

        if ($tfnd == '1') {
            $d = Client::where('pwresetkey', '=', $token)->first();
            $d->status = 'Active';
            $d->pwresetkey = '';
            $d->save();

            return redirect()->intended('dashboard');

        } else {
            return redirect('/')->with([
                'message' => language_data('Verification code not found'),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // forgotUserPassword Function Start Here
    //======================================================================
    public function forgotUserPassword()
    {
        return view('client.forgot-password');
    }



    //======================================================================
    // clientSignUp Function Start Here
    //======================================================================
    public function clientSignUp()
    {
        if (app_config('client_registration') != '1') {
            return redirect('/')->with([
                'message' => language_data('Invalid Request'),
                'message_important' => true
            ]);
        }

        return view('client.registration');

    }

    //======================================================================
    // postUserRegistration Function Start Here
    //======================================================================
    public function postUserRegistration(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'first_name' => 'required', 'user_name' => 'required', 'email' => 'required', 'password' => 'required', 'cpassword' => 'required', 'phone' => 'required', 'country' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('signup')->withInput($request->all())->withErrors($v->errors());
        }


        if (app_config('captcha_in_client_registration') == '1') {
            if (isset($_POST['g-recaptcha-response'])) {
                $getCaptchaResponse = $_POST['g-recaptcha-response'];
                $recaptcha = new ReCaptcha(app_config('captcha_secret_key'));
                $resp = $recaptcha->verify($getCaptchaResponse);

                if (!$resp->isSuccess()) {
                    if (array_key_exists('0', $resp->getErrorCodes())) {
                        $error_msg = $resp->getErrorCodes()[0];
                    } else {
                        $error_msg = language_data('Invalid Captcha');
                    }

                    return redirect('signup')->withInput($request->all())->with([
                        'message' => $error_msg,
                        'message_important' => true
                    ]);
                }
            } else {
                return redirect('signup')->withInput($request->all())->with([
                    'message' => language_data('Invalid Captcha'),
                    'message_important' => true
                ]);
            }
        }

        $exist_user_name = Client::where('username', $request->user_name)->first();
        $exist_user_email = Client::where('email', $request->email)->first();

        if ($exist_user_name) {
            return redirect('signup')->withInput($request->all())->with([
                'message' => language_data('User name already exist'),
                'message_important' => true
            ]);
        }

        if ($exist_user_email) {
            return redirect('signup')->withInput($request->all())->with([
                'message' => language_data('Email already exist'),
                'message_important' => true
            ]);
        }

        $password = $request->password;
        $cpassword = $request->cpassword;

        if ($password !== $cpassword) {
            return redirect('signup')->withInput($request->all())->with([
                'message' => language_data('Both password does not match'),
                'message_important' => true
            ]);
        } else {
            $password = bcrypt($password);
        }

        if (app_config('registration_verification') == '1') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        }

        $email_notify = $request->email_notify;
        if ($email_notify == 'yes') {
            $email_notify = 'Yes';
        } else {
            $email_notify = 'No';
        }

        $email = $request->email;
        $sms_gateway = array(app_config('sms_api_gateway'));
        $sms_gateways_id = json_encode($sms_gateway , true);

        $api_key_generate = $request->user_name . ':' . $cpassword;
        $client = new Client();
        $client->parent = '0';
        $client->fname = $request->first_name;
        $client->lname = $request->last_name;
        $client->email = $email;
        $client->username = $request->user_name;
        $client->password = $password;
        $client->country = $request->country;
        $client->phone = $request->phone;
        $client->image = 'profile.jpg';
        $client->datecreated = date('Y-m-d');
        $client->sms_limit = '0';
        $client->api_access = 'No';
        $client->api_key = base64_encode($api_key_generate);
        $client->status = $status;
        $client->reseller = 'No';
        $client->sms_gateway = $sms_gateways_id;
        $client->emailnotify = $email_notify;
        $client->save();
        $client_id = $client->id;

        /*For Email Confirmation*/
        if (is_int($client_id) && $email_notify == 'Yes' && $email != '') {

            $conf = EmailTemplates::where('tplname', '=', 'Client SignUp')->first();

            $estatus = $conf->status;

            if ($estatus == '1') {

                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $sysUrl = url('/');

                $template = $conf->message;
                $subject = $conf->subject;
                $client_name = $request->first_name . ' ' . $request->last_name;
                $data = array(
                    'name' => $client_name,
                    'business_name' => $sysCompany,
                    'from' => $sysEmail,
                    'username' => $request->user_name,
                    'email' => $email,
                    'password' => $cpassword,
                    'sys_url' => $sysUrl,
                    'template' => $template
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $client_name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('/')->with([
                                'message' => language_data('Registration Successful')
                            ]);
                        } else {
                            return redirect('/')->with([
                                'message' => language_data('Registration Successful')
                            ]);
                        }

                    } catch (\phpmailerException $e) {
                        return redirect('signup')->withInput($request->all())->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }

                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $client_name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;


                        if (!$mail->send()) {
                            return redirect('/')->with([
                                'message' => language_data('Registration Successful')
                            ]);
                        } else {
                            return redirect('/')->with([
                                'message' => language_data('Registration Successful')
                            ]);
                        }

                    } catch (\phpmailerException $e) {
                        return redirect('signup')->withInput($request->all())->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }
                }
            }
        }

        return redirect('/')->with([
            'message' => language_data('Registration Successful')
        ]);

    }



    //======================================================================
    // adminLogin Function Start Here
    //======================================================================
    public function adminLogin()
    {

        if (Auth::check()) {
            return redirect('admin/dashboard');
        }

        return view('admin.login');
    }



    //======================================================================
    // adminGetLogin Function Start Here
    //======================================================================
    public function adminGetLogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required', 'password' => 'required'
        ]);

        $check_input = $request->only('username', 'password');
        $remember = (Input::has('remember')) ? true : false;

        if (app_config('captcha_in_admin') == '1') {
            if (isset($_POST['g-recaptcha-response'])) {
                $getCaptchaResponse = $_POST['g-recaptcha-response'];
                $recaptcha = new ReCaptcha(app_config('captcha_secret_key'));
                $resp = $recaptcha->verify($getCaptchaResponse);

                if (!$resp->isSuccess()) {
                    if (array_key_exists('0', $resp->getErrorCodes())) {
                        $error_msg = $resp->getErrorCodes()[0];
                    } else {
                        $error_msg = language_data('Invalid Captcha');
                    }

                    return redirect('admin')->with([
                        'message' => $error_msg,
                        'message_important' => true
                    ]);
                }
            } else {
                return redirect('admin')->with([
                    'message' => language_data('Invalid Captcha'),
                    'message_important' => true
                ]);
            }
        }

        if (Auth::attempt($check_input, $remember)) {
            if (Auth::user()->status == 'Active') {
                return redirect()->intended('admin/dashboard');
            } else {
                return redirect('admin')->withInput($request->only('username'))->withErrors([
                    'username' => language_data('Your are inactive or blocked by system. Please contact with administrator')
                ]);
            }
        } else {
            return redirect('admin')->withInput($request->only('username'))->withErrors([
                'username' => language_data('Invalid User name or Password')
            ]);
        }
    }

    //======================================================================
    // permissionError Function Start Here
    //======================================================================
    public function permissionError()
    {
        return view('admin.permission-error');
    }

    //======================================================================
    // forgotPassword Function Start Here
    //======================================================================
    public function forgotPassword()
    {
        return view('admin.forgot-password');
    }


    //======================================================================
    // forgotPasswordToken Function Start Here
    //======================================================================
    public function forgotPasswordToken(Request $request)
    {

        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('admin')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        $v = \Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('forgot-password')->withErrors($v->errors());
        }

        $email = Input::get('email');

        $d = Admin::where('email', '=', $email)->count();
        if ($d == '1') {
            $fprand = substr(str_shuffle(str_repeat('0123456789', '16')), 0, '16');
            $ef = Admin::where('email', '=', $email)->first();
            $name = $ef->fname . ' ' . $ef->lname;
            $username = $ef->username;
            $ef->pwresetkey = $fprand;
            $ef->save();

            /*For Email Confirmation*/

            $conf = EmailTemplates::where('tplname', '=', 'Forgot Admin Password')->first();

            $estatus = $conf->status;
            if ($estatus == '1') {
                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $fpw_link = url('admin/forgot-password-token-code/' . $fprand);

                $template = $conf->message;
                $subject = $conf->subject;

                $data = array('name' => $name,
                    'business_name' => $sysCompany,
                    'username' => $username,
                    'from' => $sysEmail,
                    'template' => $template,
                    'forgotpw_link' => $fpw_link
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;
                        if (!$mail->send()) {
                            return redirect('admin/forgot-password')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('admin/forgot-password')->with([
                                'message' => language_data('Password Reset Successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('admin/forgot-password')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }
                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('admin/forgot-password')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('admin/forgot-password')->with([
                                'message' => language_data('Password Reset Successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('admin/forgot-password')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }
                }
            }

            return redirect('admin/forgot-password')->with([
                'message' => language_data('Your Password Already Reset. Please Check your email')
            ]);
        } else {
            return redirect('admin/forgot-password')->with([
                'message' => language_data('Sorry There is no registered user with this email address'),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // forgotPasswordTokenCode Function Start Here
    //======================================================================
    public function forgotPasswordTokenCode($token)
    {


        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('admin')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        $tfnd = Admin::where('pwresetkey', '=', $token)->count();

        if ($tfnd == '1') {
            $d = Admin::where('pwresetkey', '=', $token)->first();
            $name = $d->fname . ' ' . $d->lname;
            $email = $d->email;
            $username = $d->username;

            $rawpass = substr(str_shuffle(str_repeat('0123456789', '16')), 0, '16');
            $password = bcrypt($rawpass);

            $d->password = $password;
            $d->pwresetkey = '';
            $d->save();

            /*For Email Confirmation*/

            $conf = EmailTemplates::where('tplname', '=', 'Admin Password Reset')->first();

            $estatus = $conf->status;
            if ($estatus == '1') {
                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $fpw_link = url('admin');

                $template = $conf->message;
                $subject = $conf->subject;

                $data = array('name' => $name,
                    'business_name' => $sysCompany,
                    'username' => $username,
                    'password' => $rawpass,
                    'template' => $template,
                    'sys_url' => $fpw_link
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;
                        if (!$mail->send()) {
                            return redirect('admin')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('admin')->with([
                                'message' => language_data('A New Password Generated. Please Check your email.')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('admin')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }


                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('admin')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('admin')->with([
                                'message' => language_data('A New Password Generated. Please Check your email.')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('admin')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }

                }

            }
            return redirect('admin')->with([
                'message' => language_data('A New Password Generated. Please Check your email.')
            ]);
        } else {
            return redirect('admin')->with([
                'message' => language_data('Sorry Password reset Token expired or not exist, Please try again.'),
                'message_important' => true
            ]);
        }


    }



    //======================================================================
    // forgotUserPasswordToken Function Start Here
    //======================================================================
    public function forgotUserPasswordToken(Request $request)
    {

        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('/')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        $v = \Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('forgot-password')->withErrors($v->errors());
        }

        $email = Input::get('email');

        $d = Client::where('email', '=', $email)->count();
        if ($d == '1') {
            $fprand = substr(str_shuffle(str_repeat('0123456789', '16')), 0, '16');
            $ef = Client::where('email', '=', $email)->first();
            $name = $ef->fname . ' ' . $ef->lname;
            $username = $ef->username;
            $ef->pwresetkey = $fprand;
            $ef->save();

            /*For Email Confirmation*/

            $conf = EmailTemplates::where('tplname', '=', 'Forgot Client Password')->first();

            $estatus = $conf->status;
            if ($estatus == '1') {
                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $fpw_link = url('user/forgot-password-token-code/' . $fprand);

                $template = $conf->message;
                $subject = $conf->subject;

                $data = array('name' => $name,
                    'business_name' => $sysCompany,
                    'username' => $username,
                    'from' => $sysEmail,
                    'template' => $template,
                    'forgotpw_link' => $fpw_link
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;
                        if (!$mail->send()) {
                            return redirect('forgot-password')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('forgot-password')->with([
                                'message' => language_data('Password Reset Successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('forgot-password')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }
                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('forgot-password')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('forgot-password')->with([
                                'message' => language_data('Password Reset Successfully. Please check your email')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('forgot-password')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }
                }
            }

            return redirect('forgot-password')->with([
                'message' => language_data('Your Password Already Reset. Please Check your email')
            ]);
        } else {
            return redirect('forgot-password')->with([
                'message' => language_data('Sorry There is no registered user with this email address'),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // forgotUserPasswordTokenCode Function Start Here
    //======================================================================
    public function forgotUserPasswordTokenCode($token)
    {


        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('/')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        $tfnd = Client::where('pwresetkey', '=', $token)->count();

        if ($tfnd == '1') {
            $d = Client::where('pwresetkey', '=', $token)->first();
            $name = $d->fname . ' ' . $d->lname;
            $email = $d->email;
            $username = $d->username;

            $rawpass = substr(str_shuffle(str_repeat('0123456789', '16')), 0, '16');
            $password = bcrypt($rawpass);

            $d->password = $password;
            $d->pwresetkey = '';
            $d->save();

            /*For Email Confirmation*/

            $conf = EmailTemplates::where('tplname', '=', 'Client Password Reset')->first();

            $estatus = $conf->status;
            if ($estatus == '1') {
                $sysEmail = app_config('Email');
                $sysCompany = app_config('AppName');
                $fpw_link = url('/');

                $template = $conf->message;
                $subject = $conf->subject;

                $data = array('name' => $name,
                    'business_name' => $sysCompany,
                    'username' => $username,
                    'password' => $rawpass,
                    'template' => $template,
                    'sys_url' => $fpw_link
                );

                $message = _render($template, $data);
                $mail_subject = _render($subject, $data);
                $body = $message;

                /*Set Authentication*/

                $default_gt = app_config('Gateway');

                if ($default_gt == 'default') {

                    $mail = new \PHPMailer();

                    try {
                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;
                        if (!$mail->send()) {
                            return redirect('/')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('/')->with([
                                'message' => language_data('A New Password Generated. Please Check your email.')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('/')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }


                } else {
                    $host = app_config('SMTPHostName');
                    $smtp_username = app_config('SMTPUserName');
                    $stmp_password = app_config('SMTPPassword');
                    $port = app_config('SMTPPort');
                    $secure = app_config('SMTPSecure');


                    $mail = new \PHPMailer();

                    try {

                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = $host;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = $smtp_username;                 // SMTP username
                        $mail->Password = $stmp_password;                           // SMTP password
                        $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = $port;

                        $mail->setFrom($sysEmail, $sysCompany);
                        $mail->addAddress($email, $name);     // Add a recipient
                        $mail->isHTML(true);                                  // Set email format to HTML

                        $mail->Subject = $mail_subject;
                        $mail->Body = $body;

                        if (!$mail->send()) {
                            return redirect('/')->with([
                                'message' => language_data('Please Check your Email Settings'),
                                'message_important' => true
                            ]);
                        } else {
                            return redirect('/')->with([
                                'message' => language_data('A New Password Generated. Please Check your email.')
                            ]);
                        }
                    } catch (\phpmailerException $e) {
                        return redirect('/')->with([
                            'message' => $e->getMessage(),
                            'message_important' => true
                        ]);
                    }

                }

            }
            return redirect('/')->with([
                'message' => language_data('A New Password Generated. Please Check your email.')
            ]);
        } else {
            return redirect('/')->with([
                'message' => language_data('Sorry Password reset Token expired or not exist, Please try again.'),
                'message_important' => true
            ]);
        }
    }


    /* updateApplication  Function Start Here */
    public function updateApplication(Request $request)
    {
        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('/')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }


        $v = \Validator::make($request->all(), [
            'purchase_code' => 'required', 'app_url' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('update')->withErrors($v->errors());
        }


        $purchase_code = $request->purchase_code;
        $domain_name = $request->app_url;

        $input = trim($domain_name, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }

        $urlParts = parse_url($input);
        $domain_name = preg_replace('/^www\./', '', $urlParts['host']);


        $get_verification = 'https://support.codeglen.com/forum/api/get-product-data/' . $purchase_code . '/' . $domain_name;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $get_verification);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($data, true);

        if (is_array($data) && array_key_exists('status', $data)) {
            if ($data['status'] != 'success') {
                return redirect('update')->with([
                    'message' => $data['msg'],
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('update')->with([
                'message' => 'Something went wrong. please try again.',
                'message_important' => true
            ]);
        }

        $version = trim(app_config('SoftwareVersion'));

        switch ($version) {
            case '2.3':
                $message = 'You are already using latest version';
                break;

            case '2.2':
                $sql = <<<EOF
ALTER TABLE `sys_clients` ADD `lan_id` INT(11) NOT NULL DEFAULT '1' AFTER `menu_open`;
ALTER TABLE `sys_clients` CHANGE `sms_gateway` `sms_gateway` TEXT NOT NULL;
ALTER TABLE `sys_clients` ADD `api_gateway` INT(11) NULL DEFAULT NULL AFTER `api_key`;
ALTER TABLE `sys_payment_gateways` ADD `custom_one` TEXT NULL DEFAULT NULL AFTER `password`;
ALTER TABLE `sys_payment_gateways` ADD `custom_two` TEXT NULL DEFAULT NULL AFTER `custom_one`;
ALTER TABLE `sys_payment_gateways` ADD `custom_three` TEXT NULL AFTER `custom_two`;
ALTER TABLE `sys_invoices` CHANGE `subtotal` `subtotal` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoices` CHANGE `total` `total` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoice_items` CHANGE `price` `price` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoice_items` CHANGE `subtotal` `subtotal` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoice_items` CHANGE `tax` `tax` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoice_items` CHANGE `discount` `discount` VARCHAR(100) NOT NULL DEFAULT '0.00';
ALTER TABLE `sys_invoice_items` CHANGE `total` `total` VARCHAR(100) NOT NULL DEFAULT '0.00';

CREATE TABLE `sys_block_message` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `use_gateway` int(11) NOT NULL,
  `scheduled_time` text COLLATE utf8_unicode_ci,
  `type` enum('plain','unicode','voice','mms') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'plain',
  `status` enum('block','release') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'block',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `sys_block_message` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_block_message` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `sys_bulk_sms` CHANGE `type` `type` ENUM('plain','unicode','voice','mms') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'plain';
ALTER TABLE `sys_language` ADD `language_code` VARCHAR(5) NOT NULL DEFAULT 'en' AFTER `status`;


CREATE TABLE `sys_operator` (
  `id` int(10) UNSIGNED NOT NULL,
  `coverage_id` int(11) NOT NULL,
  `operator_name` text COLLATE utf8_unicode_ci NOT NULL,
  `operator_code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `operator_setting` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `sys_operator` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_operator` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

CREATE TABLE `sys_recurring_sms` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` int(11) NOT NULL,
  `sender` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_recipients` int(11) NOT NULL,
  `use_gateway` int(11) NOT NULL,
  `media_url` longtext COLLATE utf8_unicode_ci,
  `recurring` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `recurring_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('plain','unicode','voice','mms') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'plain',
  `status` enum('running','stop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'running',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `sys_recurring_sms_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `receiver` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `sys_recurring_sms` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_recurring_sms_contacts` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_recurring_sms` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `sys_recurring_sms_contacts` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `sys_schedule_sms` CHANGE `type` `type` ENUM('plain','unicode','voice','mms') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'plain';


CREATE TABLE `sys_sms_gateway_credential` (
  `id` int(10) UNSIGNED NOT NULL,
  `gateway_id` int(11) NOT NULL,
  `username` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci,
  `extra` longtext COLLATE utf8_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Inactive',
  `c1` longtext COLLATE utf8_unicode_ci,
  `c2` longtext COLLATE utf8_unicode_ci,
  `c3` longtext COLLATE utf8_unicode_ci,
  `c4` longtext COLLATE utf8_unicode_ci,
  `c5` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `sys_sms_gateway_credential` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_sms_gateway_credential` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `sys_sms_history` ADD `sms_type` ENUM('plain','unicode','voice','mms') NOT NULL DEFAULT 'plain' AFTER `status`;
ALTER TABLE `sys_sms_history` ADD `media_url` LONGTEXT NULL DEFAULT NULL AFTER `send_by`;
ALTER TABLE `sys_sms_history` CHANGE `message` `message` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `sys_sms_history` CHANGE `amount` `amount` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `sys_schedule_sms` ADD `media_url` LONGTEXT NULL DEFAULT NULL AFTER `submit_time`;


CREATE TABLE `sys_spam_word` (
  `id` int(10) UNSIGNED NOT NULL,
  `word` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `sys_spam_word` ADD PRIMARY KEY (`id`);
ALTER TABLE `sys_spam_word` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `sys_sms_gateways` ADD `settings` VARCHAR(50) NOT NULL AFTER `name`;
ALTER TABLE `sys_sms_gateways` ADD `port` VARCHAR(20) NULL DEFAULT NULL AFTER `api_link`;
ALTER TABLE `sys_sms_gateways` ADD `mms` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `two_way`;
ALTER TABLE `sys_sms_gateways` ADD `voice` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `mms`;
EOF;

                \DB::connection()->getPdo()->exec($sql);

                $app_config = [
                    [
                        'setting' => 'fraud_detection',
                        'value' => '0'
                    ], [
                        'setting' => 'dec_point',
                        'value' => '.'
                    ], [
                        'setting' => 'thousands_sep',
                        'value' => ','
                    ], [
                        'setting' => 'currency_decimal_digits',
                        'value' => true
                    ], [
                        'setting' => 'currency_symbol_position',
                        'value' => 'left'
                    ]
                ];

                foreach ($app_config as $config) {
                    AppConfig::create($config);
                }

                $sms_gateways = SMSGateways::all();

                foreach ($sms_gateways as $gateway) {
                    $status = SMSGatewayCredential::create([
                        'gateway_id' => $gateway->id,
                        'username' => $gateway->username,
                        'password' => $gateway->password,
                        'extra' => $gateway->api_id,
                        'status' => 'Active'
                    ]);

                    if ($status) {
                        $gateway->settings = $gateway->name;
                        $gateway->save();
                    }
                }

                EmailTemplates::create([
                    'tplname' => 'Spam Word Notification',
                    'subject' => 'Get spam word from {{business_name}}]',
                    'message' => '<div style="margin:0;padding:0">
<table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#439cc8">
  <tbody><tr>
    <td align="center">
            <table cellspacing="0" cellpadding="0" width="672" border="0">
              <tbody><tr>
                <td height="95" bgcolor="#439cc8" style="background:#439cc8;text-align:left">
                <table cellspacing="0" cellpadding="0" width="672" border="0">
                      <tbody><tr>
                        <td width="672" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                      </tr>
                      <tr>
                        <td style="text-align:left">
                        <table cellspacing="0" cellpadding="0" width="672" border="0">
                          <tbody><tr>
                            <td width="37" height="24" style="font-size:40px;line-height:40px;height:40px;text-align:left">
                            </td>
                            <td width="523" height="24" style="text-align:left">
                            <div width="125" height="23" style="display:block;color:#ffffff;font-size:20px;font-family:Arial,Helvetica,sans-serif;max-width:557px;min-height:auto">{{business_name}}</div>
                            </td>
                            <td width="44" style="text-align:left"></td>
                            <td width="30" style="text-align:left"></td>
                            <td width="38" height="24" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                          </tr>
                        </tbody></table>
                        </td>
                      </tr>
                      <tr><td width="672" height="33" style="font-size:33px;line-height:33px;height:33px;text-align:left"></td></tr>
                    </tbody></table>

                </td>
              </tr>
            </tbody></table>
     </td>
    </tr>
 </tbody></table>

 <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#439cc8"><tbody><tr><td height="5" style="background:#439cc8;height:5px;font-size:5px;line-height:5px"></td></tr></tbody></table>

 <table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#e9eff0">
  <tbody><tr>
    <td align="center">
      <table cellspacing="0" cellpadding="0" width="671" border="0" bgcolor="#e9eff0" style="background:#e9eff0">
        <tbody><tr>
          <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="596" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="37" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
        </tr>
        <tr>
          <td width="38" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
          <td style="text-align:left"><table cellspacing="0" cellpadding="0" width="596" border="0" bgcolor="#ffffff">
            <tbody><tr>
              <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
              <td width="556" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
              <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
            </tr>
            <tr>
              <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
              <td width="556" style="text-align:left"><table cellspacing="0" cellpadding="0" width="556" border="0" style="font-family:helvetica,arial,sans-seif;color:#666666;font-size:16px;line-height:22px">
                <tbody><tr>
                  <td style="text-align:left"></td>
                </tr>
                <tr>
                  <td style="text-align:left"><table cellspacing="0" cellpadding="0" width="556" border="0">
                    <tbody><tr><td style="font-family:helvetica,arial,sans-serif;font-size:30px;line-height:40px;font-weight:normal;color:#253c44;text-align:left"></td></tr>
                    <tr><td width="556" height="20" style="font-size:20px;line-height:20px;height:20px;text-align:left"></td></tr>
                    <tr>
                      <td style="text-align:left">
                 Hi,<br>
                 <br>
                 Spam word detected. Here is the message and client details:
            <br>
                User name: <a href="{{profile_link}}" target="_blank">{{user_name}}</a><br>
                Message: {{message}}<br><br>
                Waiting for your quick response.
            <br><br>
            Thank you.
            <br>
            Regards,<br>
            {{business_name}}
            <br>
          </td>
                    </tr>
                    <tr>
                      <td width="556" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left">&nbsp;</td>
                    </tr>
                  </tbody></table></td>
                </tr>
              </tbody></table></td>
              <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
            </tr>
            <tr>
              <td width="20" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
              <td width="556" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
              <td width="20" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
            </tr>
          </tbody></table></td>
          <td width="37" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
        </tr>
        <tr>
          <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="596" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="37" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
        </tr>
      </tbody></table>
  </td></tr>
</tbody>
</table>
<table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#273f47"><tbody><tr><td align="center">&nbsp;</td></tr></tbody></table>
<table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#364a51">
  <tbody><tr>
    <td align="center">
       <table cellspacing="0" cellpadding="0" width="672" border="0" bgcolor="#364a51">
              <tbody><tr>
              <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="569" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
          <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
              </tr>
              <tr>
                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left">
                </td>
                <td valign="top" style="font-family:helvetica,arial,sans-seif;font-size:12px;line-height:16px;color:#949fa3;text-align:left">Copyright &copy; {{business_name}}, All rights reserved.<br><br><br></td>
                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
              </tr>
              <tr>
              <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
              <td width="569" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
              </tr>
            </tbody></table>
     </td>
  </tr>
</tbody></table><div class="yj6qo"></div><div class="adL">

</div></div>
',
                    'status' => '1'
                ]);

                $permission_id = [
                    [
                        'role_id' => 1,
                        'perm_id' => 38
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 39
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 40
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 41
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 42
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 43
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 44
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 45
                    ],
                    [
                        'role_id' => 1,
                        'perm_id' => 46
                    ]
                ];

                foreach ($permission_id as $perm_id) {
                    AdminRolePermission::create($perm_id);
                }

                $all_clients = Client::all();

                foreach($all_clients as $client){
                    $exist_gateway = $client->sms_gateway;
                    $insert_gateway = "[\"$exist_gateway\"]";
                    $client->sms_gateway = $insert_gateway;
                    $client->save();
                }

                $sms_sql = <<<EOF
ALTER TABLE `sys_sms_gateways` DROP `username`;
ALTER TABLE `sys_sms_gateways` DROP `password`;
ALTER TABLE `sys_sms_gateways` DROP `api_id`;
EOF;
                \DB::connection()->getPdo()->exec($sms_sql);

                $new_sms_gateways = [
                    [
                        'name' => 'Tyntec',
                        'settings' => 'Tyntec',
                        'api_link' => 'https://rest.tyntec.com/sms/v1/outbound/requests',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'TobeprecisesmsSMPP',
                        'settings' => 'TobeprecisesmsSMPP',
                        'api_link' => 'IP_Address/HostName',
                        'type' => 'smpp',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Onehop',
                        'settings' => 'Onehop',
                        'api_link' => 'http://api.onehop.co/v1/sms/send/',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'TigoBeekun',
                        'settings' => 'TigoBeekun',
                        'api_link' => 'https://tigo.beekun.com/pushapi',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'MubasherSMS',
                        'settings' => 'MubasherSMS',
                        'api_link' => 'http://www.mubashersms.com/sendsms/default.aspx',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Advansystelecom',
                        'settings' => 'Advansystelecom',
                        'api_link' => 'http://www.advansystelecom.com/AdvansysBulk/Message_Request.aspx',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Beepsend',
                        'settings' => 'Beepsend',
                        'api_link' => 'https://api.beepsend.com/2/send',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Toplusms',
                        'settings' => 'Toplusms',
                        'api_link' => 'http://www.toplusms.com.tr/api/mesaj_gonder',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'AlertSMS',
                        'settings' => 'AlertSMS',
                        'api_link' => 'http://client.alertsms.ro/api/v2',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Easy',
                        'settings' => 'Easy',
                        'api_link' => 'http://app.easy.com.np/easyApi',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Clxnetworks',
                        'settings' => 'Clxnetworks',
                        'api_link' => 'http://sms1.clxnetworks.net:3800/sendsms',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Textmarketer',
                        'settings' => 'Textmarketer',
                        'api_link' => 'https://api.textmarketer.co.uk/gateway/',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Bhashsms',
                        'settings' => 'Bhashsms',
                        'api_link' => 'http://bhashsms.com/api/sendmsg.php',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'KingTelecom',
                        'settings' => 'KingTelecom',
                        'api_link' => 'http://sms.kingtelecom.com.br/kingsms/api.php',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Diafaan',
                        'settings' => 'Diafaan',
                        'api_link' => 'https://127.0.0.1:8080',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'Yes',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Smsmisr',
                        'settings' => 'Smsmisr',
                        'api_link' => 'https://www.smsmisr.com/api/send',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ],
                    [
                        'name' => 'Broadnet',
                        'settings' => 'Broadnet',
                        'api_link' => 'http://104.156.253.108:8008/websmpp',
                        'type' => 'http',
                        'status' => 'Inactive',
                        'two_way' => 'No',
                        'mms' => 'No',
                        'voice' => 'No'
                    ]
                ];

                foreach ($new_sms_gateways as $n_gateway) {
                    $exist = SMSGateways::where('settings', $n_gateway)->first();
                    if (!$exist) {
                        SMSGateways::create($n_gateway);
                    }
                }

                SMSGateways::where('settings','Twilio')->update(['mms' => 'Yes','voice' => 'Yes']);
                SMSGateways::where('settings','Text Local')->update(['mms' => 'Yes']);
                SMSGateways::where('settings','Plivo')->update(['voice' => 'Yes']);
                SMSGateways::where('settings','SMSGlobal')->update(['mms' => 'Yes']);
                SMSGateways::where('settings','Nexmo')->update(['voice' => 'Yes']);
                SMSGateways::where('settings','InfoBip')->update(['voice' => 'Yes']);
                SMSGateways::where('settings','MessageBird')->update(['mms' => 'Yes','voice' => 'Yes']);

                $payment_gateways = [
                    [
                        'name' => 'WebXPay',
                        'value' => 'secret_key',
                        'settings' => 'webxpay',
                        'extra_value' => 'public_key',
                        'status' => 'Active',
                    ],
                    [
                        'name' => 'CoinPayments',
                        'value' => 'merchant_id',
                        'settings' => 'coinpayments',
                        'extra_value' => 'Ipn_secret',
                        'status' => 'Active',
                    ],
                ];

                foreach ($payment_gateways as $p_gateway) {
                    PaymentGateways::create($p_gateway);

                }

                $language = Language::select('id')->get();

                foreach ($language as $l) {
                    $lan_id = $l->id;
                    $lan = [
                        [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Spam Words',
                            'lan_value' => 'Spam Words'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Block Message',
                            'lan_value' => 'Block Message'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Block',
                            'lan_value' => 'Block'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Release',
                            'lan_value' => 'Release'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'SMS release successfully',
                            'lan_value' => 'SMS release successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Add New Word',
                            'lan_value' => 'Add New Word'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Words',
                            'lan_value' => 'Words'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Word already exist',
                            'lan_value' => 'Word already exist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Word added on Spam word list',
                            'lan_value' => 'Word added on Spam word list'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Word deleted from list',
                            'lan_value' => 'Word deleted from list'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Word not found on list',
                            'lan_value' => 'Word not found on list'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'SMS Fraud Detection',
                            'lan_value' => 'SMS Fraud Detection'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Reply',
                            'lan_value' => 'Reply'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Successfully sent reply',
                            'lan_value' => 'Successfully sent reply'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Routing',
                            'lan_value' => 'Routing'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Add Operator',
                            'lan_value' => 'Add Operator'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'View Operator',
                            'lan_value' => 'View Operator'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator Name',
                            'lan_value' => 'Operator Name'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator Code',
                            'lan_value' => 'Operator Code'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Area Code',
                            'lan_value' => 'Area Code'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Area Name',
                            'lan_value' => 'Area Name'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Sample Phone Number',
                            'lan_value' => 'Sample Phone Number'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Enter a real phone number like',
                            'lan_value' => 'Enter a real phone number like'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Exist on phone number',
                            'lan_value' => 'Exist on phone number'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Voice',
                            'lan_value' => 'Voice'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'MMS',
                            'lan_value' => 'MMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Are you sure',
                            'lan_value' => 'Are you sure'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Default Price',
                            'lan_value' => 'Default Price'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Set as Global',
                            'lan_value' => 'Set as Global'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Remain country code at the beginning of the number',
                            'lan_value' => 'Remain country code at the beginning of the number'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Currency Code',
                            'lan_value' => 'Currency Code'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Currency Symbol',
                            'lan_value' => 'Currency Symbol'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Currency Symbol Position',
                            'lan_value' => 'Currency Symbol Position'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Left',
                            'lan_value' => 'Left'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Right',
                            'lan_value' => 'Right'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Currency Format',
                            'lan_value' => 'Currency Format'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Currency Decimal Digits',
                            'lan_value' => 'Currency Decimal Digits'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Basic Information',
                            'lan_value' => 'Basic Information'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Credential Setup',
                            'lan_value' => 'Credential Setup'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Credential Base Status',
                            'lan_value' => 'Credential Base Status'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'You can only active one credential information',
                            'lan_value' => 'You can only active one credential information'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Current Media',
                            'lan_value' => 'Current Media'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring SMS',
                            'lan_value' => 'Recurring SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Period',
                            'lan_value' => 'Period'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Excel',
                            'lan_value' => 'Excel'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'CSV',
                            'lan_value' => 'CSV'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Work only for Recipients number',
                            'lan_value' => 'Work only for Recipients number'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring Period',
                            'lan_value' => 'Recurring Period'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Daily',
                            'lan_value' => 'Daily'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Weekly',
                            'lan_value' => 'Weekly'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Custom Date',
                            'lan_value' => 'Custom Date'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring Time',
                            'lan_value' => 'Recurring Time'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Schedule Time Type',
                            'lan_value' => 'Schedule Time Type'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Schedule Time Using Date',
                            'lan_value' => 'Schedule Time Using Date'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Schedule Time Using File',
                            'lan_value' => 'Schedule Time Using File'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Schedule Time must contain this format',
                            'lan_value' => 'Schedule Time must contain this format'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'For Text/Plain SMS',
                            'lan_value' => 'For Text/Plain SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'For Unicode SMS',
                            'lan_value' => 'For Unicode SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'For Voice SMS',
                            'lan_value' => 'For Voice SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'For MMS SMS',
                            'lan_value' => 'For MMS SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'For Schedule SMS',
                            'lan_value' => 'For Schedule SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Balance Check',
                            'lan_value' => 'Balance Check'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Reply Message',
                            'lan_value' => 'Reply Message'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Global',
                            'lan_value' => 'Global'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Update Period',
                            'lan_value' => 'Update Period'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Update Contact',
                            'lan_value' => 'Update Contact'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Update SMS data',
                            'lan_value' => 'Update SMS data'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring Note',
                            'lan_value' => 'Recurring Note'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'The sms unit will be deducted when the recurring sms task starts. If you do not have enough sms unit then
                            its automatically stop the recurring process and sms not send to users',
                            'lan_value' => 'The sms unit will be deducted when the recurring sms task starts. If you do not have enough sms unit then
                            its automatically stop the recurring process and sms not send to users'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Send Recurring SMS File',
                            'lan_value' => 'Send Recurring SMS File'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Upload .png or .jpeg or .jpg or .gif file',
                            'lan_value' => 'Upload .png or .jpeg or .jpg or .gif file'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Your are inactive or blocked by system. Please contact with administrator',
                            'lan_value' => 'Your are inactive or blocked by system. Please contact with administrator'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'At least select one sms gateway',
                            'lan_value' => 'At least select one sms gateway'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'SMS Gateway credential not found',
                            'lan_value' => 'SMS Gateway credential not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid message type',
                            'lan_value' => 'Invalid message type'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'List name already exist',
                            'lan_value' => 'List name already exist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'List added successfully',
                            'lan_value' => 'List added successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact list not found',
                            'lan_value' => 'Contact list not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'List updated successfully',
                            'lan_value' => 'List updated successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid Phone book',
                            'lan_value' => 'Invalid Phone book'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact number already exist',
                            'lan_value' => 'Contact number already exist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact added successfully',
                            'lan_value' => 'Contact added successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact updated successfully',
                            'lan_value' => 'Contact updated successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact info not found',
                            'lan_value' => 'Contact info not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact deleted successfully',
                            'lan_value' => 'Contact deleted successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid phone numbers',
                            'lan_value' => 'Invalid phone numbers'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Phone number imported successfully',
                            'lan_value' => 'Phone number imported successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Empty field',
                            'lan_value' => 'Empty field'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Amount required',
                            'lan_value' => 'Amount required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Item quantity required',
                            'lan_value' => 'Item quantity required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Insert valid tax amount',
                            'lan_value' => 'Insert valid tax amount'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Insert valid discount amount',
                            'lan_value' => 'Insert valid discount amount'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid transaction URL, cannot continue',
                            'lan_value' => 'Invalid transaction URL, cannot continue'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Data not found',
                            'lan_value' => 'Data not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invoice not paid. Please try again',
                            'lan_value' => 'Invoice not paid. Please try again'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Unauthorized payment',
                            'lan_value' => 'Unauthorized payment'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Payment gateway not active',
                            'lan_value' => 'Payment gateway not active'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'MMS not supported in block message',
                            'lan_value' => 'MMS not supported in block message'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Insert your message',
                            'lan_value' => 'Insert your message'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'MMS not supported in two way communication',
                            'lan_value' => 'MMS not supported in two way communication'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Purchase code information updated',
                            'lan_value' => 'Purchase code information updated'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Select Client',
                            'lan_value' => 'Select Client'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Insert Sender id',
                            'lan_value' => 'Insert Sender id'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Select one credential status as Active',
                            'lan_value' => 'Select one credential status as Active'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Gateway updated successfully',
                            'lan_value' => 'Gateway updated successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'SMS Gateway not supported Voice feature',
                            'lan_value' => 'SMS Gateway not supported Voice feature'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'SMS Gateway not supported MMS feature',
                            'lan_value' => 'SMS Gateway not supported MMS feature'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file',
                            'lan_value' => 'Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'MMS file required',
                            'lan_value' => 'MMS file required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'MMS is disable in demo mode',
                            'lan_value' => 'MMS is disable in demo mode'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Message required',
                            'lan_value' => 'Message required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recipient empty',
                            'lan_value' => 'Recipient empty'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Schedule time required',
                            'lan_value' => 'Schedule time required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid Recipients',
                            'lan_value' => 'Invalid Recipients'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Invalid time format',
                            'lan_value' => 'Invalid time format'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Phone number contain in blacklist',
                            'lan_value' => 'Phone number contain in blacklist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Number added on blacklist',
                            'lan_value' => 'Number added on blacklist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Number deleted from blacklist',
                            'lan_value' => 'Number deleted from blacklist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Number not found on blacklist',
                            'lan_value' => 'Number not found on blacklist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Please check sms history for status',
                            'lan_value' => 'Please check sms history for status'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'You can not send more than 100 sms using quick sms option',
                            'lan_value' => 'You can not send more than 100 sms using quick sms option'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator already exist',
                            'lan_value' => 'Operator already exist'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator added successfully',
                            'lan_value' => 'Operator added successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Something went wrong please try again',
                            'lan_value' => 'Something went wrong please try again'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator updated successfully',
                            'lan_value' => 'Operator updated successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Operator delete successfully',
                            'lan_value' => 'Operator delete successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Start Recurring',
                            'lan_value' => 'Start Recurring'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Running',
                            'lan_value' => 'Running'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recipients required',
                            'lan_value' => 'Recipients required'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring SMS info not found',
                            'lan_value' => 'Recurring SMS info not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Message recurred successfully. Delivered in correct time',
                            'lan_value' => 'Message recurred successfully. Delivered in correct time'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring SMS stop successfully',
                            'lan_value' => 'Recurring SMS stop successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring information not found',
                            'lan_value' => 'Recurring information not found'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring SMS running successfully',
                            'lan_value' => 'Recurring SMS running successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring contact added successfully',
                            'lan_value' => 'Recurring contact added successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring contact updated successfully',
                            'lan_value' => 'Recurring contact updated successfully'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Recurring SMS period changed',
                            'lan_value' => 'Recurring SMS period changed'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Your are sending fraud message',
                            'lan_value' => 'Your are sending fraud message'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Message contain spam word',
                            'lan_value' => 'Message contain spam word'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Update Application',
                            'lan_value' => 'Update Application'
                        ]
                    ];
                    foreach ($lan as $d) {
                        LanguageData::create($d);
                    }
                }

                AppConfig::where('setting', '=', 'SoftwareVersion')->update(['value' => '2.3']);

                $message = 'Congratulation!! Application updated to Version 2.3. Now your are using latest version. Refresh your page to back your application login';

                break;

            case '2.0':
                $sql = <<<EOF
ALTER TABLE `sys_sms_history` CHANGE `send_by` `send_by` ENUM('receiver','sender','api') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `sys_clients` CHANGE `sms_limit` `sms_limit` VARCHAR(11) NOT NULL DEFAULT '0';
ALTER TABLE `sys_sms_gateways` ADD `type` ENUM('http','smpp') NOT NULL DEFAULT 'http' AFTER `custom`;
ALTER TABLE `sys_schedule_sms` CHANGE `original_msg` `message` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `sys_schedule_sms` DROP `encrypt_msg`;
ALTER TABLE `sys_schedule_sms` DROP `ip`;
ALTER TABLE `sys_schedule_sms` ADD `type` ENUM('plain','unicode') NOT NULL DEFAULT 'plain' AFTER `message`;
ALTER TABLE `sys_bulk_sms` ADD `type` ENUM('plain','unicode') NOT NULL DEFAULT 'plain' AFTER `use_gateway`;
INSERT INTO `sys_app_config` (`id`, `setting`, `value`) VALUES (NULL, 'license_type', '');
EOF;
                \DB::connection()->getPdo()->exec($sql);

                PaymentGateways::create([
                    'name' => 'Pagopar',
                    'value' => 'public_key',
                    'settings' => 'pagopar',
                    'extra_value' => 'private_key',
                    'status' => 'Active',
                ]);

                $gateways = [
                    [
                        'name' => 'Smsconnexion',
                        'api_link' => 'http://smsc.smsconnexion.com/api/gateway.aspx',
                        'username' => 'username',
                        'password' => 'passphrase',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'BrandedSMS',
                        'api_link' => 'http://www.brandedsms.net//api/sms-api.php',
                        'username' => 'username',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Ibrbd',
                        'api_link' => 'http://wdgw.ibrbd.net:8080/bagaduli/apigiso/sender.php',
                        'username' => 'username',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'TxtNation',
                        'api_link' => 'http://client.txtnation.com/gateway.php',
                        'username' => 'company',
                        'password' => 'ekey',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'TeleSign',
                        'api_link' => '',
                        'username' => 'Customer ID',
                        'password' => 'API_Key',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'JasminSMS',
                        'api_link' => 'http://127.0.0.1',
                        'username' => 'foo',
                        'password' => 'bar',
                        'api_id' => '1401',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Ezeee',
                        'api_link' => 'http://my.ezeee.pk/sendsms_url.html',
                        'username' => 'user_name',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ], [
                        'name' => 'InfoBipSMPP',
                        'api_link' => 'smpp3.infobip.com',
                        'username' => 'system_id',
                        'password' => 'password',
                        'api_id' => '8888',
                        'type' => 'smpp',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'SMSGlobalSMPP',
                        'api_link' => 'smpp.smsglobal.com',
                        'username' => 'system_id',
                        'password' => 'password',
                        'api_id' => '1775',
                        'type' => 'smpp',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'ClickatellSMPP',
                        'api_link' => 'smpp.clickatell.com',
                        'username' => 'system_id',
                        'password' => 'password',
                        'api_id' => '2775',
                        'type' => 'smpp',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'JasminSmsSMPP',
                        'api_link' => 'host_name',
                        'username' => 'system_id',
                        'password' => 'password',
                        'api_id' => 'port',
                        'type' => 'smpp',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'WavecellSMPP',
                        'api_link' => 'smpp.wavecell.com',
                        'username' => 'system_id',
                        'password' => 'password',
                        'api_id' => '2775',
                        'type' => 'smpp',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Moreify',
                        'api_link' => 'https://mapi.moreify.com/api/v1/sendSms',
                        'username' => 'project_id',
                        'password' => 'your_token',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Digitalreachapi',
                        'api_link' => 'https://digitalreachapi.dialog.lk/camp_req.php',
                        'username' => 'user_name',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Tropo',
                        'api_link' => 'https://api.tropo.com/1.0/sessions',
                        'username' => 'api_token',
                        'password' => '',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'CheapSMS',
                        'api_link' => 'http://198.24.149.4/API/pushsms.aspx',
                        'username' => 'loginID',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'CCSSMS',
                        'api_link' => 'http://193.58.235.30:8001/api',
                        'username' => 'Username',
                        'password' => 'Password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'MyCoolSMS',
                        'api_link' => 'http://www.my-cool-sms.com/api-socket.php',
                        'username' => 'Username',
                        'password' => 'Password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'SmsBump',
                        'api_link' => 'https://api.smsbump.com/send',
                        'username' => 'API_KEY',
                        'password' => '',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'BSG',
                        'api_link' => '',
                        'username' => 'API_KEY',
                        'password' => '',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'SmsBroadcast',
                        'api_link' => 'https://api.smsbroadcast.co.uk/api-adv.php',
                        'username' => 'username',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'BullSMS',
                        'api_link' => 'http://portal.bullsms.com/vendorsms/pushsms.aspx',
                        'username' => 'user',
                        'password' => 'password',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ],
                    [
                        'name' => 'Skebby',
                        'api_link' => 'https://api.skebby.it/API/v1.0/REST/sms',
                        'username' => 'User_key',
                        'password' => 'Access_Token',
                        'api_id' => '',
                        'type' => 'http',
                        'two_way' => 'No'
                    ]
                ];

                foreach ($gateways as $g) {
                    $exist = SMSGateways::where('name', $g)->first();
                    if (!$exist) {
                        SMSGateways::create($g);
                    }
                }


                $language = Language::select('id')->get();

                foreach ($language as $l) {
                    $lan_id = $l->id;
                    $lan = [
                        [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Edit Contact',
                            'lan_value' => 'Edit Contact'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Bulk Delete',
                            'lan_value' => 'Bulk Delete'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'File Uploading.. Please wait',
                            'lan_value' => 'File Uploading.. Please wait'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Contact importing.. Please wait',
                            'lan_value' => 'Contact importing.. Please wait'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Send Quick SMS',
                            'lan_value' => 'Send Quick SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Remove Duplicate',
                            'lan_value' => 'Remove Duplicate'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Message Type',
                            'lan_value' => 'Message Type'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Plain',
                            'lan_value' => 'Plain'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Unicode',
                            'lan_value' => 'Unicode'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Message adding in Queue.. Please wait',
                            'lan_value' => 'Message adding in Queue.. Please wait'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Purchase Code',
                            'lan_value' => 'Purchase Code'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Search Condition',
                            'lan_value' => 'Search Condition'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Receive SMS',
                            'lan_value' => 'Receive SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'API SMS',
                            'lan_value' => 'API SMS'
                        ], [
                            'lan_id' => $lan_id,
                            'lan_data' => 'Search',
                            'lan_value' => 'Search'
                        ]
                    ];
                    foreach ($lan as $d) {
                        LanguageData::create($d);
                    }
                }

                AppConfig::where('setting', '=', 'SoftwareVersion')->update(['value' => '2.2']);
                AppConfig::where('setting', '=', 'purchase_key')->update(['value' => $purchase_code]);
                AppConfig::where('setting', '=', 'purchase_code_error_count')->update(['value' => 0]);
                AppConfig::where('setting', '=', 'license_type')->update(['value' => $data['license_type']]);
                AppConfig::where('setting', '=', 'valid_domain')->update(['value' => 'yes']);

                $message = 'You are in version 2.2 now. Latest version 2.3 Please refresh your page again for latest version';

                break;

            case '1.1':
            case '1.2':
            case '1.5':
            case 'default':
                $message = 'Please contact with application author. Author email address: akasham67@gmail.com';
                break;

        }

        return redirect('update')->with([
            'message' => $message
        ]);


    }


    //======================================================================
    // verifyProductUpdate Function Start Here
    //======================================================================
    public function verifyProductUpdate()
    {
        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('/')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        if (app_config('SoftwareVersion') == '2.3') {
            return redirect('/')->with([
                'message' => 'You are already in latest version'
            ]);
        }

        return view('admin.verify-product-update', compact('file'));
    }


}
