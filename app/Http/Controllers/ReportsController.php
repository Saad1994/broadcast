<?php

namespace App\Http\Controllers;

use App\BlockMessage;
use App\Classes\Permission;
use App\Client;
use App\CustomSMSGateways;
use App\Jobs\SendBulkSMS;
use App\Jobs\SendBulkVoice;
use App\ScheduleSMS;
use App\SMSGatewayCredential;
use App\SMSGateways;
use App\SMSHistory;
use App\SMSInbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class ReportsController extends Controller
{

    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    //======================================================================
    // smsHistory Function Start Here
    //======================================================================
    public function smsHistory()
    {

        $self = 'sms-history';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }
        return view('admin.sms-history');
    }

    //======================================================================
    // smsViewInbox Function Start Here
    //======================================================================
    public function smsViewInbox($id)
    {

        $self = 'sms-history';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }


        $inbox_info = SMSHistory::find($id);

        if ($inbox_info) {
            return view('admin.sms-inbox', compact('inbox_info'));
        } else {
            return redirect('sms/history')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // deleteSMS Function Start Here
    //======================================================================
    public function deleteSMS($id)
    {

        $self = 'sms-history';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }


        $inbox_info = SMSHistory::find($id);

        if ($inbox_info) {
            $inbox_info->delete();
            return redirect('sms/history')->with([
                'message' => language_data('SMS info deleted successfully')
            ]);

        } else {
            return redirect('sms/history')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // getSmsHistoryData Function Start Here
    //======================================================================
    public function getSmsHistoryData(Request $request)
    {

        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];

            if ($get_search_column == 'date') {
                $get_search_column = 'updated_at';
            }

        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }

        $sms_history = SMSHistory::select(['id', 'sender', 'receiver', 'amount', 'status', 'send_by', 'updated_at'])->orderBy($get_search_column, $short_by);
        return Datatables::of($sms_history)
            ->addColumn('action', function ($sms) {
                $reply_url = '';
                if ($sms->send_by == 'receiver') {
                    $reply_url .= ' <a class="btn btn-complete btn-xs reply_message" href="#" id="' . $sms->id . '"><i class="fa fa-reply"></i> ' . language_data('Reply') . ' </a>';
                }
                return $reply_url . '
                <a class="btn btn-success btn-xs" href="' . url("sms/view-inbox/$sms->id") . '" ><i class="fa fa-inbox"></i> ' . language_data('Inbox') . '</a>
                <a href="#" id="' . $sms->id . '" class="cdelete btn btn-xs btn-danger"><i class="fa fa-trash"></i> ' . language_data('Delete') . '</a>
                ';
            })
            ->addColumn('date', function ($sms) {
                return $sms->updated_at;
            })
            ->addColumn('id', function ($sms) {
                return "<div class='coder-checkbox'>
                             <input type='checkbox'  class='deleteRow' value='$sms->id'  />
                                            <span class='co-check-ui'></span>
                                        </div>";

            })
            ->filter(function ($query) use ($request) {

                if ($request->has('send_by') && $request->get('send_by') != '0') {
                    $query->where('send_by', $request->get('send_by'));
                }

                if ($request->has('sender')) {
                    $query->where('sender', 'like', "%{$request->get('sender')}%");
                }

                if ($request->has('receiver')) {
                    $query->where('receiver', 'like', "%{$request->get('receiver')}%");
                }

                if ($request->has('status')) {
                    $query->where('status', 'like', "%{$request->get('status')}%");
                }

                if ($request->has('date_from') && $request->has('date_to')) {
                    $date_from = date('Y-m-d H:i:s', strtotime($request->get('date_from')));
                    $date_to = date('Y-m-d H:i:s', strtotime($request->get('date_to')));
                    $query->whereBetween('updated_at', [$date_from, $date_to]);
                }
            })
            ->addColumn('send_by', function ($sms) {
                if ($sms->send_by == 'sender') {
                    return language_data('Outgoing');
                } elseif ($sms->send_by == 'api') {
                    return '<span class="text-info">' . language_data('API SMS') . ' </span>';
                } else {
                    return '<span class="text-success"> ' . language_data('Incoming') . ' </span>';
                }
            })
            ->escapeColumns([])
            ->make(true);


    }

    //======================================================================
    // bulkDeleteSMS Function Start Here
    //======================================================================
    public function bulkDeleteSMS(Request $request)
    {
        $self = 'sms-history';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        if ($request->has('data_ids')) {
            $all_ids = explode(',', $request->get('data_ids'));

            if (is_array($all_ids) && count($all_ids) > 0) {
                SMSHistory::destroy($all_ids);
            }
        }

    }

    //======================================================================
    // Version 2.3
    //======================================================================

    //======================================================================
    // blockMessage Function Start Here
    //======================================================================
    public function blockMessage()
    {
        $self = 'block-message';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        return view('admin.block-message');
    }



    //======================================================================
    // getBlockMessageData Function Start Here
    //======================================================================
    public function getBlockMessageData()
    {
        $block_message = BlockMessage::select(['id', 'sender', 'user_id', 'message', 'status', 'use_gateway', 'updated_at'])->orderBy('updated_at', 'DESC');
        return Datatables::of($block_message)
            ->addColumn('action', function ($sms) {
                return '
                <a href="#" id="' . $sms->id . '" class="crelease btn btn-xs btn-complete"><i class="fa fa-check"></i> ' . language_data('Release') . '</a>
                <a class="btn btn-success btn-xs" href="' . url("sms/view-block-message/$sms->id") . '" ><i class="fa fa-inbox"></i>' . language_data('View') . '</a>
                <a href="#" id="' . $sms->id . '" class="cdelete btn btn-xs btn-danger"><i class="fa fa-trash"></i> ' . language_data('Delete') . '</a>
                ';
            })
            ->addColumn('user_id', function ($sms) {
                if ($sms->user_id == 0) {
                    return language_data('Admin');
                } else {
                    return '
                    <a href=' . url('clients/view/' . $sms->user_id) . '>' . client_info($sms->user_id)->username . '</a>
                    ';
                }
            })
            ->addColumn('status', function ($sms) {
                if ($sms->status == 'block') {
                    return '<span class="text-danger"> ' . language_data('Block') . ' </span>';
                } else {
                    return '<span class="text-success"> ' . language_data('Release') . ' </span>';
                }
            })
            ->addColumn('date', function ($sms) {
                return $sms->updated_at;
            })
            ->escapeColumns([])
            ->make(true);
    }



    //======================================================================
    // viewBlockMessage Function Start Here
    //======================================================================
    public function viewBlockMessage($id)
    {
        $self = 'block-message';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        $inbox_info = BlockMessage::find($id);

        if ($inbox_info) {
            return view('admin.view-block-message', compact('inbox_info'));
        } else {
            return redirect('sms/block-message')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // releaseBlockMessage Function Start Here
    //======================================================================
    public function releaseBlockMessage($id)
    {


       $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('sms/block-message')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }
        
        $self = 'block-message';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        $inbox_info = BlockMessage::find($id);

        if ($inbox_info) {

            $message = $inbox_info->message;
            $msg_type = $inbox_info->type;

            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                if ($msgcount <= 160) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 157;
                }
            }
            if ($msg_type == 'unicode') {
                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                if ($msgcount <= 70) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 67;
                }
            }

            $msgcount = ceil($msgcount);


            $gateway = SMSGateways::find($inbox_info->use_gateway);

            $gateway_credential = null;
            $cg_info = null;
            if ($gateway->custom == 'Yes') {
                if ($gateway->type == 'smpp'){
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                    if ($gateway_credential == null) {
                        return redirect('sms/block-message')->with([
                            'message' => language_data('SMS Gateway credential not found'),
                            'message_important' => true
                        ]);
                    }
                }else{
                    $cg_info = CustomSMSGateways::where('gateway_id', $gateway->id)->first();
                }

            } else {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('sms/block-message')->with([
                        'message' => language_data('SMS Gateway credential not found'),
                        'message_important' => true
                    ]);
                }
            }


            if ($inbox_info->scheduled_time === null) {

                if ($msg_type == 'plain' || $msg_type == 'unicode') {
                    $this->dispatch(new SendBulkSMS($inbox_info->user_id, $inbox_info->receiver, $gateway, $gateway_credential, $inbox_info->sender, $message, $msgcount, $cg_info, '', $msg_type));
                }

                if ($msg_type == 'voice') {
                    $this->dispatch(new SendBulkVoice($inbox_info->user_id, $inbox_info->receiver, $gateway, $gateway_credential, $inbox_info->sender, $message, $msgcount));
                }

                if ($msg_type == 'mms') {
                    return redirect('sms/block-message')->with([
                        'message' => language_data('MMS not supported in block message'),
                        'message_important' => true
                    ]);
                }

            } else {

                ScheduleSMS::create([
                    'userid' => $inbox_info->user_id,
                    'sender' => $inbox_info->sender,
                    'receiver' => $inbox_info->receiver,
                    'amount' => $msgcount,
                    'message' => $message,
                    'type' => $msg_type,
                    'submit_time' => $inbox_info->scheduled_time,
                    'use_gateway' => $gateway->id
                ]);
            }

            //call jobs

            $inbox_info->delete();

            return redirect('sms/block-message')->with([
                'message' => language_data('SMS release successfully')
            ]);

        } else {
            return redirect('sms/block-message')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }
    }



    //======================================================================
    // deleteBlockMessage Function Start Here
    //======================================================================
    public function deleteBlockMessage($id)
    {
        $self = 'block-message';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        $inbox_info = BlockMessage::find($id);

        if ($inbox_info) {
            //refund money to client

            $msg_type = $inbox_info->type;
            $message = $inbox_info->message;

            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                if ($msgcount <= 160) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 157;
                }
            }
            if ($msg_type == 'unicode') {
                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                if ($msgcount <= 70) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 67;
                }
            }

            $msgcount = ceil($msgcount);

            $client = Client::find($inbox_info->user_id);
            $client->sms_limit = $client->sms_limit + $msgcount;
            $client->save();

            $inbox_info->delete();
            return redirect('sms/block-message')->with([
                'message' => language_data('SMS info deleted successfully')
            ]);

        } else {
            return redirect('sms/block-message')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // postReplySMS Function Start Here
    //======================================================================
    public function postReplySMS($cmd, $message)
    {
        $self = 'sms-history';
        if (Auth::user()->username !== 'admin') {
            $get_perm = Permission::permitted($self);

            if ($get_perm == 'access denied') {
                return redirect('permission-error')->with([
                    'message' => language_data('You do not have permission to view this page'),
                    'message_important' => true
                ]);
            }
        }

        if ($message == '') {
            return redirect('sms/history')->with([
                'message' => language_data('Insert your message'),
                'message_important' => true
            ]);
        }

        $h = SMSHistory::find($cmd);

        if ($h) {
            $gateway = SMSGateways::find($h->use_gateway);

            if ($gateway->status != 'Active') {
                return redirect('sms/history')->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider'),
                    'message_important' => true
                ]);
            }

            $gateway_credential = null;
            $cg_info = null;
            if ($gateway->custom == 'Yes') {
                if ($gateway->type == 'smpp'){
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                    if ($gateway_credential == null) {
                        return redirect('sms/history')->with([
                            'message' => language_data('SMS Gateway credential not found'),
                            'message_important' => true
                        ]);
                    }
                }else{
                    $cg_info = CustomSMSGateways::where('gateway_id', $gateway->id)->first();
                }

            } else {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('sms/history')->with([
                        'message' => language_data('SMS Gateway credential not found'),
                        'message_important' => true
                    ]);
                }
            }

            $sender_id = $h->receiver;
            $msg_type = $h->sms_type;
            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                if ($msgcount <= 160) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 157;
                }
            }
            if ($msg_type == 'unicode') {
                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                if ($msgcount <= 70) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 67;
                }
            }

            $msgcount = ceil($msgcount);

            if ($msg_type == 'plain' || $msg_type == 'unicode') {
                $this->dispatch(new SendBulkSMS(0, $h->sender, $gateway, $gateway_credential, $sender_id, $message, $msgcount, $cg_info, '', $msg_type));
            }

            if ($msg_type == 'voice') {
                $this->dispatch(new SendBulkVoice(0, $h->sender, $gateway, $gateway_credential, $sender_id, $message, $msgcount));
            }

            if ($msg_type == 'mms') {
                return redirect('sms/history')->with([
                    'message' => language_data('MMS not supported in two way communication'),
                    'message_important' => true
                ]);
            }

            return redirect('sms/history')->with([
                'message' => language_data('Successfully sent reply')
            ]);

        } else {
            return redirect('sms/history')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }
}
