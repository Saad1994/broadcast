<?php

namespace App\Http\Controllers;

use App\Admin;
use App\BlackListContact;
use App\BlockMessage;
use App\Classes\PhoneNumber;
use App\Client;
use App\ClientGroups;
use App\ContactList;
use App\CustomSMSGateways;
use App\EmailTemplates;
use App\ImportPhoneNumber;
use App\IntCountryCodes;
use App\Jobs\SendBulkMMS;
use App\Jobs\SendBulkSMS;
use App\Jobs\SendBulkVoice;
use App\Operator;
use App\PaymentGateways;
use App\RecurringSMS;
use App\RecurringSMSContacts;
use App\ScheduleSMS;
use App\SenderIdManage;
use App\SMSBundles;
use App\SMSGatewayCredential;
use App\SMSGateways;
use App\SMSHistory;
use App\SMSPlanFeature;
use App\SMSPricePlan;
use App\SMSTemplates;
use App\SpamWord;
use App\StoreBulkSMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberToCarrierMapper;
use libphonenumber\PhoneNumberUtil;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;

class UserSMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('client');
    }

    //======================================================================
    // senderIdManagement Function Start Here
    //======================================================================
    public function senderIdManagement()
    {

        $all_sender_id = SenderIdManage::where('status', 'unblock')->orWhere('status', 'Pending')->get();
        $all_ids = [];

        foreach ($all_sender_id as $sid) {
            $client_array = json_decode($sid->cl_id);

            if (is_array($client_array) && in_array('0', $client_array)) {
                array_push($all_ids, $sid->id);
            } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                array_push($all_ids, $sid->id);
            }
        }
        $sender_ids = array_unique($all_ids);

        $sender_id = SenderIdManage::whereIn('id', $sender_ids)->get();

        return view('client.sender-id-management', compact('sender_id'));
    }

    //======================================================================
    // postSenderID Function Start Here
    //======================================================================
    public function postSenderID(Request $request)
    {
        if ($request->sender_id == '') {
            return redirect('user/sms/sender-id-management')->with([
                'message' => language_data('Sender ID required', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $client_id = (string)Auth::guard('client')->user()->id;
        $client_id = (array)$client_id;
        $client_id = json_encode($client_id);

        $sender_id = new  SenderIdManage();
        $sender_id->sender_id = $request->sender_id;
        $sender_id->cl_id = $client_id;
        $sender_id->status = 'pending';
        $sender_id->save();

        return redirect('user/sms/sender-id-management')->with([
            'message' => language_data('Request send successfully', Auth::guard('client')->user()->lan_id)
        ]);
    }


    //======================================================================
    // sendBulkSMS Function Start Here
    //======================================================================
    public function sendBulkSMS()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $phone_book = ImportPhoneNumber::where('user_id', Auth::guard('client')->user()->id)->get();
        $client_group = ClientGroups::where('created_by', Auth::guard('client')->user()->id)->where('status', 'Yes')->get();
        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', 1)->select('country_code', 'country_name')->get();

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        $schedule_sms = false;

        return view('client.send-bulk-sms', compact('client_group', 'sms_templates', 'sender_ids', 'phone_book', 'schedule_sms', 'country_code', 'sms_gateways'));
    }

    //======================================================================
    // postSendBulkSMS Function Start Here
    //======================================================================
    public function postSendBulkSMS(Request $request)
    {

        if (function_exists('ini_set') && ini_get('max_execution_time')) {
            ini_set('max_execution_time', '-1');
        }

        if ($request->schedule_sms_status) {
            $v = \Validator::make($request->all(), [
                'schedule_time' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required', 'sms_gateway' => 'required'
            ]);

            $redirect_url = 'user/sms/send-schedule-sms';
        } else {
            $v = \Validator::make($request->all(), [
                'message_type' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required', 'sms_gateway' => 'required'
            ]);

            $redirect_url = 'user/sms/send-sms';
        }

        if ($v->fails()) {
            return redirect($redirect_url)->withInput($request->all())->withErrors($v->errors());
        }


        $client = Client::find(Auth::guard('client')->user()->id);

        $sms_count = $client->sms_limit;
        $sender_id = $request->sender_id;
        $message = $request->message;
        $msg_type = $request->message_type;

        if (app_config('sender_id_verification') == '1') {
            if ($sender_id == null) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($sender_id != '' && app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::all();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $all_ids = array_unique($all_ids);

            if (!in_array($sender_id, $all_ids)) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $gateway = SMSGateways::find($request->sms_gateway);

        if ($gateway->status != 'Active') {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $gateway_credential = null;
        if ($gateway->custom == 'Yes') {
            if ($gateway->type == 'smpp') {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect($redirect_url)->withInput($request->all())->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }
        } else {
            $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
            if ($gateway_credential == null) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($msg_type == 'voice') {
            if ($gateway->voice != 'Yes') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type == 'mms') {

            if ($gateway->mms != 'Yes') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $image = $request->image;

            if ($image == '') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('MMS file required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if (app_config('AppStage') != 'Demo') {
                if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                    $destinationPath = public_path() . '/assets/mms_file/';
                    $image_name = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $image_name);
                    $media_url = asset('assets/mms_file/' . $image_name);

                } else {
                    return redirect($redirect_url)->withInput($request->all())->with([
                        'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

            } else {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            $media_url = null;
            if ($message == '') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        $get_cost = 0;
        $total_cost = 0;
        $get_inactive_coverage = [];
        $results = [];

        if ($request->contact_type == 'phone_book') {
            if (count($request->contact_list_id)) {
                $get_data = ContactList::whereIn('pid', $request->contact_list_id)->select('phone_number', 'email_address', 'user_name', 'company', 'first_name', 'last_name')->get()->toArray();
                foreach ($get_data as $data) {
                    array_push($results, $data);
                }
            }
        }

        if ($request->contact_type == 'client_group') {
            $get_group = Client::whereIn('groupid', $request->client_group_id)->select('phone AS phone_number', 'email AS email_address', 'username AS user_name', 'company AS company', 'fname AS first_name', 'lname AS last_name')->get()->toArray();
            foreach ($get_group as $data) {
                array_push($results, $data);
            }
        }

        if ($request->recipients) {
            $recipients = multi_explode(array(",", "\n", ";"), $request->recipients);
            foreach ($recipients as $r) {

                $phone = str_replace(['(', ')', '+', '-', ' '], '', $r);
                if ($request->country_code != 0) {
                    $phone = $request->country_code . ltrim($phone, '0');
                }

                $data = [
                    'phone_number' => $phone,
                    'email_address' => null,
                    'user_name' => null,
                    'company' => null,
                    'first_name' => null,
                    'last_name' => null
                ];
                array_push($results, $data);
            }
        }


        if (is_array($results)) {

            if (count($results) > 0) {

                $filtered_data = [];
                $blacklist = BlackListContact::select('numbers')->get()->toArray();

                if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {

                    $blacklist = array_column($blacklist, 'numbers');

                    array_filter($results, function ($element) use ($blacklist, &$filtered_data) {
                        if (!in_array($element['phone_number'], $blacklist)) {
                            array_push($filtered_data, $element);
                        }
                    });

                    $results = array_values($filtered_data);
                }

                if (count($results) <= 0) {
                    return redirect($redirect_url)->withInput($request->all())->with([
                        'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($request->remove_duplicate == 'yes') {
                    $results = unique_multidim_array($results, 'phone_number');
                }

                $results = array_values($results);

                foreach (array_chunk($results, 50) as $chunk_result) {
                    foreach ($chunk_result as $r) {
                        $msg_data = array(
                            'Phone Number' => $r['phone_number'],
                            'Email Address' => $r['email_address'],
                            'User Name' => $r['user_name'],
                            'Company' => $r['company'],
                            'First Name' => $r['first_name'],
                            'Last Name' => $r['last_name'],
                        );


                        $get_message = $this->renderSMS($message, $msg_data);

                        if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                            $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                            if ($msgcount <= 160) {
                                $msgcount = 1;
                            } else {
                                $msgcount = $msgcount / 157;
                            }
                        }
                        if ($msg_type == 'unicode') {
                            $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                            if ($msgcount <= 70) {
                                $msgcount = 1;
                            } else {
                                $msgcount = $msgcount / 67;
                            }
                        }

                        $msgcount = ceil($msgcount);
                        $phone = $r['phone_number'];

                        if ($request->country_code == 0) {
                            if ($gateway->settings == 'FortDigital') {
                                $c_phone = 61;
                            } elseif ($gateway->settings == 'Ibrbd') {
                                $c_phone = 880;
                            } else {
                                $c_phone = PhoneNumber::get_code($phone);
                            }
                        } else {
                            $c_phone = $request->country_code;
                        }

                        $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
                        if ($sms_cost) {

                            $phoneUtil = PhoneNumberUtil::getInstance();
                            $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                            $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                            if ($area_code_exist) {
                                $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                                $get_format_data = explode(" ", $format);
                                $operator_settings = explode('-', $get_format_data[1])[0];

                            } else {
                                $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                                $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                            }

                            $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                            if ($get_operator) {
                                $sms_charge = $get_operator->price;
                                $get_cost += $sms_charge;
                            } else {
                                $sms_charge = $sms_cost->tariff;
                                $get_cost += $sms_charge;
                            }
                        } else {
                            array_push($get_inactive_coverage, 'found');
                        }


                        if (in_array('found', $get_inactive_coverage)) {
                            return redirect($redirect_url)->withInput($request->all())->with([
                                'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                                'message_important' => true
                            ]);
                        }

                        $total_cost = $get_cost * $msgcount;

                        if ($total_cost == 0) {
                            return redirect($redirect_url)->withInput($request->all())->with([
                                'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                                'message_important' => true
                            ]);
                        }

                        if ($total_cost > $sms_count) {
                            return redirect($redirect_url)->withInput($request->all())->with([
                                'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                                'message_important' => true
                            ]);
                        }
                    }
                }


                if (app_config('fraud_detection') == 1) {
                    $spam_word = SpamWord::all()->toArray();
                    if (is_array($spam_word) && count($spam_word) > 0) {
                        $spam_word = array_column($spam_word, 'word');
                        $admin = Admin::where('username', 'admin')->first();
                        $admin_email = $admin->email;
                        $admin_name = $admin->fname . ' ' . $admin->lname;

                        $conf = EmailTemplates::where('tplname', '=', 'Spam Word Notification')->first();
                        $estatus = $conf->status;
                    }
                }


                if ($request->send_later == 'on') {

                    if ($request->schedule_time == '') {
                        return redirect($redirect_url)->with([
                            'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    $schedule_time = date('Y-m-d H:i:s', strtotime($request->schedule_time));

                    foreach (array_chunk($results, 50) as $chunk_result) {
                        foreach ($chunk_result as $r) {

                            $msg_data = array(
                                'Phone Number' => $r['phone_number'],
                                'Email Address' => $r['email_address'],
                                'User Name' => $r['user_name'],
                                'Company' => $r['company'],
                                'First Name' => $r['first_name'],
                                'Last Name' => $r['last_name'],
                            );

                            $get_message = $this->renderSMS($message, $msg_data);


                            if (app_config('fraud_detection') == 1) {
                                if (is_array($spam_word) && count($spam_word) > 0) {
                                    $search_word = implode('|', $spam_word);
                                    if (preg_match('(' . $search_word . ')', $get_message) === 1) {

                                        BlockMessage::create([
                                            'user_id' => Auth::guard('client')->user()->id,
                                            'sender' => $sender_id,
                                            'receiver' => $r['phone_number'],
                                            'message' => $get_message,
                                            'scheduled_time' => $schedule_time,
                                            'use_gateway' => $gateway->id,
                                            'status' => 'block',
                                            'type' => $msg_type
                                        ]);


                                        if ($estatus == '1') {

                                            $sysEmail = app_config('Email');
                                            $sysCompany = app_config('AppName');
                                            $sysUrl = url('clients/view/' . Auth::guard('client')->user()->id);

                                            $template = $conf->message;
                                            $subject = $conf->subject;

                                            $data = array(
                                                'user_name' => Auth::guard('client')->user()->username,
                                                'business_name' => $sysCompany,
                                                'message' => $get_message,
                                                'profile_link' => $sysUrl,
                                                'template' => $template
                                            );

                                            $body = _render($template, $data);
                                            $mail_subject = _render($subject, $data);

                                            /*Set Authentication*/

                                            $default_gt = app_config('Gateway');

                                            if ($default_gt == 'default') {

                                                $mail = new \PHPMailer();

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();

                                            } else {
                                                $host = app_config('SMTPHostName');
                                                $smtp_username = app_config('SMTPUserName');
                                                $stmp_password = app_config('SMTPPassword');
                                                $port = app_config('SMTPPort');
                                                $secure = app_config('SMTPSecure');


                                                $mail = new \PHPMailer();

                                                $mail->isSMTP();                                      // Set mailer to use SMTP
                                                $mail->Host = $host;  // Specify main and backup SMTP servers
                                                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                                $mail->Username = $smtp_username;                 // SMTP username
                                                $mail->Password = $stmp_password;                           // SMTP password
                                                $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                                                $mail->Port = $port;

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();


                                            }
                                        }

                                        continue;
                                    }
                                }
                            }

                            ScheduleSMS::create([
                                'userid' => Auth::guard('client')->user()->id,
                                'sender' => $sender_id,
                                'receiver' => $r['phone_number'],
                                'amount' => $msgcount,
                                'message' => $get_message,
                                'type' => $msg_type,
                                'submit_time' => $schedule_time,
                                'use_gateway' => $gateway->id,
                                'media_url' => $media_url
                            ]);

                        }
                    }

                } else {

                    $final_insert_data = [];

                    foreach (array_chunk($results, 50) as $chunk_result) {
                        foreach ($chunk_result as $r) {
                            $msg_data = array(
                                'Phone Number' => $r['phone_number'],
                                'Email Address' => $r['email_address'],
                                'User Name' => $r['user_name'],
                                'Company' => $r['company'],
                                'First Name' => $r['first_name'],
                                'Last Name' => $r['last_name'],
                            );

                            $get_message = $this->renderSMS($message, $msg_data);

                            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                                if ($msgcount <= 160) {
                                    $msgcount = 1;
                                } else {
                                    $msgcount = $msgcount / 157;
                                }
                            }
                            if ($msg_type == 'unicode') {
                                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                                if ($msgcount <= 70) {
                                    $msgcount = 1;
                                } else {
                                    $msgcount = $msgcount / 67;
                                }
                            }

                            $msgcount = ceil($msgcount);

                            $phone = $r['phone_number'];

                            if (app_config('fraud_detection') == 1) {
                                if (is_array($spam_word) && count($spam_word) > 0) {
                                    $search_word = implode('|', $spam_word);
                                    if (preg_match('(' . $search_word . ')', $get_message) === 1) {

                                        BlockMessage::create([
                                            'user_id' => Auth::guard('client')->user()->id,
                                            'sender' => $sender_id,
                                            'receiver' => $phone,
                                            'message' => $get_message,
                                            'scheduled_time' => null,
                                            'use_gateway' => $gateway->id,
                                            'status' => 'block',
                                            'type' => $msg_type
                                        ]);


                                        if ($estatus == '1') {

                                            $sysEmail = app_config('Email');
                                            $sysCompany = app_config('AppName');
                                            $sysUrl = url('clients/view/' . Auth::guard('client')->user()->id);

                                            $template = $conf->message;
                                            $subject = $conf->subject;

                                            $data = array(
                                                'user_name' => Auth::guard('client')->user()->username,
                                                'business_name' => $sysCompany,
                                                'message' => $get_message,
                                                'profile_link' => $sysUrl,
                                                'template' => $template
                                            );

                                            $body = _render($template, $data);
                                            $mail_subject = _render($subject, $data);

                                            /*Set Authentication*/

                                            $default_gt = app_config('Gateway');

                                            if ($default_gt == 'default') {

                                                $mail = new \PHPMailer();

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();

                                            } else {
                                                $host = app_config('SMTPHostName');
                                                $smtp_username = app_config('SMTPUserName');
                                                $stmp_password = app_config('SMTPPassword');
                                                $port = app_config('SMTPPort');
                                                $secure = app_config('SMTPSecure');


                                                $mail = new \PHPMailer();

                                                $mail->isSMTP();                                      // Set mailer to use SMTP
                                                $mail->Host = $host;  // Specify main and backup SMTP servers
                                                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                                $mail->Username = $smtp_username;                 // SMTP username
                                                $mail->Password = $stmp_password;                           // SMTP password
                                                $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                                                $mail->Port = $port;

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();


                                            }
                                        }

                                        continue;
                                    }
                                }
                            }

                            array_push($final_insert_data, [
                                'phone_number' => $phone,
                                'message' => $get_message,
                                'segments' => $msgcount,
                                'media_url' => $media_url
                            ]);
                        }
                    }


                    if (count($final_insert_data) <= 0) {
                        return redirect($redirect_url)->withInput($request->all())->with([
                            'message' => language_data('Your destination number in blacklist or you were sending fraud sms', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    $final_data = json_encode($final_insert_data, true);


                    StoreBulkSMS::create([
                        'userid' => Auth::guard('client')->user()->id,
                        'sender' => $sender_id,
                        'msg_data' => $final_data,
                        'status' => 0,
                        'type' => $msg_type,
                        'use_gateway' => $gateway->id
                    ]);

                }


                $remain_sms = $sms_count - $total_cost;
                $client->sms_limit = $remain_sms;
                $client->save();

                return redirect($redirect_url)->with([
                    'message' => language_data('SMS added in queue and will deliver one by one', Auth::guard('client')->user()->lan_id)
                ]);


            } else {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Invalid Recipients', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // purchaseSMSPlan Function Start Here
    //======================================================================
    public function purchaseSMSPlan()
    {
        $price_plan = SMSPricePlan::where('status', 'Active')->get();
        return view('client.sms-price-plan', compact('price_plan'));
    }

    //======================================================================
    // smsPlanFeature Function Start Here
    //======================================================================
    public function smsPlanFeature($id)
    {
        $sms_plan = SMSPricePlan::where('status', 'Active')->find($id);

        if ($sms_plan) {
            $plan_feature = SMSPlanFeature::where('pid', $id)->get();
            $payment_gateways = PaymentGateways::where('status', 'Active')->get();
            return view('client.sms-plan-feature', compact('sms_plan', 'plan_feature', 'payment_gateways'));
        } else {
            return redirect('user/sms/purchase-sms-plan')->with([
                'message' => language_data('SMS plan not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }

    //======================================================================
    // sendSMSFromFile Function Start Here
    //======================================================================
    public function sendSMSFromFile()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];


            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', '1')->select('country_code', 'country_name')->get();

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        $schedule_sms = false;

        return view('client.send-sms-file', compact('sms_templates', 'sender_ids', 'schedule_sms', 'country_code', 'sms_gateways'));
    }

    //======================================================================
    // downloadSampleSMSFile Function Start Here
    //======================================================================
    public function downloadSampleSMSFile()
    {
        return response()->download('assets/test_file/sms.csv');
    }

    //======================================================================
    // postSMSFromFile Function Start Here
    //======================================================================
    public function postSMSFromFile(Request $request)
    {

        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('user/sms/send-sms-file')->with([
                'message' => language_data('This Option is Disable In Demo Mode', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if (function_exists('ini_set') && ini_get('max_execution_time')) {
            ini_set('max_execution_time', '-1');
        }


        if ($request->schedule_sms_status) {
            $v = \Validator::make($request->all(), [
                'import_numbers' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required', 'sms_gateway' => 'required'
            ]);

            $redirect_url = 'user/sms/send-schedule-sms-file';
        } else {
            $v = \Validator::make($request->all(), [
                'import_numbers' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required', 'sms_gateway' => 'required'
            ]);

            $redirect_url = 'user/sms/send-sms-file';
        }


        if ($v->fails()) {
            return redirect($redirect_url)->withInput($request->all())->withErrors($v->errors());
        }


        $client = Client::find(Auth::guard('client')->user()->id);
        $sms_count = $client->sms_limit;

        $sender_id = $request->sender_id;

        if (app_config('sender_id_verification') == '1') {
            if ($sender_id == null) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($sender_id != '' && app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::all();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $all_ids = array_unique($all_ids);

            if (!in_array($sender_id, $all_ids)) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $gateway = SMSGateways::find($request->sms_gateway);

        if ($gateway->status != 'Active') {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $gateway_credential = null;
        if ($gateway->custom == 'Yes') {
            if ($gateway->type == 'smpp') {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect($redirect_url)->withInput($request->all())->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }
        } else {
            $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
            if ($gateway_credential == null) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        $msg_type = $request->message_type;
        $message = $request->message;

        if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($msg_type == 'voice') {
            if ($gateway->voice != 'Yes') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type == 'mms') {

            if ($gateway->mms != 'Yes') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $image = $request->image;

            if ($image == '') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('MMS file required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if (app_config('AppStage') != 'Demo') {
                if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                    $destinationPath = public_path() . '/assets/mms_file/';
                    $image_name = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $image_name);
                    $media_url = asset('assets/mms_file/' . $image_name);

                } else {
                    return redirect($redirect_url)->withInput($request->all())->with([
                        'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

            } else {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            $media_url = null;
            if ($message == '') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $file_extension = Input::file('import_numbers')->getClientOriginalExtension();

        $supportedExt = array('csv', 'xls', 'xlsx');

        if (!in_array_r($file_extension, $supportedExt)) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Insert Valid Excel or CSV file', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $get_cost = 0;
        $get_inactive_coverage = [];
        $valid_phone_numbers = [];
        $get_data = [];
        $final_insert_data = [];

        $all_data = Excel::load($request->import_numbers)->noHeading()->all()->toArray();

        if ($all_data && is_array($all_data) && array_empty($all_data)) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Empty field', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $counter = "A";

        if ($request->header_exist == 'on') {

            $header = array_shift($all_data);

            foreach ($header as $key => $value) {
                if (!$value) {
                    $header[$key] = "Column " . $counter;
                }

                $counter++;
            }

        } else {

            $header_like = $all_data[0];

            $header = array();

            foreach ($header_like as $h) {
                array_push($header, "Column " . $counter);
                $counter++;
            }

        }

        $all_data = array_map(function ($row) use ($header) {

            return array_combine($header, $row);

        }, $all_data);

        $blacklist = BlackListContact::select('numbers')->get()->toArray();

        if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {
            $blacklist = array_column($blacklist, 'numbers');
        }

        $number_column = $request->number_column;
        array_filter($all_data, function ($data) use ($number_column, &$get_data, &$valid_phone_numbers, $blacklist) {

            if ($data[$number_column]) {
                if (!in_array($data[$number_column], $blacklist)) {
                    array_push($valid_phone_numbers, $data[$number_column]);
                    array_push($get_data, $data);
                }
            }
        });

        if (is_array($valid_phone_numbers) && count($valid_phone_numbers) <= 0) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Invalid phone numbers', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if (count($get_data) <= 0) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($request->remove_duplicate == 'yes') {
            $valid_phone_numbers = array_unique($valid_phone_numbers, SORT_REGULAR);
        }
        $valid_phone_numbers = array_values($valid_phone_numbers);

        foreach ($valid_phone_numbers as $c) {

            $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($c));

            if ($request->country_code == 0) {
                if ($gateway->settings == 'FortDigital') {
                    $c_phone = 61;
                } elseif ($gateway->settings == 'Ibrbd') {
                    $c_phone = 880;
                } else {
                    $c_phone = PhoneNumber::get_code($phone);
                }
            } else {
                $phone = $request->country_code . ltrim($phone, '0');
                $c_phone = $request->country_code;
            }


            $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();

            if ($sms_cost) {

                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                if ($area_code_exist) {
                    $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                    $get_format_data = explode(" ", $format);
                    $operator_settings = explode('-', $get_format_data[1])[0];

                } else {
                    $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                    $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                }

                $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                if ($get_operator) {
                    $sms_charge = $get_operator->price;
                    $get_cost += $sms_charge;
                } else {
                    $sms_charge = $sms_cost->tariff;
                    $get_cost += $sms_charge;
                }

            } else {
                array_push($get_inactive_coverage, 'found');
            }
        }

        if (in_array('found', $get_inactive_coverage)) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
            $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
            if ($msgcount <= 160) {
                $msgcount = 1;
            } else {
                $msgcount = $msgcount / 157;
            }
        }
        if ($msg_type == 'unicode') {
            $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

            if ($msgcount <= 70) {
                $msgcount = 1;
            } else {
                $msgcount = $msgcount / 67;
            }
        }
        $msgcount = ceil($msgcount);


        $total_cost = $get_cost * $msgcount;

        if ($total_cost == 0) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($total_cost > $sms_count) {
            return redirect($redirect_url)->withInput($request->all())->with([
                'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        if ($request->remove_duplicate == 'yes') {
            $get_data = unique_multidim_array($get_data, $number_column);
        }
        $get_data = array_values($get_data);

        if (app_config('fraud_detection') == 1) {
            $spam_word = SpamWord::all()->toArray();
            if (is_array($spam_word) && count($spam_word) > 0) {
                $spam_word = array_column($spam_word, 'word');
                $admin = Admin::where('username', 'admin')->first();
                $admin_email = $admin->email;
                $admin_name = $admin->fname . ' ' . $admin->lname;

                $conf = EmailTemplates::where('tplname', '=', 'Spam Word Notification')->first();
                $estatus = $conf->status;
            }
        }


        if ($request->send_later == 'on') {

            if ($request->schedule_time == '' && $request->schedule_time_column == '') {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            foreach ($get_data as $msg_data) {

                if ($request->schedule_time_type == 'from_file') {
                    $schedule_time_column = $request->schedule_time_column;

                    if ($msg_data[$schedule_time_column] == '' || \DateTime::createFromFormat('m/d/Y h:i A', $msg_data[$schedule_time_column]) === FALSE) {
                        continue;
                    }

                    $schedule_time = date('Y-m-d H:i:s', strtotime($msg_data[$schedule_time_column]));
                } else {
                    if (\DateTime::createFromFormat('m/d/Y h:i A', $request->schedule_time) !== FALSE) {
                        $schedule_time = date('Y-m-d H:i:s', strtotime($request->schedule_time));
                    } else {
                        return redirect($redirect_url)->with([
                            'message' => language_data('Invalid time format', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }

                $get_message = $this->renderSMS($message, $msg_data);

                if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                    $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                    if ($msgcount <= 160) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 157;
                    }
                }
                if ($msg_type == 'unicode') {
                    $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                    if ($msgcount <= 70) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 67;
                    }
                }

                $msgcount = ceil($msgcount);
                $clphone = str_replace(['(', ')', '+', '-', ' '], '', $msg_data[$number_column]);

                if ($request->country_code != 0) {
                    $clphone = $request->country_code . ltrim($clphone, '0');
                }

                if (app_config('fraud_detection') == 1) {
                    if (is_array($spam_word) && count($spam_word) > 0) {
                        $search_word = implode('|', $spam_word);
                        if (preg_match('(' . $search_word . ')', $get_message) === 1) {

                            BlockMessage::create([
                                'user_id' => Auth::guard('client')->user()->id,
                                'sender' => $sender_id,
                                'receiver' => $clphone,
                                'message' => $get_message,
                                'scheduled_time' => $schedule_time,
                                'use_gateway' => $gateway->id,
                                'status' => 'block',
                                'type' => $msg_type
                            ]);


                            if ($estatus == '1') {

                                $sysEmail = app_config('Email');
                                $sysCompany = app_config('AppName');
                                $sysUrl = url('clients/view/' . Auth::guard('client')->user()->id);

                                $template = $conf->message;
                                $subject = $conf->subject;

                                $data = array(
                                    'user_name' => Auth::guard('client')->user()->username,
                                    'business_name' => $sysCompany,
                                    'message' => $get_message,
                                    'profile_link' => $sysUrl,
                                    'template' => $template
                                );

                                $body = _render($template, $data);
                                $mail_subject = _render($subject, $data);

                                /*Set Authentication*/

                                $default_gt = app_config('Gateway');

                                if ($default_gt == 'default') {

                                    $mail = new \PHPMailer();

                                    $mail->setFrom($sysEmail, $sysCompany);
                                    $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                    $mail->isHTML(true);                                  // Set email format to HTML

                                    $mail->Subject = $mail_subject;
                                    $mail->Body = $body;
                                    $mail->send();

                                } else {
                                    $host = app_config('SMTPHostName');
                                    $smtp_username = app_config('SMTPUserName');
                                    $stmp_password = app_config('SMTPPassword');
                                    $port = app_config('SMTPPort');
                                    $secure = app_config('SMTPSecure');


                                    $mail = new \PHPMailer();

                                    $mail->isSMTP();                                      // Set mailer to use SMTP
                                    $mail->Host = $host;  // Specify main and backup SMTP servers
                                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                    $mail->Username = $smtp_username;                 // SMTP username
                                    $mail->Password = $stmp_password;                           // SMTP password
                                    $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                                    $mail->Port = $port;

                                    $mail->setFrom($sysEmail, $sysCompany);
                                    $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                    $mail->isHTML(true);                                  // Set email format to HTML

                                    $mail->Subject = $mail_subject;
                                    $mail->Body = $body;
                                    $mail->send();


                                }
                            }

                            continue;
                        }
                    }
                }

                ScheduleSMS::create([
                    'userid' => Auth::guard('client')->user()->id,
                    'sender' => $sender_id,
                    'receiver' => $clphone,
                    'amount' => $msgcount,
                    'message' => $get_message,
                    'type' => $msg_type,
                    'submit_time' => $schedule_time,
                    'use_gateway' => $gateway->id,
                    'media_url' => $media_url
                ]);

            }

        } else {
            foreach ($get_data as $msg_data) {

                $get_message = $this->renderSMS($message, $msg_data);

                if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                    $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                    if ($msgcount <= 160) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 157;
                    }
                }
                if ($msg_type == 'unicode') {
                    $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                    if ($msgcount <= 70) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 67;
                    }
                }

                $msgcount = ceil($msgcount);

                $clphone = str_replace(['(', ')', '+', '-', ' '], '', $msg_data[$number_column]);

                if ($request->country_code != 0) {
                    $clphone = $request->country_code . ltrim($clphone, '0');
                }

                if (app_config('fraud_detection') == 1) {
                    if (is_array($spam_word) && count($spam_word) > 0) {
                        $search_word = implode('|', $spam_word);
                        if (preg_match('(' . $search_word . ')', $get_message) === 1) {

                            BlockMessage::create([
                                'user_id' => Auth::guard('client')->user()->id,
                                'sender' => $sender_id,
                                'receiver' => $clphone,
                                'message' => $get_message,
                                'scheduled_time' => null,
                                'use_gateway' => $gateway->id,
                                'status' => 'block',
                                'type' => $msg_type
                            ]);


                            if ($estatus == '1') {

                                $sysEmail = app_config('Email');
                                $sysCompany = app_config('AppName');
                                $sysUrl = url('clients/view/' . Auth::guard('client')->user()->id);

                                $template = $conf->message;
                                $subject = $conf->subject;

                                $data = array(
                                    'user_name' => Auth::guard('client')->user()->username,
                                    'business_name' => $sysCompany,
                                    'message' => $get_message,
                                    'profile_link' => $sysUrl,
                                    'template' => $template
                                );

                                $body = _render($template, $data);
                                $mail_subject = _render($subject, $data);

                                /*Set Authentication*/

                                $default_gt = app_config('Gateway');

                                if ($default_gt == 'default') {

                                    $mail = new \PHPMailer();

                                    $mail->setFrom($sysEmail, $sysCompany);
                                    $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                    $mail->isHTML(true);                                  // Set email format to HTML

                                    $mail->Subject = $mail_subject;
                                    $mail->Body = $body;
                                    $mail->send();

                                } else {
                                    $host = app_config('SMTPHostName');
                                    $smtp_username = app_config('SMTPUserName');
                                    $stmp_password = app_config('SMTPPassword');
                                    $port = app_config('SMTPPort');
                                    $secure = app_config('SMTPSecure');


                                    $mail = new \PHPMailer();

                                    $mail->isSMTP();                                      // Set mailer to use SMTP
                                    $mail->Host = $host;  // Specify main and backup SMTP servers
                                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                    $mail->Username = $smtp_username;                 // SMTP username
                                    $mail->Password = $stmp_password;                           // SMTP password
                                    $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                                    $mail->Port = $port;

                                    $mail->setFrom($sysEmail, $sysCompany);
                                    $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                    $mail->isHTML(true);                                  // Set email format to HTML

                                    $mail->Subject = $mail_subject;
                                    $mail->Body = $body;
                                    $mail->send();


                                }
                            }

                            continue;
                        }
                    }
                }


                array_push($final_insert_data, [
                    'phone_number' => $clphone,
                    'message' => $get_message,
                    'segments' => $msgcount,
                    'media_url' => $media_url
                ]);

            }

            if (count($final_insert_data) <= 0) {
                return redirect($redirect_url)->withInput($request->all())->with([
                    'message' => language_data('Your destination number in blacklist or you were sending fraud sms', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $results = json_encode($final_insert_data, true);


            StoreBulkSMS::create([
                'userid' => Auth::guard('client')->user()->id,
                'sender' => $sender_id,
                'msg_data' => $results,
                'status' => 0,
                'type' => $msg_type,
                'use_gateway' => $gateway->id
            ]);
        }


        $remain_sms = $sms_count - $total_cost;
        $client->sms_limit = $remain_sms;
        $client->save();


        return redirect($redirect_url)->with([
            'message' => language_data('SMS added in queue and will deliver one by one', Auth::guard('client')->user()->lan_id)
        ]);

    }


    //======================================================================
    // sendScheduleSMS Function Start Here
    //======================================================================
    public function sendScheduleSMS()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $phone_book = ImportPhoneNumber::where('user_id', Auth::guard('client')->user()->id)->get();
        $client_group = ClientGroups::where('created_by', Auth::guard('client')->user()->id)->where('status', 'Yes')->get();
        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', 1)->select('country_code', 'country_name')->get();

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        $schedule_sms = true;

        return view('client.send-bulk-sms', compact('client_group', 'sms_templates', 'sender_ids', 'phone_book', 'schedule_sms', 'country_code', 'sms_gateways'));
    }


    //======================================================================
    // sendScheduleSMSFromFile Function Start Here
    //======================================================================
    public function sendScheduleSMSFromFile()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];


            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', 1)->select('country_code', 'country_name')->get();


        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        $schedule_sms = true;

        return view('client.send-sms-file', compact('sms_templates', 'sender_ids', 'schedule_sms', 'country_code', 'sms_gateways'));
    }

    //======================================================================
    // smsHistory Function Start Here
    //======================================================================
    public function smsHistory()
    {
        return view('client.sms-history');
    }


    //======================================================================
    // smsViewInbox Function Start Here
    //======================================================================
    public function smsViewInbox($id)
    {

        $inbox_info = SMSHistory::where('userid', Auth::guard('client')->user()->id)->find($id);

        if ($inbox_info) {
            return view('client.sms-inbox', compact('inbox_info'));
        } else {
            return redirect('user/sms/history')->with([
                'message' => language_data('SMS Not Found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // deleteSMS Function Start Here
    //======================================================================
    public function deleteSMS($id)
    {

        $inbox_info = SMSHistory::where('userid', Auth::guard('client')->user()->id)->find($id);

        if ($inbox_info) {
            $inbox_info->delete();

            return redirect('user/sms/history')->with([
                'message' => language_data('SMS info deleted successfully', Auth::guard('client')->user()->lan_id)
            ]);
        } else {
            return redirect('user/sms/history')->with([
                'message' => language_data('SMS Not Found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }



    //======================================================================
    // apiInfo Function Start Here
    //======================================================================
    public function apiInfo()
    {

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        return view('client.sms-api-info', compact('sms_gateways'));
    }

    //======================================================================
    // updateApiInfo Function Start Here
    //======================================================================
    public function updateApiInfo(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'api_key' => 'required', 'sms_gateway' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms-api/info')->withErrors($v->errors());
        }

        if ($request->api_key != '') {
            Client::where('id', Auth::guard('client')->user()->id)->where('api_access', 'Yes')->update([
                'api_key' => $request->api_key,
                'api_gateway' => $request->sms_gateway
            ]);
        }


        return redirect('user/sms-api/info')->with([
            'message' => language_data('API information updated successfully', Auth::guard('client')->user()->lan_id)
        ]);

    }


    /*Version 1.1*/


    //======================================================================
    // updateScheduleSMS Function Start Here
    //======================================================================
    public function updateScheduleSMS()
    {
        $sms_history = ScheduleSMS::where('userid', Auth::guard('client')->user()->id)->get();
        return view('client.update-schedule-sms', compact('sms_history'));
    }



    //======================================================================
    // manageUpdateScheduleSMS Function Start Here
    //======================================================================
    public function manageUpdateScheduleSMS($id)
    {
        $sh = ScheduleSMS::find($id);

        if ($sh) {

            if (app_config('sender_id_verification') == '1') {
                $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
                $all_ids = [];


                foreach ($all_sender_id as $sid) {
                    $client_array = json_decode($sid->cl_id);

                    if (is_array($client_array) && in_array('0', $client_array)) {
                        array_push($all_ids, $sid->sender_id);
                    } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                        array_push($all_ids, $sid->sender_id);
                    }
                }
                $sender_ids = array_unique($all_ids);

            } else {
                $sender_ids = false;
            }

            $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
            $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->where('schedule', 'Yes')->get();

            return view('client.manage-update-schedule-sms', compact('sh', 'sender_ids', 'sms_gateways'));
        } else {
            return redirect('user/sms/update-schedule-sms')->with([
                'message' => language_data('Please try again', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // postUpdateScheduleSMS Function Start Here
    //======================================================================
    public function postUpdateScheduleSMS(Request $request)
    {

        $cmd = $request->cmd;

        $v = \Validator::make($request->all(), [
            'phone_number' => 'required', 'schedule_time' => 'required', 'message_type' => 'required', 'sms_gateway' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->withErrors($v->errors());
        }


        $sms_info = ScheduleSMS::where('userid', Auth::guard('client')->user()->id)->find($cmd);

        if (!$sms_info) {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->with([
                'message' => language_data('SMS info not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        if (\DateTime::createFromFormat('m/d/Y h:i A', $request->schedule_time) !== FALSE) {
            $schedule_time = date('Y-m-d H:i:s', strtotime($request->schedule_time));
        } else {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->with([
                'message' => language_data('Invalid time format', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $blacklist = BlackListContact::select('numbers')->get()->toArray();

        if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {
            $blacklist = array_column($blacklist, 'numbers');
        }

        if (in_array($request->phone_number, $blacklist)) {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                'message' => language_data('Phone number contain in blacklist', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $client = Client::find(Auth::guard('client')->user()->id);

        if ($client == '') {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                'message' => language_data('Client info not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $gateway = SMSGateways::find($request->sms_gateway);
        if ($gateway->status != 'Active') {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $gateway_credential = null;
        if ($gateway->custom == 'Yes') {
            if ($gateway->type == 'smpp') {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }
        } else {
            $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
            if ($gateway_credential == null) {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $message = $request->message;
        $msg_type = $request->message_type;


        if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($msg_type == 'voice') {
            if ($gateway->voice != 'Yes') {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type == 'mms') {

            if ($gateway->mms != 'Yes') {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $image = $request->image;

            if ($image != '') {
                if (app_config('AppStage') != 'Demo') {
                    if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                        $destinationPath = public_path() . '/assets/mms_file/';
                        $image_name = $image->getClientOriginalName();
                        Input::file('image')->move($destinationPath, $image_name);
                        $media_url = asset('assets/mms_file/' . $image_name);

                    } else {
                        return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                            'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                } else {
                    return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                        'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            } else {
                $media_url = $sms_info->media_url;
            }

        } else {
            $media_url = null;
            if ($message == '') {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
            $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
            if ($msgcount <= 160) {
                $msgcount = 1;
            } else {
                $msgcount = $msgcount / 157;
            }
        }
        if ($msg_type == 'unicode') {
            $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

            if ($msgcount <= 70) {
                $msgcount = 1;
            } else {
                $msgcount = $msgcount / 67;
            }
        }

        $msgcount = ceil($msgcount);

        $sender_id = $request->sender_id;

        if (app_config('sender_id_verification') == '1') {
            if ($sender_id == null) {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($gateway->name == 'FortDigital') {
            $c_phone = 61;
        } elseif ($gateway->name == 'Ibrbd') {
            $c_phone = 880;
        } else {
            $phone = str_replace(['(', ')', '+', '-', ' '], '', $request->phone_number);
            $c_phone = PhoneNumber::get_code($phone);
        }

        $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();

        if ($sms_cost) {


            $phoneUtil = PhoneNumberUtil::getInstance();
            $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
            $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

            if ($area_code_exist) {
                $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                $get_format_data = explode(" ", $format);
                $operator_settings = explode('-', $get_format_data[1])[0];

            } else {
                $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
            }

            $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();

            if ($get_operator) {
                $total_cost = ($get_operator->price * $msgcount);
            } else {
                $total_cost = ($sms_cost->tariff * $msgcount);
            }
            if ($total_cost == 0) {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $total_cost -= $sms_info->amount;

            if ($total_cost > $client->sms_limit) {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        if ($sender_id != '' && app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::all();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $all_ids = array_unique($all_ids);

            if (!in_array($sender_id, $all_ids)) {
                return redirect('user/sms/manage-update-schedule-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if (app_config('fraud_detection') == 1) {
            $spam_word = SpamWord::all()->toArray();
            if (is_array($spam_word) && count($spam_word) > 0) {
                $spam_word = array_column($spam_word, 'word');
                $admin = Admin::where('username', 'admin')->first();
                $admin_email = $admin->email;
                $admin_name = $admin->fname . ' ' . $admin->lname;

                $conf = EmailTemplates::where('tplname', '=', 'Spam Word Notification')->first();
                $estatus = $conf->status;

                $search_word = implode('|', $spam_word);


                if (preg_match('(' . $search_word . ')', $message) === 1) {

                    BlockMessage::create([
                        'user_id' => Auth::guard('client')->user()->id,
                        'sender' => $sender_id,
                        'receiver' => $request->phone_number,
                        'message' => $message,
                        'scheduled_time' => $schedule_time,
                        'use_gateway' => $gateway->id,
                        'status' => 'block',
                        'type' => $msg_type
                    ]);


                    if ($estatus == '1') {

                        $sysEmail = app_config('Email');
                        $sysCompany = app_config('AppName');
                        $sysUrl = url('clients/view/' . Auth::guard('client')->user()->id);

                        $template = $conf->message;
                        $subject = $conf->subject;

                        $data = array(
                            'user_name' => Auth::guard('client')->user()->username,
                            'business_name' => $sysCompany,
                            'message' => $message,
                            'profile_link' => $sysUrl,
                            'template' => $template
                        );

                        $body = _render($template, $data);
                        $mail_subject = _render($subject, $data);

                        /*Set Authentication*/

                        $default_gt = app_config('Gateway');

                        if ($default_gt == 'default') {

                            $mail = new \PHPMailer();

                            $mail->setFrom($sysEmail, $sysCompany);
                            $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                            $mail->isHTML(true);                                  // Set email format to HTML

                            $mail->Subject = $mail_subject;
                            $mail->Body = $body;
                            $mail->send();

                        } else {
                            $host = app_config('SMTPHostName');
                            $smtp_username = app_config('SMTPUserName');
                            $stmp_password = app_config('SMTPPassword');
                            $port = app_config('SMTPPort');
                            $secure = app_config('SMTPSecure');


                            $mail = new \PHPMailer();

                            $mail->isSMTP();                                      // Set mailer to use SMTP
                            $mail->Host = $host;  // Specify main and backup SMTP servers
                            $mail->SMTPAuth = true;                               // Enable SMTP authentication
                            $mail->Username = $smtp_username;                 // SMTP username
                            $mail->Password = $stmp_password;                           // SMTP password
                            $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                            $mail->Port = $port;

                            $mail->setFrom($sysEmail, $sysCompany);
                            $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                            $mail->isHTML(true);                                  // Set email format to HTML

                            $mail->Subject = $mail_subject;
                            $mail->Body = $body;
                            $mail->send();


                        }
                    }

                    return redirect('user/sms/update-schedule-sms')->with([
                        'message' => language_data('SMS are scheduled. Deliver in correct time', Auth::guard('client')->user()->lan_id)
                    ]);

                }

            }
        }


        ScheduleSMS::where('id', $request->cmd)->update([
            'sender' => $sender_id,
            'receiver' => $request->phone_number,
            'amount' => $msgcount,
            'message' => $message,
            'type' => $msg_type,
            'submit_time' => $schedule_time,
            'media_url' => $media_url
        ]);

        $remain_sms = $client->sms_limit - $total_cost;
        $client->sms_limit = $remain_sms;
        $client->save();

        return redirect('user/sms/update-schedule-sms')->with([
            'message' => language_data('SMS are scheduled. Deliver in correct time', Auth::guard('client')->user()->lan_id)
        ]);

    }

    //======================================================================
    // deleteScheduleSMS Function Start Here
    //======================================================================
    public function deleteScheduleSMS($id)
    {

        $sh = ScheduleSMS::find($id);
        if ($sh) {
            $client = Client::find($sh->userid);
            $client->sms_limit += $sh->amount;
            $client->save();

            $sh->delete();
            return redirect('user/sms/update-schedule-sms')->with([
                'message' => language_data('SMS info deleted successfully', Auth::guard('client')->user()->lan_id)
            ]);
        } else {
            return redirect('user/sms/update-schedule-sms')->with([
                'message' => language_data('Please try again', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    /*Version 1.2*/

    //======================================================================
    // buyUnit Function Start Here
    //======================================================================
    public function buyUnit()
    {
        $bundles = SMSBundles::orderBy('unit_from')->get();
        $payment_gateways = PaymentGateways::where('status', 'Active')->get();
        return view('client.buy-unit', compact('bundles', 'payment_gateways'));
    }

    //======================================================================
    // getTransaction Function Start Here
    //======================================================================
    public function getTransaction(Request $request)
    {


        if ($request->unit_number != '') {
            $data = DB::select("select * from `sys_sms_bundles` where (`unit_from` <= $request->unit_number and `unit_to` >= $request->unit_number) LIMIT 1");
            $data_count = count($data);

            if ($data_count != 0) {
                $unit_price = $data[0]->price;
                $amount_to_pay = $request->unit_number * $unit_price;
                $transaction_fee = ($amount_to_pay * $data[0]->trans_fee) / 100;
                $total = $amount_to_pay + $transaction_fee;
            } else {
                $unit_price = 'Price Bundle empty';
                $amount_to_pay = 'Price Bundle empty';
                $transaction_fee = 'Price Bundle empty';
                $total = 'Price Bundle empty';
            }
        } else {
            $unit_price = 'Price Bundle empty';
            $amount_to_pay = 'Price Bundle empty';
            $transaction_fee = 'Price Bundle empty';
            $total = 'Price Bundle empty';
        }


        return response()->json([
            'unit_price' => $unit_price,
            'amount_to_pay' => $amount_to_pay,
            'transaction_fee' => $transaction_fee,
            'total' => $total
        ]);


    }

    //======================================================================
    // postGetTemplateInfo Function Start Here
    //======================================================================
    public function postGetTemplateInfo(Request $request)
    {
        $template = SMSTemplates::find($request->st_id);
        if ($template) {
            return response()->json([
                'from' => $template->from,
                'message' => $template->message,
            ]);
        }
    }

    //======================================================================
    // renderSMS Start Here
    //======================================================================
    public function renderSMS($msg, $data)
    {
        preg_match_all('~<%(.*?)%>~s', $msg, $datas);
        $Html = $msg;
        foreach ($datas[1] as $value) {
            if (array_key_exists($value, $data)) {
                $Html = str_replace($value, $data[$value], $Html);
            } else {
                $Html = str_replace($value, '', $Html);
            }
        }
        return str_replace(array("<%", "%>"), '', $Html);
    }


    //======================================================================
    // smsTemplates Function Start Here
    //======================================================================
    public function smsTemplates()
    {
        $sms_templates = SMSTemplates::where('cl_id', Auth::guard('client')->user()->id)->orWhere('global', 'yes')->get();
        return view('client.sms-templates', compact('sms_templates'));
    }

    //======================================================================
    // createSmsTemplate Function Start Here
    //======================================================================
    public function createSmsTemplate()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];


            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        return view('client.create-sms-template', compact('sender_ids'));
    }

    //======================================================================
    // postSmsTemplate Function Start Here
    //======================================================================
    public function postSmsTemplate(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'template_name' => 'required', 'message' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/create-sms-template')->withErrors($v->errors());
        }
        $exist = SMSTemplates::where('template_name', $request->template_name)->where('cl_id', Auth::guard('client')->user()->id)->first();

        if ($exist) {
            return redirect('user/sms/create-sms-template')->with([
                'message' => language_data('Template already exist', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $st = new SMSTemplates();
        $st->cl_id = Auth::guard('client')->user()->id;
        $st->template_name = $request->template_name;
        $st->from = $request->from;
        $st->message = $request->message;
        $st->global = 'no';
        $st->status = 'active';
        $st->save();

        return redirect('user/sms/sms-templates')->with([
            'message' => language_data('Sms template created successfully', Auth::guard('client')->user()->lan_id)
        ]);

    }

    //======================================================================
    // manageSmsTemplate Function Start Here
    //======================================================================
    public function manageSmsTemplate($id)
    {

        $st = SMSTemplates::find($id);
        if ($st) {

            if (app_config('sender_id_verification') == '1') {
                $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
                $all_ids = [];

                foreach ($all_sender_id as $sid) {
                    $client_array = json_decode($sid->cl_id);

                    if (in_array('0', $client_array)) {
                        array_push($all_ids, $sid->sender_id);
                    } elseif (in_array(Auth::guard('client')->user()->id, $client_array)) {
                        array_push($all_ids, $sid->sender_id);
                    }
                }
                $sender_ids = array_unique($all_ids);

            } else {
                $sender_ids = false;
            }

            return view('client.manage-sms-template', compact('st', 'sender_ids'));
        } else {
            return redirect('user/sms/sms-templates')->with([
                'message' => language_data('Sms template not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }

    //======================================================================
    // postManageSmsTemplate Function Start Here
    //======================================================================
    public function postManageSmsTemplate(Request $request)
    {
        $cmd = Input::get('cmd');
        $v = \Validator::make($request->all(), [
            'template_name' => 'required', 'message' => 'required', 'status' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/manage-sms-template/' . $cmd)->withErrors($v->errors());
        }

        $st = SMSTemplates::find($cmd);

        if ($st) {
            if ($st->template_name != $request->template_name) {

                $exist = SMSTemplates::where('template_name', $request->template_name)->where('cl_id', Auth::guard('client')->user()->id)->first();

                if ($exist) {
                    return redirect('user/sms/manage-sms-template/' . $cmd)->with([
                        'message' => language_data('Template already exist', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }

            $st->template_name = $request->template_name;
            $st->from = $request->from;
            $st->message = $request->message;
            $st->status = $request->status;
            $st->save();

            return redirect('user/sms/sms-templates')->with([
                'message' => language_data('Sms template updated successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/sms-templates')->with([
                'message' => language_data('Sms template not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }

    //======================================================================
    // deleteSmsTemplate Function Start Here
    //======================================================================
    public function deleteSmsTemplate($id)
    {
        $st = SMSTemplates::find($id);
        if ($st) {
            $st->delete();

            return redirect('user/sms/sms-templates')->with([
                'message' => language_data('Sms template delete successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/sms-templates')->with([
                'message' => language_data('Sms template not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }



    /*Version 2.0*/

    //======================================================================
    // blacklistContacts Function Start Here
    //======================================================================
    public function blacklistContacts()
    {
        $country_code = IntCountryCodes::where('Active', '1')->select('country_code', 'country_name')->get();
        $phone_book = ImportPhoneNumber::where('user_id', Auth::guard('client')->user()->id)->get();
        $clientGroups = ImportPhoneNumber::where('user_id', Auth::guard('client')->user()->id)->get();

        return view('client.old-blacklist-contacts',compact('country_code','phone_book','clientGroups'));
    }

    //======================================================================
    // postBlacklistContact Function Start Here
    //======================================================================
    public function postBlacklistContact(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'phone_number' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/blacklist-contacts')->withErrors($v->errors());
        }


        $number = str_replace(['(', ')', '+', '-', ' '], '', $request->phone_number);

        $exist = BlackListContact::where('numbers', $number)->where('user_id', Auth::guard('client')->user()->id)->first();

        if ($exist) {
            return redirect('user/sms/blacklist-contacts')->with([
                'message' => language_data('Contact number already exist', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        BlackListContact::create([
            'user_id' => Auth::guard('client')->user()->id,
            'numbers' => $number
        ]);

        return redirect('user/sms/blacklist-contacts')->with([
            'message' => language_data('Number added on blacklist', Auth::guard('client')->user()->lan_id),
        ]);

    }

    //======================================================================
    // deleteBlacklistContact Function Start Here
    //======================================================================
    public function deleteBlacklistContact($id)
    {
        $blacklist = BlackListContact::where('user_id', Auth::guard('client')->user()->id)->find($id);
        if ($blacklist) {
            $blacklist->delete();
            return redirect('user/sms/blacklist-contacts')->with([
                'message' => language_data('Number deleted from blacklist', Auth::guard('client')->user()->lan_id),
            ]);
        } else {
            return redirect('user/sms/blacklist-contacts')->with([
                'message' => language_data('Number not found on blacklist', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // getBlacklistContacts Function Start Here
    //======================================================================
    public function getBlacklistContacts()
    {
        $blacklist = BlackListContact::select(['id', 'numbers'])->where('user_id', Auth::guard('client')->user()->id)->get();
        return Datatables::of($blacklist)
            ->addColumn('action', function ($bl) {
                return '
            <a href="#" class="btn btn-danger btn-xs cdelete" id="' . $bl->id . '"><i class="fa fa-trash"></i> ' . language_data("Delete") . '</a>';
            })
            ->escapeColumns([])
            ->make(true);
    }


    //======================================================================
    // getSmsHistoryData Function Start Here
    //======================================================================
    public function getSmsHistoryData(Request $request)
    {
        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];

            if ($get_search_column == 'date') {
                $get_search_column = 'updated_at';
            }

        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }

        $sms_history = SMSHistory::select(['id', 'sender', 'receiver', 'amount', 'status', 'send_by', 'updated_at', 'api_key'])->where('userid', Auth::guard('client')->user()->id)->orderBy($get_search_column, $short_by);

        return Datatables::of($sms_history)
            ->addColumn('action', function ($sms) {
                $reply_url = '';
                if ($sms->send_by == 'receiver') {
                    $reply_url .= ' <a class="btn btn-complete btn-xs reply_message" href="#" id="' . $sms->id . '"><i class="fa fa-reply"></i> ' . language_data('Reply', Auth::guard('client')->user()->lan_id) . ' </a>';
                }
                return $reply_url . '
                <a class="btn btn-success btn-xs" href="' . url("user/sms/view-inbox/$sms->id") . '" ><i class="fa fa-inbox"></i> ' . language_data('Inbox', Auth::guard('client')->user()->lan_id) . '</a>
                <a href="#" id="' . $sms->id . '" class="cdelete btn btn-xs btn-danger"><i class="fa fa-danger"></i> ' . language_data('Delete', Auth::guard('client')->user()->lan_id) . '</a>
                ';
            })
            ->addColumn('date', function ($sms) {
                return $sms->updated_at;
            })
            ->addColumn('id', function ($sms) {
                return "<div class='coder-checkbox'>
                             <input type='checkbox'  class='deleteRow' value='$sms->id'  />
                                            <span class='co-check-ui'></span>
                                        </div>";

            })
            ->filter(function ($query) use ($request) {

                if ($request->has('send_by') && $request->get('send_by') != '0') {
                    $query->where('send_by', $request->get('send_by'));
                }

                if ($request->has('sender')) {
                    $query->where('sender', 'like', "%{$request->get('sender')}%");
                }

                if ($request->has('receiver')) {
                    $query->where('receiver', 'like', "%{$request->get('receiver')}%");
                }

                if ($request->has('status')) {
                    $query->where('status', 'like', "%{$request->get('status')}%");
                }

                if ($request->has('date_from') && $request->has('date_to')) {
                    $date_from = date('Y-m-d H:i:s', strtotime($request->get('date_from')));
                    $date_to = date('Y-m-d H:i:s', strtotime($request->get('date_to')));
                    $query->whereBetween('updated_at', [$date_from, $date_to]);
                }
            })
            ->addColumn('send_by', function ($sms) {
                if ($sms->send_by == 'sender') {
                    return language_data('Outgoing', Auth::guard('client')->user()->lan_id);
                } elseif ($sms->send_by == 'api') {
                    return '<span class="text-info">' . language_data('API SMS', Auth::guard('client')->user()->lan_id) . ' </span>';
                } else {
                    return '<span class="text-success"> ' . language_data('Incoming', Auth::guard('client')->user()->lan_id) . ' </span>';
                }
            })
            ->escapeColumns([])
            ->make(true);
    }

    //======================================================================
    // getRecipientsData Function Start Here
    //======================================================================
    public function getRecipientsData(Request $request)
    {
        if ($request->has('client_group_ids')) {
            $client_group_ids = $request->client_group_ids;
            if (is_array($client_group_ids) && count($client_group_ids) > 0) {
                $count = Client::whereIn('groupid', $client_group_ids)->count();
                return response()->json(['status' => 'success', 'data' => $count]);
            } else {
                return response()->json(['status' => 'success', 'data' => 0]);
            }
        } elseif ($request->has('contact_list_ids')) {
            $contact_list_ids = $request->contact_list_ids;
            if (is_array($contact_list_ids) && count($contact_list_ids) > 0) {
                $count = ContactList::whereIn('pid', $contact_list_ids)->count();
                return response()->json(['status' => 'success', 'data' => $count]);
            } else {
                return response()->json(['status' => 'success', 'data' => 0]);
            }
        } else {
            return response()->json(['status' => 'success', 'data' => 0]);
        }
    }

    //======================================================================
    // deleteBulkSMS Function Start Here
    //======================================================================
    public function deleteBulkSMS(Request $request)
    {
        if ($request->has('data_ids')) {
            $all_ids = explode(',', $request->get('data_ids'));

            if (is_array($all_ids) && count($all_ids) > 0) {
                SMSHistory::where('userid', Auth::guard('client')->user()->id)->whereIn('id', $all_ids)->delete();
            }
        }
    }


    //======================================================================
    // sdkInfo Function Start Here
    //======================================================================
    public function sdkInfo()
    {
        return view('client.sms-sdk-info');
    }


    //======================================================================
    // sendQuickSMS Function Start Here
    //======================================================================
    public function sendQuickSMS()
    {
        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();
        $country_code = IntCountryCodes::where('Active', 1)->select('country_code', 'country_name')->get();

        return view('client.send-quick-sms', compact('sender_ids', 'country_code', 'sms_gateways'));
    }




    //======================================================================
    // postQuickSMS Function Start Here
    //======================================================================
    public function postQuickSMS(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'recipients' => 'required', 'message' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required', 'sms_gateway' => 'required',
        ]);

        if ($v->fails()) {
            return redirect('user/sms/quick-sms')->withInput($request->all())->withErrors($v->errors());
        }


        $message = $request->message;

        if (app_config('fraud_detection') == 1) {
            $spam_word = SpamWord::all()->toArray();
            if (is_array($spam_word) && count($spam_word) > 0) {
                $spam_word = array_column($spam_word, 'word');
                $search_word = implode('|', $spam_word);

                if (preg_match('(' . $search_word . ')', $message) === 1) {
                    return redirect('user/sms/quick-sms')->with([
                        'message' => language_data('Your are sending fraud message', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);

                }
            }
        }

        $client = Client::find(Auth::guard('client')->user()->id);
        $sms_count = $client->sms_limit;
        $sender_id = $request->sender_id;

        if (app_config('sender_id_verification') == '1') {
            if ($sender_id == null) {
                return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($sender_id != '' && app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::all();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $all_ids = array_unique($all_ids);

            if (!in_array($sender_id, $all_ids)) {
                return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        try {

            $recipients = multi_explode(array(",", "\n", ";"), $request->recipients);
            $results = array_filter($recipients);

            if (is_array($results) && count($results) <= 100) {

                $gateway = SMSGateways::find($request->sms_gateway);
                if ($gateway->status != 'Active') {
                    return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                        'message' => language_data('SMS gateway not active', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                $gateway_credential = null;
                $cg_info = null;
                if ($gateway->custom == 'Yes') {
                    if ($gateway->type == 'smpp') {
                        $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
                        if ($gateway_credential == null) {
                            return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                                'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                                'message_important' => true
                            ]);
                        }
                    } else {
                        $cg_info = CustomSMSGateways::where('gateway_id', $request->sms_gateway)->first();
                    }

                } else {
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $request->sms_gateway)->where('status', 'Active')->first();
                    if ($gateway_credential == null) {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }

                $msg_type = $request->message_type;

                if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
                    return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                        'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($msg_type == 'voice') {
                    if ($gateway->voice != 'Yes') {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }

                if ($msg_type == 'mms') {

                    if ($gateway->mms != 'Yes') {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    $image = $request->image;

                    if ($image == '') {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('MMS file required', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    if (app_config('AppStage') != 'Demo') {
                        if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                            $destinationPath = public_path() . '/assets/mms_file/';
                            $image_name = $image->getClientOriginalName();
                            Input::file('image')->move($destinationPath, $image_name);
                            $media_url = asset('assets/mms_file/' . $image_name);

                        } else {
                            return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                                'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                                'message_important' => true
                            ]);
                        }

                    } else {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                } else {
                    $media_url = null;
                    if ($message == '') {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }


                if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                    $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                    if ($msgcount <= 160) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 157;
                    }
                }
                if ($msg_type == 'unicode') {
                    $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                    if ($msgcount <= 70) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 67;
                    }
                }

                $msgcount = ceil($msgcount);


                $get_cost = 0;
                $get_inactive_coverage = [];
                $total_cost = 0;

                $filtered_data = [];
                $blacklist = BlackListContact::select('numbers')->get()->toArray();

                if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {

                    $blacklist = array_column($blacklist, 'numbers');

                    array_filter($results, function ($element) use ($blacklist, &$filtered_data) {
                        if (!in_array(trim($element), $blacklist)) {
                            array_push($filtered_data, $element);
                        }
                    });

                    $results = array_values($filtered_data);
                }


                if (count($results) <= 0) {
                    return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                        'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($request->remove_duplicate == 'yes') {
                    $results = array_unique($results, SORT_REGULAR);
                }

                $results = array_values($results);

                foreach ($results as $r) {

                    $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($r));

                    if ($request->country_code == 0) {
                        if ($gateway->settings == 'FortDigital') {
                            $c_phone = 61;
                        } elseif ($gateway->settings == 'Ibrbd') {
                            $c_phone = 880;
                        } else {
                            $c_phone = PhoneNumber::get_code($phone);
                        }
                    } else {
                        $phone = $request->country_code . ltrim($phone, '0');
                        $c_phone = $request->country_code;
                    }

                    $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
                    if ($sms_cost) {

                        $phoneUtil = PhoneNumberUtil::getInstance();
                        $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                        $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                        if ($area_code_exist) {
                            $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                            $get_format_data = explode(" ", $format);
                            $operator_settings = explode('-', $get_format_data[1])[0];

                        } else {
                            $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                            $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                        }

                        $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                        if ($get_operator) {
                            $sms_charge = $get_operator->price;
                            $get_cost += $sms_charge;
                        } else {
                            $sms_charge = $sms_cost->tariff;
                            $get_cost += $sms_charge;
                        }
                    } else {
                        array_push($get_inactive_coverage, 'found');
                    }


                    if (in_array('found', $get_inactive_coverage)) {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    $total_cost = $get_cost * $msgcount;

                    if ($total_cost == 0) {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    if ($total_cost > $sms_count) {
                        return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                            'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }

                $remain_sms = $sms_count - $total_cost;
                $client->sms_limit = $remain_sms;
                $client->save();

                foreach ($results as $r) {
                    $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($r));
                    if ($request->country_code != 0) {
                        $phone = $request->country_code . ltrim($phone, '0');
                    }

                    if ($msg_type == 'plain' || $msg_type == 'unicode') {
                        $this->dispatch(new SendBulkSMS($client->id, $phone, $gateway, $gateway_credential, $sender_id, $message, $msgcount, $cg_info, '', $msg_type));
                    }
                    if ($msg_type == 'voice') {
                        $this->dispatch(new SendBulkVoice($client->id, $phone, $gateway, $gateway_credential, $sender_id, $message, $msgcount));
                    }
                    if ($msg_type == 'mms') {
                        $this->dispatch(new SendBulkMMS($client->id, $phone, $gateway, $gateway_credential, $sender_id, $message, $media_url));
                    }
                }


                return redirect('user/sms/quick-sms')->with([
                    'message' => language_data('Please check sms history for status', Auth::guard('client')->user()->lan_id)
                ]);
            } else {
                return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                    'message' => language_data('You can not send more than 100 sms using quick sms option', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

        } catch (\Exception $e) {
            return redirect('user/sms/quick-sms')->withInput($request->all())->with([
                'message' => $e->getMessage(),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // postReplySMS Function Start Here
    //======================================================================
    public function postReplySMS($cmd, $message)
    {
        if ($message == '') {
            return redirect('user/sms/history')->with([
                'message' => language_data('Insert your message', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $h = SMSHistory::find($cmd);
        if ($h) {

            $client = Client::find($h->userid);
            $gateway = SMSGateways::find($h->use_gateway);

            if ($gateway->status != 'Active') {
                return redirect('user/sms/history')->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $gateway_credential = null;
            $cg_info = null;
            if ($gateway->custom == 'Yes') {
                if ($gateway->type == 'smpp') {
                    $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                    if ($gateway_credential == null) {
                        return redirect('user/sms/history')->with([
                            'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                } else {
                    $cg_info = CustomSMSGateways::where('gateway_id', $gateway->id)->first();
                }
            } else {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('user/sms/history')->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }

            $sender_id = $h->receiver;
            $msg_type = $h->sms_type;
            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                if ($msgcount <= 160) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 157;
                }
            }
            if ($msg_type == 'unicode') {
                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                if ($msgcount <= 70) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 67;
                }
            }

            $msgcount = ceil($msgcount);

            $phone = $h->sender;
            if ($gateway->name == 'FortDigital') {
                $c_phone = 61;
            } elseif ($gateway->name == 'Ibrbd') {
                $c_phone = 880;
            } else {
                $c_phone = PhoneNumber::get_code($phone);
            }

            $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();

            if ($sms_cost) {
                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                if ($area_code_exist) {
                    $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                    $get_format_data = explode(" ", $format);
                    $operator_settings = explode('-', $get_format_data[1])[0];

                } else {
                    $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                    $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                }

                $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();

                if ($get_operator) {
                    $total_cost = ($get_operator->price * $msgcount);
                } else {
                    $total_cost = ($sms_cost->tariff * $msgcount);
                }
                if ($total_cost == 0) {
                    return redirect('user/sms/history')->with([
                        'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($total_cost > $client->sms_limit) {
                    return redirect('user/sms/history')->with([
                        'message' => language_data('You do not have enough sms balance', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            } else {
                return redirect('user/sms/history')->with([
                    'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if ($sender_id != '' && app_config('sender_id_verification') == '1') {
                $all_sender_id = SenderIdManage::all();
                $all_ids = [];

                foreach ($all_sender_id as $sid) {
                    $client_array = json_decode($sid->cl_id);

                    if (is_array($client_array) && in_array('0', $client_array)) {
                        array_push($all_ids, $sender_id);
                    } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                        array_push($all_ids, $sid->sender_id);
                    }
                }
                $all_ids = array_unique($all_ids);

                if (!in_array($sender_id, $all_ids)) {
                    return redirect('user/sms/history')->with([
                        'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }

            if (app_config('fraud_detection') == 1) {
                $spam_word = SpamWord::all()->toArray();
                if (is_array($spam_word) && count($spam_word) > 0) {
                    $spam_word = array_column($spam_word, 'word');
                    $search_word = implode('|', $spam_word);
                    if (preg_match('(' . $search_word . ')', $message) === 1) {
                        return redirect('user/sms/history')->with([
                            'message' => language_data('Message contain spam word', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                }
            }

            if ($msg_type == 'plain' || $msg_type == 'unicode') {
                $this->dispatch(new SendBulkSMS($h->userid, $h->sender, $gateway, $gateway_credential, $sender_id, $message, $msgcount, $cg_info, '', $msg_type));
            }

            if ($msg_type == 'voice') {
                $this->dispatch(new SendBulkVoice($h->userid, $h->sender, $gateway, $gateway_credential, $sender_id, $message, $msgcount));
            }

            if ($msg_type == 'mms') {
                return redirect('user/sms/history')->with([
                    'message' => language_data('MMS not supported in two way communication', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $remain_sms = $client->sms_limit - $total_cost;
            $client->sms_limit = $remain_sms;
            $client->save();

            return redirect('user/sms/history')->with([
                'message' => language_data('Successfully sent reply', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/history')->with([
                'message' => language_data('SMS Not Found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }


    /*
    |--------------------------------------------------------------------------
    | Recurring SMS
    |--------------------------------------------------------------------------
    |
    | All work on Recurring sms
    |
    */

    //======================================================================
    // recurringSMS Function Start Here
    //======================================================================
    public function recurringSMS()
    {
        return view('client.recurring-sms');
    }


    //======================================================================
    // getRecurringSMSData Function Start Here
    //======================================================================
    public function getRecurringSMSData(Request $request)
    {


        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];
        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }

        $recurring_sms = RecurringSMS::select(['id', 'sender', 'status', 'total_recipients', 'recurring_date', 'recurring', 'updated_at'])->where('userid', Auth::guard('client')->user()->id)->orderBy($get_search_column, $short_by);
        return Datatables::of($recurring_sms)
            ->addColumn('action', function ($sms) {
                $reply_url = '';
                if ($sms->status == 'running') {
                    $reply_url .= ' <a class="btn btn-warning btn-xs stop-recurring" href="#" id="' . $sms->id . '"><i class="fa fa-stop"></i> ' . language_data('Stop Recurring', Auth::guard('client')->user()->lan_id) . '  </a>';
                } else {
                    $reply_url .= ' <a class="btn btn-success btn-xs start-recurring" href="#" id="' . $sms->id . '"><i class="fa fa-check"></i> ' . language_data('Start Recurring', Auth::guard('client')->user()->lan_id) . ' </a>';

                }
                return $reply_url . '
                <a href="#" id="' . $sms->id . '" class="cdelete btn btn-xs btn-danger"><i class="fa fa-trash"></i> ' . language_data('Delete') . '</a>
                <div class="btn-group btn-mini-group dropdown-default">
                    <a class="btn btn-xs dropdown-toggle btn-complete" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-bars"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="' . url("user/sms/update-recurring-sms/$sms->id") . '" data-toggle="tooltip" data-placement="left" title="' . language_data('Update Period', Auth::guard('client')->user()->lan_id) . '"><i class="fa fa-clock-o"></i></a></li>
                        <li><a href="' . url("user/sms/add-recurring-sms-contact/$sms->id") . '" data-toggle="tooltip" data-placement="left" title="' . language_data('Add Contact', Auth::guard('client')->user()->lan_id) . '"><i class="fa fa-plus"></i></a></li>
                        <li><a href="' . url("user/sms/update-recurring-sms-contact/$sms->id") . '" data-toggle="tooltip" data-placement="left" title="' . language_data('Update Contact', Auth::guard('client')->user()->lan_id) . '"><i class="fa fa-edit"></i></a></li>
                    </ul>
                </div>
                ';
            })
            ->addColumn('recurring_date', function ($sms) {
                return $sms->recurring_date;
            })
            ->addColumn('sender', function ($sms) {
                return $sms->sender;
            })
            ->addColumn('total_recipients', function ($sms) {
                return $sms->total_recipients;
            })
            ->addColumn('id', function ($sms) {
                return "<div class='coder-checkbox'>
                             <input type='checkbox'  class='deleteRow' value='$sms->id'  />
                                            <span class='co-check-ui'></span>
                                        </div>";

            })
            ->addColumn('status', function ($sms) {
                if ($sms->status == 'running') {
                    return '<span class="text-success"> ' . language_data('Running') . ' </span>';
                } else {
                    return '<span class="text-danger"> ' . language_data('Stop') . ' </span>';
                }
            })
            ->addColumn('recurring', function ($sms) {
                if ($sms->recurring == '0') {
                    $period = language_data('Custom date');
                } elseif ($sms->recurring == 'day') {
                    $period = language_data('Daily');
                } elseif ($sms->recurring == 'week1') {
                    $period = language_data('Weekly');
                } elseif ($sms->recurring == 'weeks2') {
                    $period = language_data('2 Weeks');
                } elseif ($sms->recurring == 'month1') {
                    $period = language_data('Monthly');
                } elseif ($sms->recurring == 'months2') {
                    $period = language_data('2 Months');
                } elseif ($sms->recurring == 'months3') {
                    $period = language_data('3 Months');
                } elseif ($sms->recurring == 'months6') {
                    $period = language_data('6 Months');
                } elseif ($sms->recurring == 'year1') {
                    $period = language_data('Yearly');
                } elseif ($sms->recurring == 'years2') {
                    $period = language_data('2 Years');
                } elseif ($sms->recurring == 'years3') {
                    $period = language_data('3 Years');
                } else {
                    $period = language_data('Invalid');
                }

                return $period;
            })
            ->escapeColumns([])
            ->make(true);


    }


//======================================================================
// deleteRecurringSMS Function Start Here
//======================================================================
    public function deleteRecurringSMS($id)
    {

        $recurring_sms = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);

        if ($recurring_sms) {
            RecurringSMSContacts::where('campaign_id', $id)->delete();
            $recurring_sms->delete();
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('SMS info deleted successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('SMS Not Found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }



//======================================================================
// deleteRecurringSMSContact Function Start Here
//======================================================================
    public function deleteRecurringSMSContact($id)
    {

        $recurring_contact = RecurringSMSContacts::find($id);

        if ($recurring_contact) {
            $check_exist = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($recurring_contact->campaign_id);
            if ($check_exist) {
                $recurring_contact->delete();

                return redirect('user/sms/update-recurring-sms-contact/' . $recurring_contact->campaign_id)->with([
                    'message' => language_data('Contact deleted successfully', Auth::guard('client')->user()->lan_id)
                ]);
            } else {
                return redirect('user/sms/recurring-sms')->with([
                    'message' => language_data('Invalid access', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('SMS Not Found'),
                'message_important' => true
            ]);
        }

    }


//======================================================================
// bulkDeleteRecurringSMS Function Start Here
//======================================================================
    public function bulkDeleteRecurringSMS(Request $request)
    {
        if ($request->has('data_ids')) {
            $all_ids = explode(',', $request->get('data_ids'));

            if (is_array($all_ids) && count($all_ids) > 0) {
                foreach ($all_ids as $id) {

                    $check_exist = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);

                    if ($check_exist) {
                        RecurringSMSContacts::where('campaign_id', $id)->delete();
                        $check_exist->delete();
                    }
                }
            }
        }
    }


    //======================================================================
// bulkDeleteRecurringSMSContact Function Start Here
//======================================================================
    public function bulkDeleteRecurringSMSContact(Request $request)
    {
        if ($request->has('data_ids')) {
            $all_ids = explode(',', $request->get('data_ids'));
            $recipients = count($all_ids);
            if ($request->has('campaign_id')) {
                if (is_array($all_ids) && count($all_ids) > 0) {
                    $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($request->campaign_id);
                    if ($recurring) {
                        RecurringSMSContacts::destroy($all_ids);
                        $recurring->total_recipients -= $recipients;
                        $recurring->save();
                    } else {
                        return response()->json([
                            'status' => 'error',
                            'message' => language_data('Recurring SMS info not found', Auth::guard('client')->user()->lan_id)
                        ]);
                    }

                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => language_data('Recipients required', Auth::guard('client')->user()->lan_id)
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => language_data('Recurring SMS info not found', Auth::guard('client')->user()->lan_id)
                ]);
            }
            return response()->json([
                'status' => 'success',
                'message' => language_data('Contact deleted successfully', Auth::guard('client')->user()->lan_id)
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => language_data('Invalid request', Auth::guard('client')->user()->lan_id)
            ]);
        }
    }



//======================================================================
// sendRecurringSMS Function Start Here
//======================================================================
    public function sendRecurringSMS()
    {

        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $phone_book = ImportPhoneNumber::where('user_id', Auth::guard('client')->user()->id)->get();
        $client_group = ClientGroups::where('created_by', Auth::guard('client')->user()->id)->where('status', 'Yes')->get();
        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', 1)->select('country_code', 'country_name')->get();

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

        return view('client.send-recurring-sms', compact('client_group', 'sms_templates', 'sender_ids', 'phone_book', 'country_code', 'sms_gateways'));
    }

    //======================================================================
    // postRecurringSMS Function Start Here
    //======================================================================

    public function postRecurringSMS(Request $request)
    {

        if (function_exists('ini_set') && ini_get('max_execution_time')) {
            ini_set('max_execution_time', '-1');
        }


        $v = \Validator::make($request->all(), [
            'sms_gateway' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'period' => 'required', 'country_code' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/send-recurring-sms')->withInput($request->all())->withErrors($v->errors());
        }


        $gateway = SMSGateways::find($request->sms_gateway);
        if ($gateway->status != 'Active') {
            return redirect('user/sms/send-recurring-sms')->with([
                'message' => language_data('SMS gateway not active'),
                'message_important' => true
            ]);
        }


        $gateway_credential = null;
        if ($gateway->custom == 'Yes') {
            if ($gateway->type == 'smpp') {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('user/sms/send-recurring-sms')->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }
        } else {
            $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
            if ($gateway_credential == null) {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $sender_id = $request->sender_id;
        $message = $request->message;
        $msg_type = $request->message_type;


        if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
            return redirect('user/sms/send-recurring-sms')->with([
                'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($msg_type == 'voice') {
            if ($gateway->voice != 'Yes') {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type == 'mms') {

            if ($gateway->mms != 'Yes') {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $image = $request->image;

            if ($image == '') {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('MMS file required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if (app_config('AppStage') != 'Demo') {
                if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                    $destinationPath = public_path() . '/assets/mms_file/';
                    $image_name = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $image_name);
                    $media_url = asset('assets/mms_file/' . $image_name);

                } else {
                    return redirect('user/sms/send-recurring-sms')->with([
                        'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

            } else {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            $media_url = null;
            if ($message == '') {
                return redirect('user/sms/send-recurring-sms')->with([
                    'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        if (app_config('sender_id_verification') == '1') {
            if ($sender_id == null) {
                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($sender_id != '' && app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::all();
            $all_ids = [];

            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $all_ids = array_unique($all_ids);

            if (!in_array($sender_id, $all_ids)) {
                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('This Sender ID have Blocked By Administrator', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        $period = $request->period;
        $its = strtotime(date('Y-m-d'));

        if ($period == 'day') {
            $nd = date('Y-m-d', strtotime('+1 day', $its));
        } elseif ($period == 'week1') {
            $nd = date('Y-m-d', strtotime('+1 week', $its));
        } elseif ($period == 'weeks2') {
            $nd = date('Y-m-d', strtotime('+2 weeks', $its));
        } elseif ($period == 'month1') {
            $nd = date('Y-m-d', strtotime('+1 month', $its));
        } elseif ($period == 'months2') {
            $nd = date('Y-m-d', strtotime('+2 months', $its));
        } elseif ($period == 'months3') {
            $nd = date('Y-m-d', strtotime('+3 months', $its));
        } elseif ($period == 'months6') {
            $nd = date('Y-m-d', strtotime('+6 months', $its));
        } elseif ($period == 'year1') {
            $nd = date('Y-m-d', strtotime('+1 year', $its));
        } elseif ($period == 'years2') {
            $nd = date('Y-m-d', strtotime('+2 years', $its));
        } elseif ($period == 'years3') {
            $nd = date('Y-m-d', strtotime('+3 years', $its));
        } elseif ($period == '0') {
            if ($request->schedule_time == '') {
                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
            $nd = date('Y-m-d H:i:s', strtotime($request->schedule_time));
        } else {
            return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                'message' => language_data('Date Parsing Error', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($period != '0') {

            if ($request->recurring_time == '') {
                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $schedule_time = $request->recurring_time;
            $nd = date("Y-m-d H:i:s", strtotime($nd . ' ' . $schedule_time));
        }

        $message = $request->message;
        $get_cost = 0;
        $get_inactive_coverage = [];
        $results = [];

        if ($request->contact_type == 'phone_book') {
            if (count($request->contact_list_id)) {
                $get_data = ContactList::whereIn('pid', $request->contact_list_id)->select('phone_number', 'email_address', 'user_name', 'company', 'first_name', 'last_name')->get()->toArray();
                foreach ($get_data as $data) {
                    array_push($results, $data);
                }
            }
        }

        if ($request->contact_type == 'client_group') {
            $get_group = Client::whereIn('groupid', $request->client_group_id)->select('phone AS phone_number', 'email AS email_address', 'username AS user_name', 'company AS company', 'fname AS first_name', 'lname AS last_name')->get()->toArray();
            foreach ($get_group as $data) {
                array_push($results, $data);
            }
        }

        if ($request->recipients) {
            $recipients = multi_explode(array(",", "\n", ";"), $request->recipients);
            foreach ($recipients as $r) {

                $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($r));
                if ($request->country_code != 0) {
                    $phone = $request->country_code . ltrim($phone, '0');
                }

                $data = [
                    'phone_number' => $phone,
                    'email_address' => null,
                    'user_name' => null,
                    'company' => null,
                    'first_name' => null,
                    'last_name' => null
                ];
                array_push($results, $data);
            }
        }


        if (is_array($results)) {

            if (count($results) > 0) {

                $filtered_data = [];
                $blacklist = BlackListContact::select('numbers')->get()->toArray();

                if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {

                    $blacklist = array_column($blacklist, 'numbers');

                    array_filter($results, function ($element) use ($blacklist, &$filtered_data) {
                        if (!in_array($element['phone_number'], $blacklist)) {
                            array_push($filtered_data, $element);
                        }
                    });

                    $results = array_values($filtered_data);
                }

                if (count($results) <= 0) {
                    return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                        'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($request->remove_duplicate == 'yes') {
                    $results = unique_multidim_array($results, 'phone_number');
                }

                $results = array_values($results);
                $total_recipients = count($results);

                $recurring_id = RecurringSMS::create([
                    'userid' => Auth::guard('client')->user()->id,
                    'sender' => $sender_id,
                    'total_recipients' => $total_recipients,
                    'status' => 'running',
                    'type' => $msg_type,
                    'media_url' => $media_url,
                    'use_gateway' => $gateway->id,
                    'recurring' => $period,
                    'recurring_date' => $nd
                ]);


                if ($recurring_id) {
                    foreach (array_chunk($results, 50) as $chunk_result) {
                        foreach ($chunk_result as $r) {
                            $msg_data = array(
                                'Phone Number' => $r['phone_number'],
                                'Email Address' => $r['email_address'],
                                'User Name' => $r['user_name'],
                                'Company' => $r['company'],
                                'First Name' => $r['first_name'],
                                'Last Name' => $r['last_name'],
                            );

                            $get_message = $this->renderSMS($message, $msg_data);

                            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                                if ($msgcount <= 160) {
                                    $msgcount = 1;
                                } else {
                                    $msgcount = $msgcount / 157;
                                }
                            }
                            if ($msg_type == 'unicode') {
                                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                                if ($msgcount <= 70) {
                                    $msgcount = 1;
                                } else {
                                    $msgcount = $msgcount / 67;
                                }
                            }
                            $msgcount = ceil($msgcount);

                            $phone = str_replace(['(', ')', '+', '-', ' '], '', $r['phone_number']);

                            if ($gateway->name == 'FortDigital') {
                                $c_phone = 61;
                            } elseif ($gateway->name == 'Ibrbd') {
                                $c_phone = 880;
                            } else {
                                $c_phone = PhoneNumber::get_code($phone);
                            }

                            if (app_config('fraud_detection') == 1) {
                                $spam_word = SpamWord::all()->toArray();
                                if (is_array($spam_word) && count($spam_word) > 0) {
                                    $spam_word = array_column($spam_word, 'word');
                                    $search_word = implode('|', $spam_word);
                                    if (preg_match('(' . $search_word . ')', $get_message) === 1) {
                                        continue;
                                    }
                                }
                            }

                            $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
                            if ($sms_cost) {

                                $phoneUtil = PhoneNumberUtil::getInstance();
                                $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                                $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                                if ($area_code_exist) {
                                    $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                                    $get_format_data = explode(" ", $format);
                                    $operator_settings = explode('-', $get_format_data[1])[0];

                                } else {
                                    $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                                    $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                                }

                                $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                                if ($get_operator) {
                                    $sms_charge = $get_operator->price;
                                    $get_cost += $sms_charge;
                                } else {
                                    $sms_charge = $sms_cost->tariff;
                                    $get_cost += $sms_charge;
                                }
                            } else {
                                array_push($get_inactive_coverage, 'found');
                            }


                            if (in_array('found', $get_inactive_coverage)) {
                                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                                    'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                                    'message_important' => true
                                ]);
                            }

                            $total_cost = $get_cost * $msgcount;

                            RecurringSMSContacts::create([
                                'campaign_id' => $recurring_id->id,
                                'receiver' => $phone,
                                'message' => $get_message,
                                'amount' => $total_cost
                            ]);

                        }
                    }

                    return redirect('user/sms/send-recurring-sms')->with([
                        'message' => language_data('Message recurred successfully. Delivered in correct time', Auth::guard('client')->user()->lan_id)
                    ]);

                } else {
                    return redirect('user/sms/send-recurring-sms')->with([
                        'message' => language_data('Something went wrong please try again', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

            } else {
                return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                'message' => language_data('Invalid Recipients', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }

    //======================================================================
    // stopRecurringSMS Function Start Here
    //======================================================================
    public function stopRecurringSMS($id)
    {
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);

        if ($recurring) {
            $recurring->status = 'stop';
            $recurring->save();

            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring SMS stop successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // startRecurringSMS Function Start Here
    //======================================================================
    public function startRecurringSMS($id)
    {
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);

        if ($recurring) {
            $period = $recurring->recurring;
            $its = strtotime(date('Y-m-d'));

            if ($period == 'day') {
                $nd = date('Y-m-d', strtotime('+1 day', $its));
            } elseif ($period == 'week1') {
                $nd = date('Y-m-d', strtotime('+1 week', $its));
            } elseif ($period == 'weeks2') {
                $nd = date('Y-m-d', strtotime('+2 weeks', $its));
            } elseif ($period == 'month1') {
                $nd = date('Y-m-d', strtotime('+1 month', $its));
            } elseif ($period == 'months2') {
                $nd = date('Y-m-d', strtotime('+2 months', $its));
            } elseif ($period == 'months3') {
                $nd = date('Y-m-d', strtotime('+3 months', $its));
            } elseif ($period == 'months6') {
                $nd = date('Y-m-d', strtotime('+6 months', $its));
            } elseif ($period == 'year1') {
                $nd = date('Y-m-d', strtotime('+1 year', $its));
            } elseif ($period == 'years2') {
                $nd = date('Y-m-d', strtotime('+2 years', $its));
            } elseif ($period == 'years3') {
                $nd = date('Y-m-d', strtotime('+3 years', $its));
            } elseif ($period == '0') {
                $nd = date('Y-m-d H:i:s', strtotime($recurring->recurring_date));
            } else {
                return redirect('user/sms/recurring-sms')->with([
                    'message' => language_data('Date Parsing Error', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if ($period != '0') {
                $schedule_time = date("H:i:s", strtotime($recurring->recurring_date));
                $nd = date("Y-m-d H:i:s", strtotime($nd . ' ' . $schedule_time));
            }

            $recurring->recurring_date = $nd;
            $recurring->status = 'running';
            $recurring->save();

            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring SMS running successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }

    //======================================================================
    // updateRecurringSMS Function Start Here
    //======================================================================
    public function updateRecurringSMS($id)
    {
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);
        if ($recurring) {

            $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
            $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();

            return view('client.update-recurring-sms', compact('recurring', 'sms_gateways'));
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }

    //======================================================================
    // postUpdateRecurringSMS Function Start Here
    //======================================================================
    public function postUpdateRecurringSMS(Request $request)
    {
        $cmd = $request->cmd;
        $v = \Validator::make($request->all(), [
            'period' => 'required', 'sms_gateway' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/update-recurring-sms/' . $cmd)->withErrors($v->errors());
        }

        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($cmd);

        if ($recurring) {

            $gateway = SMSGateways::find($request->sms_gateway);
            if ($gateway->status != 'Active') {
                return redirect('user/sms/update-recurring-sms/' . $cmd)->with([
                    'message' => language_data('SMS gateway not active'),
                    'message_important' => true
                ]);
            }

            $period = $request->period;
            $its = strtotime(date('Y-m-d'));

            if ($period == 'day') {
                $nd = date('Y-m-d', strtotime('+1 day', $its));
            } elseif ($period == 'week1') {
                $nd = date('Y-m-d', strtotime('+1 week', $its));
            } elseif ($period == 'weeks2') {
                $nd = date('Y-m-d', strtotime('+2 weeks', $its));
            } elseif ($period == 'month1') {
                $nd = date('Y-m-d', strtotime('+1 month', $its));
            } elseif ($period == 'months2') {
                $nd = date('Y-m-d', strtotime('+2 months', $its));
            } elseif ($period == 'months3') {
                $nd = date('Y-m-d', strtotime('+3 months', $its));
            } elseif ($period == 'months6') {
                $nd = date('Y-m-d', strtotime('+6 months', $its));
            } elseif ($period == 'year1') {
                $nd = date('Y-m-d', strtotime('+1 year', $its));
            } elseif ($period == 'years2') {
                $nd = date('Y-m-d', strtotime('+2 years', $its));
            } elseif ($period == 'years3') {
                $nd = date('Y-m-d', strtotime('+3 years', $its));
            } elseif ($period == '0') {
                if ($request->schedule_time == '') {
                    return redirect('user/sms/update-recurring-sms/' . $cmd)->withInput($request->all())->with([
                        'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
                $nd = date('Y-m-d H:i:s', strtotime($request->schedule_time));
            } else {
                return redirect('user/sms/update-recurring-sms/' . $cmd)->withInput($request->all())->with([
                    'message' => language_data('Date Parsing Error'),
                    'message_important' => true
                ]);
            }

            if ($period != '0') {
                if ($request->recurring_time == '') {
                    return redirect('user/sms/update-recurring-sms/' . $cmd)->withInput($request->all())->with([
                        'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                $schedule_time = $request->recurring_time;
                $nd = date("Y-m-d H:i:s", strtotime($nd . ' ' . $schedule_time));
            }

            $recurring->use_gateway = $gateway->id;
            $recurring->recurring = $period;
            $recurring->recurring_date = $nd;
            $recurring->sender = $request->sender_id;
            $recurring->save();

            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring SMS period changed', Auth::guard('client')->user()->lan_id)
            ]);
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // addRecurringSMSContact Function Start Here
    //======================================================================
    public function addRecurringSMSContact($id)
    {
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);
        if ($recurring) {
            $country_code = IntCountryCodes::where('Active', '1')->select('country_code', 'country_name')->get();
            return view('client.add-recurring-sms-contact', compact('recurring', 'country_code'));
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // postRecurringSMSContact Function Start Here
    //======================================================================
    public function postRecurringSMSContact(Request $request)
    {
        $id = $request->recurring_id;
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);
        if ($recurring) {

            $v = \Validator::make($request->all(), [
                'recipients' => 'required', 'remove_duplicate' => 'required', 'country_code' => 'required'
            ]);

            if ($v->fails()) {
                return redirect('user/sms/add-recurring-sms-contact/' . $id)->withInput($request->all())->withErrors($v->errors());
            }


            $recipients = multi_explode(array(",", "\n", ";"), $request->recipients);
            $results = array_filter($recipients);

            if (is_array($results)) {

                $msg_type = $recurring->type;
                $message = $request->message;


                if ($msg_type != 'mms') {
                    if ($message == '') {
                        return redirect('user/sms/add-recurring-sms-contact/' . $id)->withInput($request->all())->with([
                            'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }

                $filtered_data = [];
                $blacklist = BlackListContact::select('numbers')->get()->toArray();

                if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {

                    $blacklist = array_column($blacklist, 'numbers');

                    array_filter($results, function ($element) use ($blacklist, &$filtered_data) {
                        if (!in_array(trim($element), $blacklist)) {
                            array_push($filtered_data, $element);
                        }
                    });

                    $results = array_values($filtered_data);
                }

                if (count($results) <= 0) {
                    return redirect('user/sms/add-recurring-sms-contact/' . $id)->withInput($request->all())->with([
                        'message' => language_data('Recipient empty', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

                if ($request->remove_duplicate == 'yes') {
                    $results = array_unique($results, SORT_REGULAR);
                }

                $results = array_values($results);

                $current_recipients = 0;

                if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                    $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                    if ($msgcount <= 160) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 157;
                    }
                }
                if ($msg_type == 'unicode') {
                    $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                    if ($msgcount <= 70) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 67;
                    }
                }
                $msgcount = ceil($msgcount);

                $get_inactive_coverage = [];

                foreach ($results as $r) {

                    $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($r));

                    if ($request->country_code != 0) {
                        $phone = $request->country_code . ltrim($phone, '0');
                        $c_phone = $request->country_code;
                    } else {
                        $c_phone = PhoneNumber::get_code($phone);
                    }

                    if (app_config('fraud_detection') == 1) {
                        $spam_word = SpamWord::all()->toArray();
                        if (is_array($spam_word) && count($spam_word) > 0) {
                            $spam_word = array_column($spam_word, 'word');
                            $search_word = implode('|', $spam_word);
                            if (preg_match('(' . $search_word . ')', $message) === 1) {
                                continue;
                            }
                        }
                    }

                    $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
                    if ($sms_cost) {

                        $phoneUtil = PhoneNumberUtil::getInstance();
                        $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                        $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                        if ($area_code_exist) {
                            $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                            $get_format_data = explode(" ", $format);
                            $operator_settings = explode('-', $get_format_data[1])[0];

                        } else {
                            $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                            $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                        }

                        $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                        if ($get_operator) {
                            $sms_charge = $get_operator->price;
                        } else {
                            $sms_charge = $sms_cost->tariff;
                        }
                    } else {
                        array_push($get_inactive_coverage, 'found');
                    }


                    if (in_array('found', $get_inactive_coverage)) {
                        return redirect('user/sms/send-recurring-sms')->withInput($request->all())->with([
                            'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    $total_cost = $sms_charge * $msgcount;

                    $exist_check = RecurringSMSContacts::where('campaign_id', $id)->where('receiver', $phone)->first();
                    if (!$exist_check) {
                        RecurringSMSContacts::create([
                            'campaign_id' => $id,
                            'receiver' => $phone,
                            'message' => $message,
                            'amount' => $total_cost
                        ]);
                        $current_recipients++;
                    }
                }
                $recurring->total_recipients += $current_recipients;
                $recurring->save();

                return redirect('user/sms/add-recurring-sms-contact/' . $id)->with([
                    'message' => language_data('Recurring contact added successfully', Auth::guard('client')->user()->lan_id)
                ]);
            } else {
                return redirect('user/sms/recurring-sms')->withInput($request->all())->with([
                    'message' => language_data('Invalid request', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // postUpdateRecurringSMSContactData Function Start Here
    //======================================================================
    public function postUpdateRecurringSMSContactData(Request $request)
    {
        $id = $request->recurring_id;
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);
        if ($recurring) {

            $contact_id = $request->contact_id;

            $v = \Validator::make($request->all(), [
                'phone_number' => 'required', 'message' => 'required'
            ]);

            if ($v->fails()) {
                return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->withInput($request->all())->withErrors($v->errors());
            }

            $msg_type = $recurring->type;
            $message = $request->message;

            if ($msg_type != 'mms') {
                if ($message == '') {
                    return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->withInput($request->all())->with([
                        'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }

            if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));
                if ($msgcount <= 160) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 157;
                }
            }
            if ($msg_type == 'unicode') {
                $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($message)), 'UTF-8');

                if ($msgcount <= 70) {
                    $msgcount = 1;
                } else {
                    $msgcount = $msgcount / 67;
                }
            }

            $msgcount = ceil($msgcount);

            $blacklist = BlackListContact::where('user_id', 0)->select('numbers')->get()->toArray();

            if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {
                $blacklist = array_column($blacklist, 'numbers');
            }

            if (in_array($request->phone_number, $blacklist)) {
                return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->withInput($request->all())->with([
                    'message' => language_data('Phone number contain in blacklist', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }


            $phone = str_replace(['(', ')', '+', '-', ' '], '', $request->phone_number);
            $c_phone = PhoneNumber::get_code($phone);

            if (app_config('fraud_detection') == 1) {
                $spam_word = SpamWord::all()->toArray();
                if (is_array($spam_word) && count($spam_word) > 0) {
                    $spam_word = array_column($spam_word, 'word');
                    $search_word = implode('|', $spam_word);
                    if (preg_match('(' . $search_word . ')', $message) === 1) {
                        return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->withInput($request->all())->with([
                            'message' => language_data('Message contain spam word', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }
                }
            }

            $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
            if ($sms_cost) {

                $phoneUtil = PhoneNumberUtil::getInstance();
                $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                if ($area_code_exist) {
                    $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                    $get_format_data = explode(" ", $format);
                    $operator_settings = explode('-', $get_format_data[1])[0];

                } else {
                    $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                    $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                }

                $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                if ($get_operator) {
                    $sms_charge = $get_operator->price;
                } else {
                    $sms_charge = $sms_cost->tariff;
                }
            } else {
                return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->withInput($request->all())->with([
                    'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }


            $total_cost = $sms_charge * $msgcount;

            RecurringSMSContacts::find($contact_id)->update([
                'receiver' => $phone,
                'message' => $message,
                'amount' => $total_cost
            ]);

            return redirect('user/sms/update-recurring-sms-contact-data/' . $contact_id)->with([
                'message' => language_data('Recurring contact updated successfully', Auth::guard('client')->user()->lan_id)
            ]);

        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

    }


    //======================================================================
    // updateRecurringSMSContact Function Start Here
    //======================================================================
    public function updateRecurringSMSContact($id)
    {
        $recurring = RecurringSMS::where('userid', Auth::guard('client')->user()->id)->find($id);
        if ($recurring) {
            return view('client.update-recurring-sms-contact', compact('id'));
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


    //======================================================================
    // getRecurringSMSContactData Function Start Here
    //======================================================================
    public function getRecurringSMSContactData($id, Request $request)
    {

        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];
        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }


        $recurring_sms = RecurringSMSContacts::where('campaign_id', $id)->select(['id', 'receiver', 'message', 'amount'])->orderBy($get_search_column, $short_by);
        return Datatables::of($recurring_sms)
            ->addColumn('action', function ($sms) {
                return '
                <a href="' . url("user/sms/update-recurring-sms-contact-data/$sms->id") . '" class="btn btn-xs btn-complete"><i class="fa fa-edit"></i> ' . language_data('Update') . '</a>
                <a href="#" id="' . $sms->id . '" class="cdelete btn btn-xs btn-danger"><i class="fa fa-trash"></i> ' . language_data('Delete') . '</a>
                
                ';
            })
            ->addColumn('id', function ($sms) {
                return "<div class='coder-checkbox'>
                             <input type='checkbox'  class='deleteRow' value='$sms->id'  />
                                            <span class='co-check-ui'></span>
                                        </div>";

            })
            ->escapeColumns([])
            ->make(true);
    }


    //======================================================================
    // updateRecurringSMSContactData Function Start Here
    //======================================================================
    public function updateRecurringSMSContactData($id)
    {
        $recurring = RecurringSMSContacts::find($id);
        if ($recurring) {
            return view('client.update-recurring-sms-contact-data', compact('recurring'));
        } else {
            return redirect('user/sms/recurring-sms')->with([
                'message' => language_data('Recurring information not found', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }



    //======================================================================
    // sendRecurringSMSFile Function Start Here
    //======================================================================
    public function sendRecurringSMSFile()
    {

        if (app_config('sender_id_verification') == '1') {
            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
            $all_ids = [];


            foreach ($all_sender_id as $sid) {
                $client_array = json_decode($sid->cl_id);

                if (is_array($client_array) && in_array('0', $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                    array_push($all_ids, $sid->sender_id);
                }
            }
            $sender_ids = array_unique($all_ids);

        } else {
            $sender_ids = false;
        }

        $sms_templates = SMSTemplates::where('status', 'active')->where('cl_id', Auth::guard('client')->user()->id)->get();
        $country_code = IntCountryCodes::where('Active', '1')->select('country_code', 'country_name')->get();

        $gateways = json_decode(Auth::guard('client')->user()->sms_gateway);
        $sms_gateways = SMSGateways::whereIn('id', $gateways)->where('status', 'Active')->get();


        return view('client.send-recurring-sms-file', compact('sms_templates', 'sender_ids', 'country_code', 'sms_gateways'));
    }




    //======================================================================
    // postRecurringSMSFile Function Start Here
    //======================================================================
    public function postRecurringSMSFile(Request $request)
    {

        $appStage = app_config('AppStage');
        if ($appStage == 'Demo') {
            return redirect('user/sms/send-recurring-sms-file')->with([
                'message' => language_data('This Option is Disable In Demo Mode'),
                'message_important' => true
            ]);
        }

        if (function_exists('ini_set') && ini_get('max_execution_time')) {
            ini_set('max_execution_time', '-1');
        }

        $v = \Validator::make($request->all(), [
            'import_numbers' => 'required', 'sms_gateway' => 'required', 'message_type' => 'required', 'remove_duplicate' => 'required', 'period' => 'required'
        ]);

        if ($v->fails()) {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->withErrors($v->errors());
        }

        $gateway = SMSGateways::find($request->sms_gateway);
        if ($gateway->status != 'Active') {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('SMS gateway not active', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $gateway_credential = null;
        if ($gateway->custom == 'Yes') {
            if ($gateway->type == 'smpp') {
                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                if ($gateway_credential == null) {
                    return redirect('user/sms/send-recurring-sms-file')->with([
                        'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }
            }
        } else {
            $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
            if ($gateway_credential == null) {
                return redirect('user/sms/send-recurring-sms-file')->with([
                    'message' => language_data('SMS gateway not active.Contact with Provider', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        $sender_id = $request->sender_id;
        $msg_type = $request->message_type;
        $message = $request->message;

        if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('Invalid message type', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($msg_type == 'voice') {
            if ($gateway->voice != 'Yes') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported Voice feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }

        if ($msg_type == 'mms') {

            if ($gateway->mms != 'Yes') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('SMS Gateway not supported MMS feature', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $image = $request->image;

            if ($image == '') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('MMS file required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            if (app_config('AppStage') != 'Demo') {
                if (isset($image) && in_array($image->getClientOriginalExtension(), array("png", "jpeg", "gif", 'jpg', 'mp3', 'mp4', '3gp', 'mpg', 'mpeg'))) {
                    $destinationPath = public_path() . '/assets/mms_file/';
                    $image_name = $image->getClientOriginalName();
                    Input::file('image')->move($destinationPath, $image_name);
                    $media_url = asset('assets/mms_file/' . $image_name);

                } else {
                    return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                        'message' => language_data('Upload .png or .jpeg or .jpg or .gif or .mp3 or .mp4 or .3gp or .mpg or .mpeg file', Auth::guard('client')->user()->lan_id),
                        'message_important' => true
                    ]);
                }

            } else {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('MMS is disable in demo mode', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        } else {
            $media_url = null;
            if ($message == '') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('Message required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
        }


        $file_extension = Input::file('import_numbers')->getClientOriginalExtension();

        $supportedExt = array('csv', 'xls', 'xlsx');

        if (!in_array_r($file_extension, $supportedExt)) {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('Insert Valid Excel or CSV file', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }


        $period = $request->period;
        $its = strtotime(date('Y-m-d'));

        if ($period == 'day') {
            $nd = date('Y-m-d', strtotime('+1 day', $its));
        } elseif ($period == 'week1') {
            $nd = date('Y-m-d', strtotime('+1 week', $its));
        } elseif ($period == 'weeks2') {
            $nd = date('Y-m-d', strtotime('+2 weeks', $its));
        } elseif ($period == 'month1') {
            $nd = date('Y-m-d', strtotime('+1 month', $its));
        } elseif ($period == 'months2') {
            $nd = date('Y-m-d', strtotime('+2 months', $its));
        } elseif ($period == 'months3') {
            $nd = date('Y-m-d', strtotime('+3 months', $its));
        } elseif ($period == 'months6') {
            $nd = date('Y-m-d', strtotime('+6 months', $its));
        } elseif ($period == 'year1') {
            $nd = date('Y-m-d', strtotime('+1 year', $its));
        } elseif ($period == 'years2') {
            $nd = date('Y-m-d', strtotime('+2 years', $its));
        } elseif ($period == 'years3') {
            $nd = date('Y-m-d', strtotime('+3 years', $its));
        } elseif ($period == '0') {
            if ($request->schedule_time == '') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }
            $nd = date('Y-m-d H:i:s', strtotime($request->schedule_time));
        } else {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('Date Parsing Error', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($period != '0') {

            if ($request->recurring_time == '') {
                return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                    'message' => language_data('Schedule time required', Auth::guard('client')->user()->lan_id),
                    'message_important' => true
                ]);
            }

            $schedule_time = $request->recurring_time;
            $nd = date("Y-m-d H:i:s", strtotime($nd . ' ' . $schedule_time));
        }

        $all_data = Excel::load($request->import_numbers)->noHeading()->all()->toArray();

        if ($all_data && is_array($all_data) && array_empty($all_data)) {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('Empty field', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        $counter = "A";

        if ($request->header_exist == 'on') {

            $header = array_shift($all_data);

            foreach ($header as $key => $value) {
                if (!$value) {
                    $header[$key] = "Column " . $counter;
                }

                $counter++;
            }

        } else {

            $header_like = $all_data[0];

            $header = array();

            foreach ($header_like as $h) {
                array_push($header, "Column " . $counter);
                $counter++;
            }

        }

        $all_data = array_map(function ($row) use ($header) {

            return array_combine($header, $row);

        }, $all_data);

        $valid_phone_numbers = [];
        $get_data = [];

        $blacklist = BlackListContact::where('user_id', 0)->select('numbers')->get()->toArray();

        if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {
            $blacklist = array_column($blacklist, 'numbers');
        }


        $number_column = $request->number_column;

        array_filter($all_data, function ($data) use ($number_column, &$get_data, &$valid_phone_numbers, $blacklist) {

            if ($data[$number_column]) {
                if (!in_array($data[$number_column], $blacklist)) {
                    array_push($valid_phone_numbers, $data[$number_column]);
                    array_push($get_data, $data);
                }
            }
        });

        if (is_array($valid_phone_numbers) && count($valid_phone_numbers) <= 0) {
            return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                'message' => language_data('Invalid phone numbers', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }

        if ($request->remove_duplicate == 'yes') {
            $get_data = unique_multidim_array($get_data, $number_column);
        }

        $results = array_values($get_data);
        $total_recipients = count($results);

        $recurring_id = RecurringSMS::create([
            'userid' => Auth::guard('client')->user()->id,
            'sender' => $sender_id,
            'total_recipients' => $total_recipients,
            'status' => 'running',
            'type' => $msg_type,
            'media_url' => $media_url,
            'use_gateway' => $gateway->id,
            'recurring' => $period,
            'recurring_date' => $nd
        ]);

        if ($recurring_id) {
            foreach (array_chunk($results, 50) as $chunk_result) {
                foreach ($chunk_result as $msg_data) {

                    $get_message = $this->renderSMS($message, $msg_data);

                    $phone = str_replace(['(', ')', '+', '-', ' '], '', $msg_data[$number_column]);
                    if ($request->country_code != 0) {
                        $phone = $request->country_code . ltrim($phone, '0');
                        $c_phone = $request->country_code;
                    } else {
                        $c_phone = PhoneNumber::get_code($phone);
                    }

                    if (app_config('fraud_detection') == 1) {
                        $spam_word = SpamWord::all()->toArray();
                        if (is_array($spam_word) && count($spam_word) > 0) {
                            $spam_word = array_column($spam_word, 'word');
                            $search_word = implode('|', $spam_word);
                            if (preg_match('(' . $search_word . ')', $message) === 1) {
                                continue;
                            }
                        }
                    }

                    $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();
                    if ($sms_cost) {

                        $phoneUtil = PhoneNumberUtil::getInstance();
                        $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                        $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                        if ($area_code_exist) {
                            $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                            $get_format_data = explode(" ", $format);
                            $operator_settings = explode('-', $get_format_data[1])[0];

                        } else {
                            $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                            $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                        }

                        $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();
                        if ($get_operator) {
                            $sms_charge = $get_operator->price;
                        } else {
                            $sms_charge = $sms_cost->tariff;
                        }
                    } else {
                        return redirect('user/sms/send-recurring-sms-file')->withInput($request->all())->with([
                            'message' => language_data('Phone Number Coverage are not active', Auth::guard('client')->user()->lan_id),
                            'message_important' => true
                        ]);
                    }

                    if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($get_message)));
                        if ($msgcount <= 160) {
                            $msgcount = 1;
                        } else {
                            $msgcount = $msgcount / 157;
                        }
                    }
                    if ($msg_type == 'unicode') {
                        $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($get_message)), 'UTF-8');

                        if ($msgcount <= 70) {
                            $msgcount = 1;
                        } else {
                            $msgcount = $msgcount / 67;
                        }
                    }
                    $msgcount = ceil($msgcount);

                    $total_cost = $sms_charge * $msgcount;

                    RecurringSMSContacts::create([
                        'campaign_id' => $recurring_id->id,
                        'receiver' => $phone,
                        'message' => $get_message,
                        'amount' => $total_cost
                    ]);

                }
            }

            return redirect('user/sms/send-recurring-sms-file')->with([
                'message' => language_data('Message recurred successfully. Delivered in correct time', Auth::guard('client')->user()->lan_id)
            ]);
        } else {
            return redirect('user/sms/send-recurring-sms-file')->with([
                'message' => language_data('Something went wrong please try again', Auth::guard('client')->user()->lan_id),
                'message_important' => true
            ]);
        }
    }


}