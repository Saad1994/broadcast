<?php

namespace App\Http\Controllers;

use App\Admin;
use App\BlackListContact;
use App\BlockMessage;
use App\Classes\PhoneNumber;
use App\Client;
use App\ContactList;
use App\CustomSMSGateways;
use App\EmailTemplates;
use App\ImportPhoneNumber;
use App\IntCountryCodes;
use App\Jobs\SendBulkMMS;
use App\Jobs\SendBulkSMS;
use App\Jobs\SendBulkVoice;
use App\Operator;
use App\ScheduleSMS;
use App\SenderIdManage;
use App\SMSGatewayCredential;
use App\SMSGateways;
use App\SMSHistory;
use App\SpamWord;
use App\TwoWayCommunication;
use Illuminate\Http\Request;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberToCarrierMapper;
use libphonenumber\PhoneNumberUtil;
use Twilio\Twiml;

class PublicAccessController extends Controller
{

    //======================================================================
    // ultimateSMSApi Function Start Here
    //======================================================================
    public function ultimateSMSApi(Request $request)
    {

        $action = $request->input('action');
        $api_key = $request->input('api_key');
        $to = $request->input('to');
        $from = $request->input('from');
        $sms = $request->input('sms');
        $unicode = $request->input('unicode');
        $voice = $request->input('voice');
        $mms = $request->input('mms');
        $media_url = $request->input('media_url');
        $schedule_time = $request->input('schedule');

        if ($action == '' && $api_key == '') {
            return response()->json([
                'code' => '100',
                'message' => 'Bad gateway requested'
            ]);
        }

        switch ($action) {
            case 'send-sms':

                if ($to == '' && $from == '' && $sms == '') {
                    return response()->json([
                        'code' => '100',
                        'message' => 'Bad gateway requested'
                    ]);
                }


                $blacklist = BlackListContact::select('numbers')->get()->toArray();

                if ($blacklist && is_array($blacklist) && count($blacklist) > 0) {
                    $blacklist = array_column($blacklist, 'numbers');
                }

                if (in_array($to, $blacklist)) {
                    return response()->json([
                        'code' => '112',
                        'message' => 'Destination number contain in blacklist number'
                    ]);
                }

                $msg_type = 'plain';

                if ($unicode == 1) {
                    $msg_type = 'unicode';
                }

                if ($voice == 1) {
                    $msg_type = 'voice';
                }

                if ($mms == 1) {
                    $msg_type = 'mms';
                    if ($media_url == '') {
                        return response()->json([
                            'code' => '110',
                            'message' => 'Media url required'
                        ]);
                    }
                }

                if ($schedule_time != '') {
                    if (\DateTime::createFromFormat('m/d/Y h:i A', $schedule_time) !== FALSE) {
                        $schedule_time = date('Y-m-d H:i:s', strtotime($schedule_time));
                    } else {
                        return response()->json([
                            'code' => '109',
                            'message' => 'Invalid Schedule Time'
                        ]);
                    }
                }

                $isValid = PhoneNumberUtil::isViablePhoneNumber($to);

                if (!$isValid) {
                    return response()->json([
                        'code' => '103',
                        'message' => 'Invalid Phone Number'
                    ]);
                }

                if ($msg_type != 'plain' && $msg_type != 'unicode' && $msg_type != 'voice' && $msg_type != 'mms') {
                    return response()->json([
                        'code' => '107',
                        'message' => 'Invalid SMS Type'
                    ]);
                }

                if ($msg_type == 'plain' || $msg_type == 'voice' || $msg_type == 'mms') {
                    $msgcount = strlen(preg_replace('/\s+/', ' ', trim($sms)));
                    if ($msgcount <= 160) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 157;
                    }
                }

                if ($msg_type == 'unicode') {
                    $msgcount = mb_strlen(preg_replace('/\s+/', ' ', trim($sms)), 'UTF-8');

                    if ($msgcount <= 70) {
                        $msgcount = 1;
                    } else {
                        $msgcount = $msgcount / 67;
                    }
                }

                $msgcount = ceil($msgcount);

                if (app_config('api_key') == $api_key) {
                    $gateway = SMSGateways::find(app_config('sms_api_gateway'));

                    if (!$gateway) {
                        return response()->json([
                            'code' => '108',
                            'message' => 'SMS Gateway not active'
                        ]);
                    }

                    $gateway_credential = null;
                    $cg_info = null;
                    if ($gateway->custom == 'Yes') {
                        if ($gateway->type == 'smpp') {
                            $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                            if ($gateway_credential == null) {
                                return response()->json([
                                    'code' => '108',
                                    'message' => 'SMS Gateway not active'
                                ]);
                            }
                        } else {
                            $cg_info = CustomSMSGateways::where('gateway_id', $gateway->id)->first();
                        }

                    } else {
                        $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                        if ($gateway_credential == null) {
                            return response()->json([
                                'code' => '108',
                                'message' => 'SMS Gateway not active'
                            ]);
                        }
                    }

                    $user_id = '0';

                    if ($schedule_time != '') {
                        $insert_data = [
                            'userid' => $user_id,
                            'sender' => $from,
                            'receiver' => $to,
                            'amount' => $msgcount,
                            'message' => $sms,
                            'use_gateway' => $gateway->id,
                            'type' => $msg_type,
                            'submit_time' => $schedule_time,
                            'media_url' => $media_url,
                        ];

                        $status = ScheduleSMS::create($insert_data);

                        if ($status) {
                            return response()->json([
                                'code' => 'ok',
                                'message' => 'SMS scheduled successfully. Send in correct time'
                            ]);
                        } else {
                            return response()->json([
                                'code' => '101',
                                'message' => 'Wrong action'
                            ]);
                        }

                    }

                    if ($msg_type == 'plain' || $msg_type == 'unicode') {
                        $this->dispatch(new SendBulkSMS($user_id, $to, $gateway, $gateway_credential, $from, $sms, $msgcount, $cg_info, $api_key, $msg_type));
                    }

                    if ($msg_type == 'voice') {
                        $this->dispatch(new SendBulkVoice($user_id, $to, $gateway, $gateway_credential, $from, $sms, $msgcount, $api_key));
                    }
                    if ($msg_type == 'mms') {
                        $this->dispatch(new SendBulkMMS($user_id, $to, $gateway, $gateway_credential, $from, $sms, $media_url, $api_key));
                    }

                    return response()->json([
                        'code' => 'ok',
                        'message' => 'Successfully Send',
                        'balance' => 'Unlimited',
                        'user' => 'Admin'
                    ]);

                } else {
                    $client = Client::where('api_key', $api_key)->where('api_access', 'Yes')->first();
                    if ($client) {
                        $user_id = $client->id;

                        if ($from != '' && app_config('sender_id_verification') == '1') {

                            $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
                            $all_ids = [];

                            foreach ($all_sender_id as $sid) {
                                $client_array = json_decode($sid->cl_id);

                                if (in_array('0', $client_array)) {
                                    array_push($all_ids, $from);
                                } elseif (in_array($client->id, $client_array)) {
                                    array_push($all_ids, $sid->sender_id);
                                }
                            }

                            $all_ids = array_unique($all_ids);

                            if (!in_array($from, $all_ids)) {
                                return response()->json([
                                    'code' => '106',
                                    'message' => 'Invalid Sender id'
                                ]);
                            }
                        }


                        $gateway = SMSGateways::find($client->api_gateway);

                        if (!$gateway) {
                            return response()->json([
                                'code' => '108',
                                'message' => 'SMS Gateway not active'
                            ]);
                        }

                        $gateway_credential = null;
                        $cg_info = null;
                        if ($gateway->custom == 'Yes') {
                            if ($gateway->type == 'smpp') {
                                $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                                if ($gateway_credential == null) {
                                    return response()->json([
                                        'code' => '108',
                                        'message' => 'SMS Gateway not active'
                                    ]);
                                }
                            } else {
                                $cg_info = CustomSMSGateways::where('gateway_id', $gateway->id)->first();
                            }

                        } else {
                            $gateway_credential = SMSGatewayCredential::where('gateway_id', $gateway->id)->where('status', 'Active')->first();
                            if ($gateway_credential == null) {
                                return response()->json([
                                    'code' => '108',
                                    'message' => 'SMS Gateway not active'
                                ]);
                            }
                        }

                        $phone = str_replace(['(', ')', '+', '-', ' '], '', trim($to));
                        $c_phone = PhoneNumber::get_code($phone);

                        $sms_cost = IntCountryCodes::where('country_code', $c_phone)->where('active', '1')->first();

                        if ($sms_cost) {
                            $phoneUtil = PhoneNumberUtil::getInstance();
                            $phoneNumberObject = $phoneUtil->parse('+' . $phone, null);
                            $area_code_exist = $phoneUtil->getLengthOfGeographicalAreaCode($phoneNumberObject);

                            if ($area_code_exist) {
                                $format = $phoneUtil->format($phoneNumberObject, PhoneNumberFormat::INTERNATIONAL);
                                $get_format_data = explode(" ", $format);
                                $operator_settings = explode('-', $get_format_data[1])[0];

                            } else {
                                $carrierMapper = PhoneNumberToCarrierMapper::getInstance();
                                $operator_settings = $carrierMapper->getNameForNumber($phoneNumberObject, 'en');
                            }

                            $get_operator = Operator::where('operator_setting', $operator_settings)->where('coverage_id', $sms_cost->id)->first();

                            if ($get_operator) {
                                $total_cost = ($get_operator->price * $msgcount);
                            } else {
                                $total_cost = ($sms_cost->tariff * $msgcount);
                            }

                            if ($total_cost == 0) {
                                return response()->json([
                                    'code' => '105',
                                    'message' => 'Insufficient balance'
                                ]);
                            }

                            if ($total_cost > $client->sms_limit) {
                                return response()->json([
                                    'code' => '105',
                                    'message' => 'Insufficient balance'
                                ]);
                            }


                            if (app_config('fraud_detection') == 1) {
                                $spam_word = SpamWord::all()->toArray();
                                if (is_array($spam_word) && count($spam_word) > 0) {
                                    $spam_word = array_column($spam_word, 'word');
                                    $admin = Admin::where('username', 'admin')->first();
                                    $admin_email = $admin->email;
                                    $admin_name = $admin->fname . ' ' . $admin->lname;

                                    $conf = EmailTemplates::where('tplname', '=', 'Spam Word Notification')->first();
                                    $estatus = $conf->status;

                                    $search_word = implode('|', $spam_word);


                                    if (preg_match('(' . $search_word . ')', $sms) === 1) {

                                        BlockMessage::create([
                                            'user_id' => $client->id,
                                            'sender' => $from,
                                            'receiver' => $to,
                                            'message' => $sms,
                                            'scheduled_time' => null,
                                            'use_gateway' => $gateway->id,
                                            'status' => 'block'
                                        ]);


                                        if ($estatus == '1') {

                                            $sysEmail = app_config('Email');
                                            $sysCompany = app_config('AppName');
                                            $sysUrl = url('clients/view/' . $client->id);

                                            $template = $conf->message;
                                            $subject = $conf->subject;

                                            $data = array(
                                                'user_name' => $client->username,
                                                'business_name' => $sysCompany,
                                                'message' => $sms,
                                                'profile_link' => $sysUrl,
                                                'template' => $template
                                            );

                                            $body = _render($template, $data);
                                            $mail_subject = _render($subject, $data);

                                            /*Set Authentication*/

                                            $default_gt = app_config('Gateway');

                                            if ($default_gt == 'default') {

                                                $mail = new \PHPMailer();

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();

                                            } else {
                                                $host = app_config('SMTPHostName');
                                                $smtp_username = app_config('SMTPUserName');
                                                $stmp_password = app_config('SMTPPassword');
                                                $port = app_config('SMTPPort');
                                                $secure = app_config('SMTPSecure');


                                                $mail = new \PHPMailer();

                                                $mail->isSMTP();                                      // Set mailer to use SMTP
                                                $mail->Host = $host;  // Specify main and backup SMTP servers
                                                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                                $mail->Username = $smtp_username;                 // SMTP username
                                                $mail->Password = $stmp_password;                           // SMTP password
                                                $mail->SMTPSecure = $secure;                            // Enable TLS encryption, `ssl` also accepted
                                                $mail->Port = $port;

                                                $mail->setFrom($sysEmail, $sysCompany);
                                                $mail->addAddress($admin_email, $admin_name);     // Add a recipient
                                                $mail->isHTML(true);                                  // Set email format to HTML

                                                $mail->Subject = $mail_subject;
                                                $mail->Body = $body;
                                                $mail->send();


                                            }
                                        }

                                        return response()->json([
                                            'code' => '111',
                                            'message' => 'SMS contain spam word. Wait for approval'
                                        ]);

                                    }

                                }
                            }

                            if ($schedule_time != '') {
                                $insert_data = [
                                    'userid' => $user_id,
                                    'sender' => $from,
                                    'receiver' => $to,
                                    'amount' => $msgcount,
                                    'message' => $sms,
                                    'use_gateway' => $gateway->id,
                                    'type' => $msg_type,
                                    'submit_time' => $schedule_time,
                                    'media_url' => $media_url,
                                ];

                                $status = ScheduleSMS::create($insert_data);

                                if ($status) {
                                    return response()->json([
                                        'code' => 'ok',
                                        'message' => 'SMS scheduled successfully. Send in correct time'
                                    ]);
                                } else {
                                    return response()->json([
                                        'code' => '101',
                                        'message' => 'Wrong action'
                                    ]);
                                }

                            }

                            if ($msg_type == 'plain' || $msg_type == 'unicode') {
                                $this->dispatch(new SendBulkSMS($user_id, $to, $gateway, $gateway_credential, $from, $sms, $msgcount, $cg_info, $api_key, $msg_type));
                            }

                            if ($msg_type == 'voice') {
                                $this->dispatch(new SendBulkVoice($user_id, $to, $gateway, $gateway_credential, $from, $sms, $msgcount, $api_key));
                            }
                            if ($msg_type == 'mms') {
                                $this->dispatch(new SendBulkMMS($user_id, $to, $gateway, $gateway_credential, $from, $sms, $media_url, $api_key));
                            }

                            $remain_sms = $client->sms_limit - $total_cost;
                            $client->sms_limit = $remain_sms;
                            $client->save();

                            $balance = $client->sms_limit;
                            return response()->json([
                                'code' => 'ok',
                                'message' => 'Successfully Send',
                                'balance' => $balance,
                                'user' => $client->fname . ' ' . $client->lname
                            ]);

                        } else {
                            return response()->json([
                                'code' => '104',
                                'message' => 'Phone coverage not active'
                            ]);
                        }

                    } else {
                        return response()->json([
                            'code' => '102',
                            'message' => 'Authentication Failed'
                        ]);
                    }
                }
                break;

            case 'get-inbox':
                $all_messages = SMSHistory::where('api_key', $api_key)->select('id', 'sender', 'receiver')->get();
                $return_data = [];
                $all_message = [];
                foreach ($all_messages as $msg) {
                    $return_data['id'] = $msg->id;
                    $return_data['from'] = $msg->sender;
                    $return_data['phone'] = $msg->receiver;
                    $return_data['sms'] = $msg->message;
                    $return_data['segments'] = $msg->amount;
                    $return_data['status'] = $msg->status;
                    $return_data['type'] = $msg->sms_type;
                    array_push($all_message, $return_data);
                }

                return response()->json($all_message);

                break;

            case 'check-balance':
                if (app_config('api_key') == $api_key) {

                    return response()->json([
                        'balance' => 'Unlimited',
                        'user' => 'Admin',
                        'country' => app_config('Country')
                    ]);

                } else {
                    $client = Client::where('api_key', $api_key)->where('api_access', 'Yes')->first();
                    if ($client) {
                        $balance = round($client->sms_limit);

                        return response()->json([
                            'balance' => $balance,
                            'user' => $client->fname . ' ' . $client->lname,
                            'country' => $client->country
                        ]);
                    } else {
                        return response()->json([
                            'code' => '102',
                            'message' => 'Authentication Failed'
                        ]);
                    }
                }
                break;

            default:
                return response()->json([
                    'code' => '101',
                    'message' => 'Wrong action'
                ]);
                break;
        }

    }


    //======================================================================
    // insertSMS Function Start Here
    //======================================================================
    public function insertSMS($number, $msg_count, $body, $to = '', $gateway = '')
    {
        $get_info = SMSHistory::where('receiver', 'like', '%' . $number)->orderBy('id', 'desc')->first();
        $sms_gateway = SMSGateways::where('settings', $gateway)->first();

        $blacklist_word = ['stop','end','cancel','unsubscribe','quit'];
        $reply_word = strtolower($body);

        if ($get_info) {
            if (in_array($reply_word,$blacklist_word)) {
                BlackListContact::create([
                    'user_id' => $get_info->userid,
                    'numbers' => $number
                ]);
            }

            if (strtolower($body) == 'start') {
                $contact = BlackListContact::where('numbers', $number)->first();
                if ($contact) {
                    $contact->delete();
                }
            }

            $status = SMSHistory::create([
                'userid' => $get_info->userid,
                'sender' => $number,
                'receiver' => $to,
                'message' => $body,
                'amount' => $msg_count,
                'status' => 'Success',
                'api_key' => null,
                'use_gateway' => $sms_gateway->id,
                'send_by' => 'receiver',
                'sms_type' => 'plain'
            ]);

        } else {

            if (in_array($reply_word,$blacklist_word)) {
                BlackListContact::create([
                    'user_id' => 0,
                    'numbers' => $number
                ]);
            }

            if (strtolower($body) == 'start') {
                $contact = BlackListContact::where('numbers', $number)->first();
                if ($contact) {
                    $contact->delete();
                }
            }


            $status = SMSHistory::create([
                'userid' => 0,
                'sender' => $number,
                'receiver' => $to,
                'message' => $body,
                'amount' => $msg_count,
                'status' => 'Success',
                'api_key' => null,
                'use_gateway' => $sms_gateway->id,
                'send_by' => 'receiver',
                'sms_type' => 'plain'
            ]);
        }

        if ($status) {
            return true;
        } else {
            return false;
        }


    }

    //======================================================================
    // replyTwilio Function Start Here
    //======================================================================
    public function replyTwilio(Request $request)
    {
        $number = $request->input('From');
        $to = $request->input('To');
        $body = $request->input('Body');

        if ($number == '' && $body == '' && $to == '') {
            $response = new Twiml();
            $response->message("Invalid request");
            $out_put = (string)$response;
            return $out_put;
        }

        $clphone = str_replace(" ", "", $number); #Remove any whitespace
        $clphone = str_replace('+', '', $clphone);

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($clphone, $msgcount, $body, $to, 'Twilio');

        if ($get_status) {
        	return $body;

        } else {
        	return 'Failed';

        }

    }
    //======================================================================
    // replyCustomGatewayMessage Function Start Here
    //======================================================================
    public function replyCustomGatewayMessage($id,Request $request)
    {
        $gateway = TwoWayCommunication::where('gateway_id',$id)->first();

        if ($gateway){
            $gateway_name = get_sms_gateway_info($id)->settings;
            $source = $gateway->source_param;
            $destination = $gateway->destination_param;
            $message = $gateway->message_param;
            
            $number = $request->input($source);
            $to = $request->input($destination);
            $body = $request->input($message);

            if ($number == '' && $body == '' && $to == '') {
                return 'Invalid request';
            }

            $clphone = str_replace(" ", "", $number); #Remove any whitespace
            $clphone = str_replace('+', '', $clphone);

            $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
            $msgcount = $msgcount / 160;
            $msgcount = ceil($msgcount);

            $get_status = $this->insertSMS($clphone, $msgcount, $body, $to, $gateway_name);

            if ($get_status) {
                return $body;
            } else {
                return 'Failed';
            }
        }else{
            return 'Invalid request';
        }
    }

    //======================================================================
    // replyTxtLocal Function Start Here
    //======================================================================
    public function replyTxtLocal(Request $request)
    {
        $number = $request->input('inNumber');
        $sender = $request->input('sender');
        $body = $request->input('content');


        if ($number == '' && $body == '') {
            return 'Invalid Request';
        }

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $body, $sender, 'Text Local');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    //======================================================================
    // replySmsGlobal Function Start Here
    //======================================================================
    public function replySmsGlobal(Request $request)
    {
        $number = $request->input('to');
        $sender = $request->input('from');
        $body = $request->input('msg');


        if ($number == '' && $body == '') {
            return 'Invalid Request';
        }

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $body, $sender, 'SMSGlobal');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    //======================================================================
    // replyBulkSMS Function Start Here
    //======================================================================
    public function replyBulkSMS(Request $request)
    {
        $number = $request->input('msisdn');
        $sender = $request->input('sender');
        $body = $request->input('message');


        if ($number == '' && $body == '') {
            return 'Invalid Request';
        }

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $body, $sender, 'Bulk SMS');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    //======================================================================
    // replyNexmo Function Start Here
    //======================================================================
    public function replyNexmo(Request $request)
    {

        $number = $request->input('msisdn');
        $request = array_merge($_GET, $_POST);

// check that request is inbound message
        if (!isset($request['to']) OR !isset($request['msisdn']) OR !isset($request['text'])) {
            return;
        }

//Deal with concatenated messages
        $message = false;
        if (isset($request['concat']) AND $request['concat'] == true) {

            //generally this would be a database
            session_start();
            session_id($request['concat-ref']);

            if (!isset($_SESSION['messages'])) {
                $_SESSION['messages'] = array();
            }

            $_SESSION['messages'][] = $request;

            if (count($_SESSION['messages']) == $request['concat-total']) {
                //order messages
                usort(
                    $_SESSION['messages'], function ($a, $b) {
                    return $a['concat-part'] > $b['concat-part'];
                }
                );

                $message = array_reduce(
                    $_SESSION['messages'], function ($carry, $item) {
                    return $carry . $item['text'];
                }
                );
            }
        }

        $sender = $request['to'];
        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $message, $sender, 'Nexmo');


        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    //======================================================================
    // replyPlivo Function Start Here
    //======================================================================
    public function replyPlivo(Request $request)
    {
        $number = $request->input('From');
        $sender = $request->input('To');
        $message = $request->input('Text');

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $message, $sender, 'Plivo');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    //======================================================================
    // replyInfoBip Function Start Here
    //======================================================================
    public function replyInfoBip(Request $request)
    {
        $number = $request->input('from');
        $sender = $request->input('to');
        $message = $request->input('body');

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($message)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($number, $msgcount, $message, $sender, 'InfoBip');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }


    //======================================================================
    // deliveryReportBulkSMS Function Start Here
    //======================================================================
    public function deliveryReportBulkSMS(Request $request)
    {

        $batch = $request->input('batch_id');
        $status = $request->input('status');

        switch ($status) {
            case '11':
                $status = 'Success';
                break;

            case '22':
                $status = 'Internal fatal error';
                break;

            case '23':
                $status = 'Authentication failure';
                break;

            case '24':
                $status = 'Data validation failed';
                break;

            case '25':
                $status = 'You do not have sufficient credits';
                break;

            case '26':
                $status = 'Upstream credits not available';
                break;

            case '27':
                $status = 'You have exceeded your daily quota';
                break;

            case '28':
                $status = 'Upstream quota exceeded';
                break;

            case '29':
                $status = 'Message sending cancelled';
                break;

            case '31':
                $status = 'Unroutable';
                break;

            case '32':
                $status = 'Blocked';
                break;

            case '33':
                $status = 'Failed: censored';
                break;

            case '50':
                $status = 'Delivery failed - generic failure';
                break;

            case '51':
                $status = 'Delivery to phone failed';
                break;

            case '52':
                $status = 'Delivery to network failed';
                break;

            case '53':
                $status = 'Message expired';
                break;

            case '54':
                $status = 'Failed on remote network';
                break;

            case '55':
                $status = 'Failed: remotely blocked';
                break;

            case '56':
                $status = 'Failed: remotely censored';
                break;

            case '57':
                $status = 'Failed due to fault on handset';
                break;

            case '64':
                $status = 'Queued for retry after temporary failure delivering, due to fault on handset';
                break;

            case '70':
                $status = 'Unknown upstream status';
                break;

            case 'default':
                $status = 'Failed';
                break;

        }

        $existing_status = 'In progress|' . $batch;

        $get_data = SMSHistory::where('status', 'like', '%' . $existing_status . '%')->first();

        if ($get_data) {
            $get_data->status = $status;
            $get_data->save();

            return 'success';
        } else {
            return 'failed';
        }


    }

    //======================================================================
    // replyMessageBird Function Start Here
    //======================================================================
    public function replyMessageBird(Request $request)
    {
        $number = $request->input('originator');
        $sender = $request->input('recipient');
        $body = $request->input('body');

        if ($number == '' && $body == '' && $sender) {
            return 'Invalid Request';
        }

        $clphone = str_replace(" ", "", $number); #Remove any whitespace
        $clphone = str_replace('+', '', $clphone);

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($clphone, $msgcount, $body, $sender, 'MessageBird');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }

    }


    //======================================================================
    // replyDiafaan Function Start Here
    //======================================================================
    public function replyDiafaan(Request $request)
    {

        $number = $request->input('from');
        $sender = $request->input('to');
        $body = $request->input('message');

        if ($number == '' && $body == '' && $sender) {
            return 'Invalid Request';
        }

        $clphone = str_replace(" ", "", $number); #Remove any whitespace
        $clphone = str_replace('+', '', $clphone);

        $msgcount = strlen(preg_replace('/\s+/', ' ', trim($body)));;
        $msgcount = $msgcount / 160;
        $msgcount = ceil($msgcount);

        $get_status = $this->insertSMS($clphone, $msgcount, $body, $sender, 'Diafaan');

        if ($get_status) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    //======================================================================
    // ultimateSMSContactApi Function Start Here
    //======================================================================
    public function ultimateSMSContactApi(Request $request)
    {
        $action = $request->input('action');
        $api_key = $request->input('api_key');
        $phone_book = $request->input('phone_book');
        $phone_number = $request->input('phone_number');

        if ($action == '' && $api_key == '') {
            return response()->json([
                'code' => '100',
                'message' => 'Bad gateway requested'
            ]);
        }

        switch ($action) {
            case 'subscribe-us':

                if ($phone_book == '' && $phone_number == '') {
                    return response()->json([
                        'code' => '100',
                        'message' => 'Bad gateway requested'
                    ]);
                }

                $isValid = PhoneNumberUtil::isViablePhoneNumber($phone_number);
                $phone = str_replace(['(', ')', '+', '-', ' '], '', $phone_number);

                if (!$isValid || !preg_match('/^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{3})[-\. ]?([0-9]{7})$/', trim($phone))) {
                    return response()->json([
                        'code' => '103',
                        'message' => 'Invalid Phone Number'
                    ]);
                }

                if (app_config('api_key') == $api_key) {
                    $contact_list = ImportPhoneNumber::where('user_id', 0)->where('group_name', $phone_book)->first();

                    if ($contact_list) {

                        $exist_check = ContactList::where('pid', $contact_list->id)->where('phone_number', $phone)->first();
                        if ($exist_check) {
                            return response()->json([
                                'code' => '105',
                                'message' => 'You already subscribed'
                            ]);
                        }

                        $status = ContactList::create([
                            'pid' => $contact_list->id,
                            'phone_number' => $phone
                        ]);

                        if ($status) {
                            return response()->json([
                                'code' => 'ok',
                                'message' => 'Subscription successfully done'
                            ]);
                        } else {
                            return response()->json([
                                'code' => '100',
                                'message' => 'Something went wrong. Please try again'
                            ]);
                        }


                    } else {
                        return response()->json([
                            'code' => '104',
                            'message' => 'Subscription list not found'
                        ]);
                    }

                } else {
                    $client = Client::where('api_key', $api_key)->where('api_access', 'Yes')->first();
                    if ($client) {
                        $contact_list = ImportPhoneNumber::where('user_id', $client->id)->where('group_name', $phone_book)->first();

                        if ($contact_list) {

                            $exist_check = ContactList::where('pid', $contact_list->id)->where('phone_number', $phone)->first();
                            if ($exist_check) {
                                return response()->json([
                                    'code' => '105',
                                    'message' => 'You already subscribed'
                                ]);
                            }

                            $status = ContactList::create([
                                'pid' => $contact_list->id,
                                'phone_number' => $phone
                            ]);

                            if ($status) {
                                return response()->json([
                                    'code' => 'ok',
                                    'message' => 'Subscription successfully done'
                                ]);
                            } else {
                                return response()->json([
                                    'code' => '100',
                                    'message' => 'Something went wrong. Please try again'
                                ]);
                            }


                        } else {
                            return response()->json([
                                'code' => '104',
                                'message' => 'Subscription list not found'
                            ]);
                        }


                    } else {
                        return response()->json([
                            'code' => '102',
                            'message' => 'Authentication Failed'
                        ]);
                    }
                }
                break;

            default:
                return response()->json([
                    'code' => '101',
                    'message' => 'Wrong action'
                ]);
                break;
        }

    }


}
