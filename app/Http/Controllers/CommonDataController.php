<?php

namespace App\Http\Controllers;

use App\Client;
use App\ContactList;
use App\ImportPhoneNumber;
use App\SMSGatewayCredential;
use App\SMSGateways;
use App\SMSHistory;
use App\SMSInbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Exceptions\LaravelExcelException;
use Maatwebsite\Excel\Facades\Excel;

class CommonDataController extends Controller
{

    //======================================================================
    // getCsvFileInfo Function Start Here
    //======================================================================
    public function getCsvFileInfo(Request $request)
    {

        try {

            $file_extension = Input::file('import_numbers')->getClientOriginalExtension();
            $supportedExt = array('csv', 'xls', 'xlsx');

            if (!in_array_r($file_extension, $supportedExt)) {
                return response()->json(['status' => 'error', 'message' => language_data('Insert Valid Excel or CSV file')]);
            }

            $all_data = Excel::load($request->import_numbers)->noHeading()->all()->toArray();

            if ($all_data && is_array($all_data) && array_empty($all_data)) {
                return response()->json(['status' => 'error', 'message' => 'Empty Field']);
            }

            $counter = "A";

            if ($request->header_exist == 'true') {

                $header = array_shift($all_data);

                foreach ($header as $key => $value) {
                    if (!$value) {
                        $header[$key] = "Column " . $counter;
                    }

                    $counter++;
                }

            } else {

                $header_like = $all_data[0];

                $header = array();

                foreach ($header_like as $h) {
                    array_push($header, "Column " . $counter);
                    $counter++;
                }

            }

            $all_data = array_map(function ($row) use ($header) {

                return array_combine($header, $row);

            }, $all_data);


            return response()->json(["status" => "success", "data" => $all_data]);

        } catch (LaravelExcelException $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }

    }


    //======================================================================
    // customUpdate Function Start Here
    //======================================================================
    public function customUpdate()
    {
        $sql = <<<EOF
ALTER TABLE `sys_operator` DROP `gateway`;
EOF;
        \DB::connection()->getPdo()->exec($sql);
        $all_clients = Client::all();

        foreach($all_clients as $client){
            $exist_gateway = $client->sms_gateway;
            $insert_gateway = "[\"$exist_gateway\"]";
            $client->sms_gateway = $insert_gateway;
            $client->save();
        }


//        $gateway = new SMSGateways();
//        $gateway->name = 'Alaris';
//        $gateway->settings = 'Alaris';
//        $gateway->api_link = 'https://api.passport.mgage.com';
//        $gateway->port = null;
//        $gateway->custom = 'No';
//        $gateway->status = 'Active';
//        $gateway->type = 'http';
//        $gateway->two_way = 'No';
//        $gateway->voice = 'No';
//        $gateway->mms = 'No';
//        $gateway->save();
//
//        $gateway_id = $gateway->id;
//
//        if ($gateway_id) {
//            $status = SMSGatewayCredential::create([
//                'gateway_id' => $gateway->id,
//                'username' => 'user_name',
//                'password' => 'password',
//                'extra' => null,
//                'status' => 'Active'
//            ]);
//
//            if ($status) {
//                return redirect('admin')->with([
//                    'message' => 'Gateway added successfully'
//                ]);
//            }
//            return redirect('/')->with([
//                'message' => 'Something went wrong. Please try again',
//                'message_important' => true
//            ]);
//
//        }
//        return redirect('/')->with([
//            'message' => 'Something went wrong. Please try again',
//            'message_important' => true
//        ]);

    }

}
